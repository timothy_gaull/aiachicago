<?php
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
$headers .= 'From: AIA Chicago <no-reply@aiachicago.org>' . "\r\n";

$todayDate = date("Y-m-d");// current date
$checkdate = strtotime(date("Y-m-d", strtotime($todayDate)) . "+60 day");

$expiry_count = 0;

$message = '';

$message_member = "Thank you for your continued support and membership of AIA Chicago.<br><br>";
$message_member .= "Please note that your current AIA membership is due to expire " . date("m/d/Y",$checkdate) . "<br><br>";
$message_member .= "As an AIA Chicago member, you are entitled to all the benefits of being associated with the nation's second largest AIA chapter: lectures and continuing education courses; numerous award programs; specialized, issue-specific Knowledge Communities; advocacy for architects; daily communications online and through our in-house magazine, Chicago Architect; and unlimited network opportunities with like-minded design professionals.<br><br>";
$message_member .= "Maintain your membership and renew today. <a href='http://www.aiachicago.org/membership/renew'>http://www.aiachicago.org/membership/renew</a><br><br>";
$message_member .= "<strong>Prefer to pay by check?</strong><br><br>";
$message_member .= "Simply print out this email, fill out the fields below and mail to:<br><br>";
$message_member .= "AIA Chicago<br>";
$message_member .= "Steve Riforgiato, Membership Manager<br>";
$message_member .= "35 E. Wacker Dr<br>";
$message_member .= "Suite 250<br>";
$message_member .= "Chicago IL 60660<br><br><br>";
$message_member .= "Member ID #: ________________ (note: this can be used as your invoice #)<br><br>";
$message_member .= "Member Name: ______________________________________ <br><br>";
$message_member .= "Member Email: ________________________________________________________ <br><br>";
$message_member .= "Member Phone: ____________________ <br><br>";


$Host = "localhost";
$User = "wwaiachi_ee4700";
$Password = "45d.(q:I6^31aom";
$DBName ="wwaiachi_ee";

$Link = mysql_connect($Host, $User, $Password);

mysql_select_db($DBName, $Link);

$Query = "SELECT ect.title, ect.status, ecd.field_id_26, ecd.field_id_151 FROM exp_channel_data ecd LEFT JOIN exp_channel_titles ect on ecd.entry_id = ect.entry_id LEFT JOIN exp_category_posts ecp ON ect.entry_id = ecp.entry_id WHERE ecp.cat_id in (5,6,7) ORDER BY ect.entry_date";
$Result = mysql_query ($Query);

while ($Row = mysql_fetch_array ($Result)) {

          if ($Row['field_id_151'] != '') {
             $aia_expiry_date = strtotime($Row['field_id_151']);
             if ($aia_expiry_date == $checkdate) {
                $message .= $Row['title'] . ', ' . $Row['field_id_26'] . ', ' . $Row['field_id_151'] . '<br>';
                $expiry_count = $expiry_count + 1;
                mail($Row['title'], 'AIA Membership Expiry Notice', $message_member, $headers);
             }
          } else {
             $expiry_date = '';
          }

}

$message .= '<br><br>Number of accounts expiring on ' . date("m/d/Y",$checkdate) . ': ' . $expiry_count;

mysql_close ($Link);

mail('sriforgiato@aiachicago.org,thodge@a5inc.com', 'AIA Membership Expiry Reminder', $message, $headers);
?>