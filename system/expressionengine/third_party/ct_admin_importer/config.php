<?php
$config['name'] = 'CT Admin Importer';
$config['mod_class'] = 'Ct_admin_importer';
$config['ext_class'] = 'Ct_admin_importer_ext';
$config['profiles_table'] = 'ct_admin_importer_profiles';
$config['version'] = '1.0';