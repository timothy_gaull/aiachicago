<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */
 
 /**
 * CT Admin Importer - Upd Class
 *
 * Updater class
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/upd.ct_admin_importer.php
 */
class Ct_admin_importer_upd 
{   
	/**
	 * The extensions we're installing
	 * @var array
	 */
    private $installed_hooks = array(
    	array('method' => 'ct_admin_main_menu_modify', 'hook' => 'ct_admin_modify_main_menu')
    );
    
    public $name = '';
    
    public $class = '';

    public $profiles_table = '';
     
    public function __construct() 
    { 
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
		include PATH_THIRD.'ct_admin_importer/config'.EXT;
		
		$this->version = $config['version'];	
		$this->name = $this->class = $config['mod_class'];
		$this->ext_class = $config['ext_class'];
		$this->profiles_table = $config['profiles_table'];
    } 
    
	public function install() 
	{
		$this->EE->load->dbforge();
	
		$data = array(
			'module_name' => $this->name,
			'module_version' => $this->version,
			'has_cp_backend' => 'y',
			'has_publish_fields' => 'n'
		);
	
		$this->EE->db->insert('modules', $data);
		
		$data = array('class' => $this->name, 'method' => 'void');
		$this->EE->db->insert('actions', $data);
		
		$this->add_profiles_table();
		$this->activate_extension();
		
		return TRUE;
	} 
	
	public function activate_extension()
	{
		
		$data = array();
		foreach($this->installed_hooks AS $ext)
		{
			$data[] = array(
						'class'      => $this->ext_class,
						'method'    => $ext['method'],
						'hook'  => $ext['hook'],
						'settings'    => '',
						'priority'    => 1,
						'version'    => $this->version,
						'enabled'    => 'y'
			);	
		}
		
		foreach($data AS $ex)
		{
			$this->EE->db->insert('extensions', $ex);	
		}		
	}

	public function uninstall()
	{
		$this->EE->load->dbforge();
	
		$this->EE->db->select('module_id');
		$query = $this->EE->db->get_where('modules', array('module_name' => $this->class));
	
		$this->EE->db->where('module_id', $query->row('module_id'));
		$this->EE->db->delete('module_member_groups');
	
		$this->EE->db->where('module_name', $this->class);
		$this->EE->db->delete('modules');
	
		$this->EE->db->where('class', $this->class);
		$this->EE->db->delete('actions');
		
		$this->EE->dbforge->drop_table($this->profiles_table);
		$this->disable_extension();
	
		return TRUE;
	}
	
	private function add_profiles_table()
	{
		$this->EE->load->dbforge();
		$fields = array(
			'id'	=> array(
				'type'			=> 'int',
				'constraint'	=> 10,
				'unsigned'		=> TRUE,
				'null'			=> FALSE,
				'auto_increment'=> TRUE
			),
			'name'	=> array(
				'type' 			=> 'varchar',
				'constraint'	=> '100',
				'null'			=> FALSE,
				'default'		=> ''
			),
			'active' => array(
				'type' => 'tinyint',
				'constraint' => 1,
				'null' => TRUE,
				'default' => '1'
			),
			'update_channel_status'	=> array(
				'type'			=> 'int',
				'constraint'	=> 10,
				'unsigned'		=> TRUE,
				'null'			=> FALSE
			),
			'type'	=> array(
				'type' 			=> 'varchar',
				'constraint'	=> '100',
				'null'			=> FALSE,
				'default'		=> ''
			),
			'file_upload_name'	=> array(
				'type' 			=> 'varchar',
				'constraint'	=> '100',
				'null'			=> FALSE,
				'default'		=> ''
			),
			'map_config'  => array(
				'type' => 'text',
				'null' => FALSE
			),				
			'last_modified'	=> array(
				'type' 			=> 'datetime'
			),
			'created_date'	=> array(
				'type' 			=> 'datetime'
			)
		);
	
		$this->EE->dbforge->add_field($fields);
		$this->EE->dbforge->add_key('id', TRUE);
		$this->EE->dbforge->create_table($this->profiles_table, TRUE);
	}	
	
	public function disable_extension()
	{
		$this->EE->db->where('class', $this->ext_class);
		$this->EE->db->delete('extensions');
	}

	public function update($current = '')
	{
		
		if ($current == $this->version)
		{
			return FALSE;
		}
		
	}	
}