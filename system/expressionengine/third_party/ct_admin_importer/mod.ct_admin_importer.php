<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */
 
 /**
 * CT Admin O,[prter - Mod Class
 *
 * Module class
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/mod.ct_admin_importer.php
 */
class Ct_admin_importer {

	public $return_data	= '';
	
	public function __construct()
	{
		$this->EE =& get_instance();
	}
	
	public function void()
	{
		
	}
}