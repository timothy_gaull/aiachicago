<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */
 
 /**
 * CT Admin Importer - Extension
 *
 * Extension class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/ext.ct_admin_importer.php
 */
class Ct_admin_importer_ext 
{
	/**
	 * The extensions default settings
	 * @var array
	 */
	public $settings = array();
	
	/**
	 * The extension name
	 * @var string
	 */
	public $name = '';
	
	/**
	 * The extension version
	 * @var float
	 */
	public $version = '';
	public $description	= '';
	public $settings_exist	= 'n';
	public $docs_url		= '';
	
	public $required_by = array('module');
	

	public $url_base = '';
	public $query_base = '';
	
	/**
	 * The name of the module; used for links and whatnots
	 * @var string
	 */
	private $mod_name = 'ct_admin_importer';	
		
	public function __construct($settings='')
	{
		$this->EE =& get_instance();
		include PATH_THIRD.'ct_admin_importer/config'.EXT;
		
		$this->version = $config['version'];
		$this->name = $config['name'];
				
		$this->settings = (!$settings ? $this->settings : $settings);
		$this->EE->lang->loadfile('ct_admin_importer');
		$this->description = lang('ct_admin_importer_module_description');
		$this->EE->load->add_package_path(PATH_THIRD.'ct_admin_importer/');
		$this->EE->load->add_package_path(PATH_THIRD.'ct_admin/');
		

		$this->query_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->mod_name.AMP.'method=';
		
		if(defined('BASE'))
		{
			$this->url_base = BASE.AMP.$this->query_base;
			$this->EE->load->vars(
				array(
					'url_base' => $this->url_base,
					'query_base' => $this->query_base
				)
			);			
		}	
	}
	
	public function ct_admin_main_menu_modify($menu)
	{
		$menu = ($this->EE->extensions->last_call != '' ? $this->EE->extensions->last_call : $menu);
		$menu[$this->EE->lang->line('import_menu')] = $this->url_base.'profiles';
		if(!empty($menu['settings']))
		{
			$temp = $menu['settings'];
			unset($menu['settings']);
			$menu['settings'] = $temp;
		}
		
		return $menu;
	}
	
	public function activate_extension() 
	{
		return TRUE;
	}
	
	public function update_extension($current = '')
	{
		return TRUE;
	}

	public function disable_extension()
	{
		return TRUE;

	}
}