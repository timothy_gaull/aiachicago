<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */

/**
 * CT Admin Importer - Generic Library
 *
 * Generic Library Class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/libraries/Ct_admin_importer_lib.php
 */
class Ct_admin_importer_lib
{
	/**
	 * The FieldTypes we're setup to import into
	 * @var array
	 */
	public $allowed_fts = array(
			'text', 
			'textarea', 
			'select', 
			'radio', 
			'select', 
			'editor'
	);
	
	/**
	 * The values in the Formats folder we want to ignore
	 * @var array
	 */
	public $ignored_format_items = array(
			'.', 
			'..', 
			'Importer_format_abstract.php'
	);
	
	/**
	 * Set us up
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->model('Ct_admin_importer_profiles_model', 'profiles_model');
	}
	
	/**
	 * Returns the directory where the Format classes are located
	 * @return string
	 */
	public function get_type_dir()
	{
		return realpath(dirname(__FILE__).'/Formats');
	}
	
	/**
	 * Returns the ignored format items
	 * @return multitype:
	 */
	public function get_ignored_format_items()
	{
		return $this->ignored_format_items;
	}
	
	/**
	 * Loads up all the available formats
	 */
	public function load_formats()
	{
		$path = $this->get_type_dir();
		$d = dir($path);
		$ignore = $this->get_ignored_format_items();
		while (false !== ($entry = $d->read())) {
			if(in_array($entry, $ignore))
				continue;

			$parts = explode('.', $entry);
			$lib = strtolower($parts['0']);
			$this->EE->load->library('Formats/'.$lib);
		}
		
		$d->close();
	}
	
	/**
	 * Grabs the available channel fields for importing
	 * @param int $channel_id
	 * @return multitype:unknown
	 */
	public function get_channel_fields($channel_id)
	{
		$channel_data = $this->EE->channel_model->get_channel_info($channel_id)->row();
		$channel_fields = $this->EE->channel_model->get_channel_fields($channel_data->field_group)->result_array();
		$return = array();
		foreach($channel_fields AS $field)
		{
			if(in_array($field['field_type'], $this->allowed_fts))
			{
				$return[$field['field_id']] = $field['field_label'];
			}
		}
		
		return $return;
	}
	
	/**
	 * Returns the statuses to use for Orders
	 * @return multitype:string unknown
	 */
	public function get_order_channel_statuses()
	{
		$order_statuses = $this->EE->ct_admin_orders->get_channel_statuses();
		$statuses = array('' => '');
		foreach($order_statuses AS $key => $value)
		{
			$statuses[$value['status_id']] = $value['status'];
		}

		return $statuses;
	}
	
	/**
	 * Returns the name for the given $status_id
	 * @param int $status_id
	 */
	public function get_status_name($status_id)
	{
		return $this->EE->db->select('status')->from('statuses')->where(array('status_id' => $status_id))->get()->row('status');
	}
}