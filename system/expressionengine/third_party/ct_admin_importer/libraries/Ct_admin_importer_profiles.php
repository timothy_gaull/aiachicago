<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */

/**
 * CT Admin Importer - Profile Library
 *
 * Library Class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/libraries/Ct_admin_importer_profiles.php
 */
class Ct_admin_importer_profiles
{
	/**
	 * The location to store any upoaded assets
	 * @var string
	 */
	public $storage_location = FALSE;
	
	/**
	 * Set this shit up!
	 */
	public function __construct()
	{				
		$this->EE =& get_instance();
		$this->EE->load->model('Ct_admin_importer_profiles_model', 'profiles_model');
		
		$this->storage_location = APPPATH.'cache/ct_admin_importer';
		if ( ! is_dir($this->storage_location))
		{
			mkdir($this->storage_location, TRUE, TRUE);
		}		
	}
	
	/**
	 * Returns the available file formats
	 * @return multitype:string
	 */
	public function get_profile_types()
	{
		$path = $this->EE->importer->get_type_dir();
		$d = dir($path);
		$return = array();
		$ignore = $this->EE->importer->get_ignored_format_items();
		while (false !== ($entry = $d->read())) {
			if(in_array($entry, $ignore))
				continue;
			
			$parts = explode('.', $entry);
			$lib = strtolower($parts['0']);
			$name = $this->EE->$lib->get_name();
			$return = array_merge($return, $name);
		}
		
		$d->close();	
		return $return;
	}
	
	/**
	 * Returns the directory the example file types are stored on upload
	 * @return boolean
	 */
	public function get_storage_location()
	{
		return $this->storage_location;
	}
	
	/**
	 * Returns a multidimensional array of multiple profiles
	 * @param array $where
	 */
	public function get_profiles(array $where = array())
	{
		return $this->EE->profiles_model->get_profiles($where);
	}
	
	/**
	 * Returns a single array of a profile
	 * @param array $where
	 */
	public function get_profile(array $where = array())
	{
		$profile_data = $this->EE->profiles_model->get_profile($where);
		if($profile_data)
		{
			$profile_data['profile_name'] = $profile_data['name'];
			$profile_data['profile_id'] = $profile_data['id'];
			if(!empty($profile_data['map_config']))
			{
				$profile_data['map_config'] = (array)json_decode(base64_decode($profile_data['map_config']));
			}
			
			return $profile_data;
		}
	}
	
	/**
	 * Returns just the fields that are mappable
	 * @param stirng $format
	 * @param string $file_path
	 */
	public function get_mappable_fields($format, $file_path)
	{
		return $this->EE->$format->get_mappable_fields($this->get_storage_location().'/'.$file_path);
	}
	
	/**
	 * Uses the Format libraries to determine what file extensions to allow on upload
	 * @param string $format
	 * @return string
	 */
	public function validate_file_format($format)
	{
		$file_formats = $this->EE->$format->get_file_formats();
		return implode('|', $file_formats);
	}
	
	/**
	 * Creates a new profile
	 * @param array $data
	 */
	public function add_profile(array $data)
	{	
		$parts = explode('.', $data['file_upload_name']);
		$tail = end($parts);
		$new_name = md5(microtime().$data['file_upload_name']).'.'.$tail;
		
		rename($this->storage_location.'/'.$data['file_upload_name'], $this->storage_location.'/'.$new_name);
		$data['file_upload_name'] = $new_name;
		return $this->EE->profiles_model->add_profile($data);
	}	
	
	/**
	 * Removes a profile and all associated data
	 * @param int $profile_id
	 */
	public function delete_profiles(array $profile_ids)
	{
		foreach($profile_ids AS $profile_id)
		{
			if($this->EE->profiles_model->delete_profiles(array('id' => $profile_id)))
			{
				
			}
		}
		
		return TRUE;
	}
	
	/**
	 * Updates a profile
	 * @param int $profile_id
	 * @param array $data
	 */
	public function update_profile(array $data, array $where, $complete = TRUE)
	{
		return $this->EE->profiles_model->update_profile($data, $where, $complete);
	}
	
	/**
	 * Updates the Profile's specific Mapping hash
	 * @param int $profile_id
	 * @param array $data
	 */
	public function update_profile_map($profile_id, array $data)
	{
		$data = array('map_config' => base64_encode(json_encode($data)));
		return $this->update_profile($data, array('id' => $profile_id), FALSE);
	}

	/**
	 * Handles the processing of the uploaded import file
	 * @param array $profile_data
	 * @param array $upload_data
	 * @return array
	 */
	public function import_upload(array $profile_data, array $upload_data)
	{
		$upload_data['file_upload_name'] = str_replace(' ', '_', $upload_data['file_upload_name']);
		$parts = explode('.', $upload_data['file_upload_name']);
		$tail = end($parts);
		$new_name = $this->storage_location.'/'.md5(microtime().$upload_data['file_upload_name']).'.'.$tail;
		rename($this->storage_location.'/'.$upload_data['file_upload_name'], $new_name);
		$upload_data['file_upload_name'] = $new_name;
		$import_data = $this->EE->$profile_data['type']->proc_upload($new_name, $profile_data['map_config']);
		unlink($new_name);
		$entry_id = FALSE;
		$updated = array();
		foreach($import_data AS $key => $entry_data)
		{
			//verify how we're updating the data (entry_id or transaction_id)
			if($profile_data['map_config']['pk_link_type'] != 'entry_id')
			{
				break; //todo
			}
			else
			{
				$entry_id = $entry_data['entry_id'];
				unset($entry_data['entry_id']);
			}
			
			//no point continuing without $entry_id
			if(!$entry_id)
			{
				continue;
			}
			
			//update the order meta info first
			$this->EE->db->where('entry_id', $entry_id);
			$this->EE->db->update('channel_data', $entry_data);
			
			//collect entry's so we can notify the user about which order was updated
			if($this->EE->db->affected_rows() >= 1)
			{
				$updated[] = $entry_id;
			}
			
			//now the status update (only if it's needed)
			if($upload_data['update_channel_status'] != '0')
			{
				$status = $this->EE->importer->get_status_name($upload_data['update_channel_status']);
				$this->EE->ct_admin_orders->upate_status($entry_id, $status);
			}
		}
		
		return $updated;
		
	}
}