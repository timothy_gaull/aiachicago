<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */

/**
 * Include the Abstract
 */
require_once 'Importer_format_abstract.php';

/**
 * CT Admin Importer - Tab Seperated Format
 *
 * Tab Seperated Format Class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/libraries/Formats/Tsv.php
 */
class Tsv extends Importer_format_abstract
{
	
	/**
	 * The column delimiter used
	 * @var string
	 */
	public $delim = "\t";
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_name()
	 */
	public function get_name() 
	{
		return array('tsv' => 'TSV');
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::validate_upload()
	 */
	public function validate_upload() 
	{
		$this->EE->form_validation->set_rules('file_upload', 'File', 'required');
		
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_mappable_fields()
	 */
	public function get_mappable_fields($path) 
	{
		$return = array();
		$row = 1;
		ini_set('auto_detect_line_endings',TRUE);
		if (($handle = fopen($path, "r")) !== FALSE) 
		{
			while (($data = fgetcsv($handle, 0, $this->delim)) !== FALSE) {
				foreach($data AS $key => $value) {
					$value = str_replace(' ', '_', $value);
					$return[$value] = $value;
				}
				break;
			}
			fclose($handle);
		}
		
		return $return;
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::proc_upload()
	 */
	public function proc_upload($path, array $map_data)
	{
		if (($handle = fopen($path, "r")) !== FALSE)
		{
			$count = 0;
			$keys = array();
			$arr = array();
			ini_set('auto_detect_line_endings',TRUE);
			while (($data = fgetcsv($handle, 0, $this->delim)) !== FALSE) 
			{
				//extract out the keys for use as column relationships
				if($count == '0')
				{
					foreach($data AS $key => $value)
					{
						$keys[] = str_replace(' ', '_', $value);
					}
					
					$count++;
					continue;
				}

				$row = array();
				foreach($data AS $key => $value) 
				{
					$row[$keys[$key]] = $value;
				}
				
				$arr[] = $row;
				$count++;
			}
			
			fclose($handle);
			
			$return = array();
			$count = 0;
			foreach($arr AS $key => $row)
			{
				foreach($row AS $colum_key => $column_value)
				{
					//set the value for the FT
					if(array_key_exists($colum_key, $map_data) && $map_data[$colum_key] != '0')
					{
						$return[$count]['field_id_'.$map_data[$colum_key]] = $column_value;
					}

					if(!empty($row[$map_data['pk_link']]))
					{
						$return[$count][$map_data['pk_link_type']] = $row[$map_data['pk_link']];
					}					
				}
				
				$count++;
			}
			
			return $return;
		}		
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_file_formats()
	*/
	public function get_file_formats()
	{
		return array('xls', 'txt');
	}	
}