<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */

/**
 * Include the Abstract
 */
require_once 'Tsv.php';

/**
 * CT Admin Importer - Worldship Format
 *
 * Worldship Format Class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/libraries/Formats/Worldship.php
 */
class Csv extends Tsv
{
	
	/**
	 * The column delimiter used
	 * @var string
	 */
	public $delim = ",";
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_name()
	 */
	public function get_name() 
	{
		return array('csv' => 'CSV');
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_file_formats()
	*/
	public function get_file_formats()
	{
		return array('csv', 'txt');
	}	
}