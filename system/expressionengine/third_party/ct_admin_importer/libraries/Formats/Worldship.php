<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */

/**
 * Include the Abstract
 */
require_once 'Importer_format_abstract.php';

/**
 * CT Admin Importer - Worldship Format
 *
 * Worldship Format Class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/libraries/Formats/Worldship.php
 */
class Worldship extends Importer_format_abstract
{

	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_name()
	 */
	public function get_name() {
		return array('worldship' => 'Worldship');
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::validate_upload()
	 */
	public function validate_upload() {
		$this->EE->form_validation->set_rules('file_upload', 'File', 'required');
		
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_mappable_fields()
	 */
	public function get_mappable_fields($path) {
		$xml = file_get_contents($path);
		$xml = simplexml_load_string(str_replace('encoding="UTF-16"', 'encoding="UTF-8"', $xml));
		$return = array();
		foreach($xml->Shipment AS $shipment)
		{
			$arr = (array)$shipment->ShipmentInformation;
			foreach($arr AS $key => $value)
			{
				$return[$key] = $key;
			}
		}
		
		return $return;
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::proc_upload()
	 */
	public function proc_upload($path, array $map_data)
	{
		$return = array();
		$count = 0;
		$xml = file_get_contents($path);
		$xml = simplexml_load_string(str_replace('encoding="UTF-16"', 'encoding="UTF-8"', $xml));
		foreach($xml->Shipment AS $shipment)
		{
			$arr = (array)$shipment->ShipmentInformation;
			foreach($arr AS $key => $value)
			{
				foreach($map_data AS $map_key => $map_value)
				{
					//set the value for the FT
					if($map_key == $key && $map_value != '0')
					{
						$return[$count]['field_id_'.$map_value] = $value;
					}
				}
			}
			
			if(!empty($arr[$map_data['pk_link']]))
			{
				$return[$count][$map_data['pk_link_type']] = $arr[$map_data['pk_link']];
			}
			
			$count++;
		}
		
		return $return;
	}
	
	/* (non-PHPdoc)
	 * @see Importer_format_abstract::get_file_formats()
	 */
	public function get_file_formats()
	{
		return array('xml');
	}
}