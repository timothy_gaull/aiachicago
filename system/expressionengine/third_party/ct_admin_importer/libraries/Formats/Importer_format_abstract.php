<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */

/**
 * Importer Format - Abstract
 *
 * Importer Format Abstract Class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/libraries/Formats/Importer_format_abstract.php
 */
abstract class Importer_format_abstract
{
	/**
	 * Set things up
	 */
	public function __construct() {}
	
	/**
	 * Returns the details of the format
	 */
	public abstract function get_name();
	
	/**
	 * Validates the uploaded map file
	 */
	public abstract function validate_upload();
	
	/**
	 * Returns a single dimension array of the pieces to map
	 * @param string $path
	 */
	public abstract function get_mappable_fields($path);
	
	/**
	 * The actual processing of the uploaded file
	 * @param string $path
	 */
	public abstract function proc_upload($path, array $map_data);
	
	/**
	 * Returns an array of file extensions that CodeIgniter will allow for upload
	 */
	public abstract function get_file_formats();
}