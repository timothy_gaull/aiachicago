<?php $this->load->view('errors'); ?>

<div class="ct_top_nav">
	<div class="ct_nav">
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'edit_profile&profile_id='.$profile_id; ?>" ><?php echo lang('edit_profile')?></a>
		</span>	
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'map_profile&profile_id='.$profile_id; ?>" ><?php echo lang('map_profile')?></a>
		</span>			
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'add_profile'; ?>" ><?php echo lang('add_profile')?></a>
		</span>						
	</div>
</div>

<br clear="all" />
<?php if($updated ): ?>

	<div class="ct_admin_message_success">
		<?php 
		if($updated == 'yes')
		{
			$imported_orders = explode('|', $orders);
			echo count($imported_orders).' '.lang('order(s) updated!');
		}
		else
		{
			echo lang('Nothing to update...');
		}
		
		?>
	</div>

<?php endif; ?>

<?php 
if(!isset($form_action))
{
	$form_action = 'view_profile';
}
echo form_open($query_base.$form_action, array('id'=>'my_accordion', 'enctype' => 'multipart/form-data'));
?>
<input type="hidden" name="profile_id" value="<?php echo $profile_id; ?>" />
<h3  class="accordion"><?=lang('general')?></h3>
<div>
	<?php 
	
	//set form defaults
	$defaults = array();
	$defaults['update_channel_status'] = $profile_data['update_channel_status'];

	$this->table->set_heading(lang('setting'), lang('value'));
	$this->table->add_row('<label for="update_channel_status">'.lang('update_channel_status').'</label><div class="subtext">'.lang('update_channel_status_instructions').'</div>', form_dropdown('update_channel_status', $channel_statuses, $defaults['update_channel_status'], 'id="update_channel_status"'). form_error('update_channel_status'));	
	$this->table->add_row('<label for="file_upload">'.lang('file_upload').'</label><div class="subtext">'.lang('file_upload_instructions').'</div>', form_upload('file_upload', '', 'id="file_upload"'). form_error('file_upload'));	
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>

<div class="tableFooter">
	<div class="tableSubmit">
		<?php echo form_submit(array('name' => 'submit', 'value' => lang('submit'), 'class' => 'submit'));?>
	</div>
</div>	
<?php echo form_close()?>