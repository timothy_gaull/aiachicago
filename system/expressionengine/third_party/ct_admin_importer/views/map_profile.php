<?php 

$tmpl = array (
		'table_open'          => '<table class="mainTable" border="0" cellspacing="0" cellpadding="0">',

		'row_start'           => '<tr class="even">',
		'row_end'             => '</tr>',
		'cell_start'          => '<td style="width:50%;">',
		'cell_end'            => '</td>',

		'row_alt_start'       => '<tr class="odd">',
		'row_alt_end'         => '</tr>',
		'cell_alt_start'      => '<td>',
		'cell_alt_end'        => '</td>',

		'table_close'         => '</table>'
);

$this->table->set_template($tmpl);
$this->table->set_empty("&nbsp;");

$this->load->view('errors'); 

?>

<div class="ct_top_nav">
	<div class="ct_nav">
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'edit_profile&profile_id='.$profile_id; ?>" ><?php echo lang('edit_profile')?></a>
		</span>	
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'view_profile&profile_id='.$profile_id; ?>" ><?php echo lang('view_profile')?></a>
		</span>			
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'add_profile'; ?>" ><?php echo lang('add_profile')?></a>
		</span>						
	</div>
</div>

<br clear="all" />

<?php echo lang('profile_map_instructions'); ?>
<div class="clear_left shun"></div>

<?php 

echo form_open($query_base.$form_action, array('id'=>'my_accordion'));
?>
<h3  class="accordion"><?=lang('link_column')?></h3>
<div>
	<?php 
	
	echo lang('link_column_instructions');
	//set form defaults
	$defaults = array();
	$this->table->set_heading(lang('setting'), lang('value'));
	
	$defaults['pk_link_type'] = (!empty($profile_data['map_config']['pk_link_type']) ? $profile_data['map_config']['pk_link_type'] : false);
	$defaults['pk_link'] = (!empty($profile_data['map_config']['pk_link']) ? $profile_data['map_config']['pk_link'] : false);
	$this->table->add_row(
			form_dropdown('pk_link_type', 
			array('entry_id' => 'entry_id', 'transaction_id' => 'transaction_id'), 
			$defaults['pk_link_type'], 
			'id="pk_link_type"'). form_error('pk_link_type'),

			form_dropdown('pk_link',
			$map_fields,
			$defaults['pk_link'],
			'id="pk_link"'). form_error('pk_link')
	);
	
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>

<h3  class="accordion"><?=lang('update_columns')?></h3>
<div>
	<?php 
	
	echo lang('update_columns_instructions');
	
	//set form defaults
	$defaults = array();
	$this->table->set_heading(lang('setting'), lang('value'));
	$channel_fields = array('0' => '') + $channel_fields;
	foreach($map_fields AS $map_field)
	{
		$defaults[$map_field] = (!empty($profile_data['map_config'][$map_field]) ? $profile_data['map_config'][$map_field] : false);;
		$this->table->add_row(
				'<label for="'.$map_field.'">'.$map_field.'</label>',
				form_dropdown($map_field,
					$channel_fields,
					$defaults[$map_field],
					'id="'.$map_field.'"'
				).form_error('type')
		);
	}	
	
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>

<div class="tableFooter">
	<div class="tableSubmit">
		<?php echo form_submit(array('name' => 'submit', 'value' => lang('submit'), 'class' => 'submit'));?>
	</div>
</div>	
<?php echo form_close(); 
