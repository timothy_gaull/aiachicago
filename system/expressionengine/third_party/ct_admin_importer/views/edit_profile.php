<div class="ct_top_nav">
	<div class="ct_nav">
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'view_profile&profile_id='.$profile_id; ?>" ><?php echo lang('back_to_profile')?></a>
		</span>	
		<span class="button"> 
			<a class="nav_button" href="<?php echo $url_base.'add_profile'; ?>" ><?php echo lang('add_profile')?></a>
		</span>						
	</div>
</div>
<?php

$tmpl = array (
		'table_open'          => '<table class="mainTable" border="0" cellspacing="0" cellpadding="0">',

		'row_start'           => '<tr class="even">',
		'row_end'             => '</tr>',
		'cell_start'          => '<td style="width:50%;">',
		'cell_end'            => '</td>',

		'row_alt_start'       => '<tr class="odd">',
		'row_alt_end'         => '</tr>',
		'cell_alt_start'      => '<td>',
		'cell_alt_end'        => '</td>',

		'table_close'         => '</table>'
);

$this->table->set_template($tmpl);
$this->table->set_empty("&nbsp;");
?>
<div class="clear_left shun"></div>

<?php 
if(!isset($form_action))
{
	$form_action = 'edit_profile';
}
echo form_open($query_base.$form_action, array('id'=>'my_accordion', 'enctype' => 'multipart/form-data'));
?>
<h3  class="accordion"><?=lang('general')?></h3>
<div>
	<?php 
	
	//set form defaults
	$defaults = array();
	$defaults['name'] = (isset($profile_data['name']) ? $profile_data['name'] : FALSE);
	$defaults['active'] = (isset($profile_data['active']) ? $profile_data['active'] : '1');
	$defaults['type'] = (isset($profile_data['type']) ? $profile_data['type'] : 'worldship');
	$defaults['update_channel_status'] = (isset($profile_data['update_channel_status']) ? $profile_data['update_channel_status'] : FALSE);

	$this->table->set_heading(lang('setting'), lang('value'));
	$this->table->add_row('<label for="name">'.lang('name').'</label><div class="subtext">'.lang('name_instructions').'</div>', form_input('name', $defaults['name'], 'id="name"'). form_error('name'));
	$this->table->add_row('<label for="update_channel_status">'.lang('update_channel_status').'</label><div class="subtext">'.lang('update_channel_status_instructions').'</div>', form_dropdown('update_channel_status', $channel_statuses, $defaults['update_channel_status'], 'id="update_channel_status"'). form_error('update_channel_status'));	
	$this->table->add_row('<label for="type">'.lang('type').'</label><div class="subtext">'.lang('type_instructions').'</div>', form_dropdown('type', $profile_types, $defaults['type'], 'id="type"'). form_error('type'));	
	$this->table->add_row('<label for="file_upload">'.lang('file_upload').'</label><div class="subtext">'.lang('file_upload_instructions').'</div>', form_upload('file_upload', '', 'id="file_upload"'). form_error('file_upload'));	
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>

<div class="tableFooter">
	<div class="tableSubmit">
		<?php echo form_submit(array('name' => 'submit', 'value' => lang('submit'), 'class' => 'submit'));?>
	</div>
</div>	
<?php echo form_close()?>