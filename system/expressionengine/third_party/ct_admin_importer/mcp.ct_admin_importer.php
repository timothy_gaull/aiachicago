<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin Importer
 *
 * @package		mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2013, mithra62, Eric Lamb.
 * @link		http://mithra62.com/
 * @updated		1.0
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/
 */
 
/**
 * Setup the CT Admin CP
 */
$EE =& get_instance();
$EE->load->file(PATH_THIRD . "ct_admin/mcp.ct_admin.php");

 /**
 * CT Admin Importer - CP Class
 *
 * Control Panel class
 *
 * @package 	mithra62:Ct_admin_importer
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin_importer/mcp.ct_admin_extender.php
 */
class Ct_admin_importer_mcp extends Ct_admin_mcp
{
	/**
	 * The URL to the module
	 * @var string
	 */
	public $url_base = '';
	
	/**
	 * The name of the module; used for links and whatnots
	 * @var string
	 */
	private $mod_name = 'ct_admin_importer';
	
	/**
	 * The breadcrumb override 
	 * @var array
	 */
	protected static $_breadcrumbs = array();
	
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->add_package_path(PATH_THIRD.'ct_admin/');
		$this->EE->lang->loadfile('ct_admin');
		parent::__construct(); //load up CT Admin and CartThrob libraries
		
		$this->add_breadcrumb($this->url_base.'index', lang('ct_admin_module_name'));
		$this->EE->load->add_package_path(PATH_THIRD.'ct_admin_importer/');
		
		$this->EE->load->library('ct_admin_importer_profiles', null, 'profiles');
		$this->EE->load->library('ct_admin_importer_lib', null, 'importer');
		
		//override a couple things now
		$this->ct_admin_url_base = $this->url_base;
		$this->ct_admin_query_base = $this->query_base;
		$this->query_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->mod_name.AMP.'method=';
		$this->url_base = BASE.AMP.$this->query_base;
		$this->EE->load->vars(
			array(
				'url_base' => $this->url_base, 
				'query_base' => $this->query_base, 
				'ct_admin_url_base' => $this->ct_admin_url_base, 
				'ct_admin_query_base' => $this->ct_admin_query_base, 
				'settings' => $this->settings
			)
		);	
		
		$this->EE->importer->load_formats();
		$this->add_breadcrumb($this->url_base.'index', lang('importer'));
	}

	private function add_breadcrumb($link, $title)
	{
		$this->EE->cp->set_breadcrumb($link, $title);
	}	

	public function index()
	{
		$this->EE->functions->redirect($this->url_base.'profiles');
	}	
	
	public function profiles()
	{
		$this->add_breadcrumb($this->url_base.'index', lang('importer'));
		
		$vars = array();
		$vars['errors'] = $this->errors;
		$vars['profiles'] = $this->EE->profiles->get_profiles();
		if(!$vars['profiles'])
		{
			$this->EE->functions->redirect($this->url_base.'add_profile');
			exit;
		}
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('importer_main');
		return $this->EE->load->view('profiles', $vars, TRUE);		
	}
	
	public function add_profile()
	{
		$this->EE->load->library('form_validation');
		$this->EE->form_validation->set_error_delimiters('<div class="ct_admin_error">', '</div>');
		$this->EE->form_validation->set_rules('name', 'Name', 'required');
		
		if (empty($_FILES['file_upload']['name']))
		{
			$this->EE->form_validation->set_rules('file_upload', 'File', 'required');
		}		
		
		$upload_error = FALSE;
		if ($this->EE->form_validation->run() == TRUE)
		{
			$data = $_POST;
			$config['upload_path'] = $this->EE->profiles->get_storage_location();
			$config['allowed_types'] = $this->EE->profiles->validate_file_format($data['type']);
			$this->EE->load->library('upload', $config);
			if ( ! $this->EE->upload->do_upload('file_upload') )
			{
				$upload_error = $this->EE->upload->display_errors();
			}
			else 
			{
				$data['file_upload_name'] = $_FILES['file_upload']['name'];
				$profile_id = $this->EE->profiles->add_profile($data);
				if($profile_id)
				{
					$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('profile_added'));
					$this->EE->functions->redirect($this->url_base.'map_profile'.AMP.'profile_id='.$profile_id);
					exit;
				}
			}
		}
	
		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->javascript->output($this->EE->ct_admin_js->get_accordian_css());
		//$this->EE->javascript->output($this->EE->flag_master_js->get_form_profile());
		$this->EE->javascript->compile();
	
		$this->add_breadcrumb($this->url_base.'profiles', lang('profiles'));
	
		$vars = array();
		$vars['profile'] = $_POST;
		$vars['upload_error'] = $upload_error;
		$vars['profile_types'] = $this->EE->profiles->get_profile_types();
		$vars['channel_statuses'] = $this->EE->importer->get_order_channel_statuses();
		$this->EE->view->cp_page_title = $this->EE->lang->line('add_profile');
		return $this->EE->load->view('add_profile', $vars, TRUE);
	}
	
	public function map_profile()
	{
		$this->EE->load->library('form_validation');
		$this->EE->form_validation->set_rules('pk_link_type', 'Primary Link', 'required');

		$vars = array();
		$profile_id = $this->EE->input->get_post('profile_id', FALSE);
		if(!$profile_id)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_found'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
		
		$where = array('id' => $profile_id);
		$profile_data = $this->EE->profiles->get_profile($where);
		if(!$profile_data)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_found'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
		
		$error = FALSE;
		if ($this->EE->form_validation->run() == TRUE)
		{
			$data = $_POST;
			$update_profile = $this->EE->profiles->update_profile_map($profile_id, $data);
			if($update_profile)
			{
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('profile_mapped'));
				$this->EE->functions->redirect($this->url_base.'view_profile'.AMP.'profile_id='.$profile_id);
				exit;
			}
		}		
		
		$map_fields = $this->EE->profiles->get_mappable_fields($profile_data['type'], $profile_data['file_upload_name']);
		$channel_fields = $this->EE->importer->get_channel_fields($this->EE->cartthrob->store->config('orders_channel'));
		$this->EE->cp->add_js_script('ui', 'accordion');
		
		$this->add_breadcrumb($this->url_base.'profiles', lang('profiles'));
		$vars['profile_data'] = $profile_data;
		$vars['profile_id'] = $profile_id;
		$vars['map_fields'] = $map_fields;
		$vars['channel_fields'] = $channel_fields;
		$vars['form_action'] = $this->query_base.'map_profile&profile_id='.$profile_id;

		$this->EE->view->cp_page_title = $this->EE->lang->line('map_profile').' ('.$profile_data['name'].')';
		return $this->EE->load->view('map_profile', $vars, TRUE);		
	}
	
	public function view_profile()
	{
		$this->EE->cp->add_js_script('ui', 'accordion', 'sortable');
		$this->EE->javascript->compile();
	
		$vars = array();
		$error = FALSE;
		$profile_id = $this->EE->input->get('profile_id');
		if(!$profile_id)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_found'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
	
		$where = array('id' => $profile_id);
		$profile_data = $this->EE->profiles->get_profile($where);
		if(!$profile_data)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_found'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
		
		if(empty($profile_data['map_config']))
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('map_profile_required'));
			$this->EE->functions->redirect($this->url_base.'map_profile&profile_id='.$profile_id);
			exit;
		}
		
		$this->EE->load->library('form_validation');
		$this->EE->form_validation->set_rules('profile_id', 'Profile ID', 'required');
		$this->EE->form_validation->set_error_delimiters('<div class="ct_admin_error">', '</div>');
		
		if (empty($_FILES['file_upload']['name']))
		{
			$this->EE->form_validation->set_rules('file_upload', 'File', 'required');
		}
				
		if ($this->EE->form_validation->run() == TRUE)
		{
			$data = $_POST;
			$config['upload_path'] = $this->EE->profiles->get_storage_location();
			$config['allowed_types'] = 'xml|csv|xls';
			$this->EE->load->library('upload', $config);
			if ( ! $this->EE->upload->do_upload('file_upload') )
			{
				$error = array('error' => $this->EE->upload->display_errors());
			}
			else 
			{
				$data['file_upload_name'] = $_FILES['file_upload']['name'];
				$updated = $this->EE->profiles->import_upload($profile_data, $data);
				if(count($updated) >= 1)
				{
					$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('upload_imported'));
					$this->EE->functions->redirect($this->url_base.'view_profile'.AMP.'profile_id='.$profile_id.AMP.'updated=yes'.AMP.'orders='.implode('|', $updated));
					exit;
				}
				else
				{
					$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('nothing_to_import'));
					$this->EE->functions->redirect($this->url_base.'view_profile'.AMP.'profile_id='.$profile_id.AMP.'updated=no');
					exit;					
				}
			}
		}

		$this->add_breadcrumb($this->url_base.'profiles', lang('profiles'));

		$vars['profile_id'] = $profile_id;
		$vars['upload_errors'] = $error;
		$vars['profile_data'] = $profile_data;
		$vars['channel_statuses'] = $this->EE->importer->get_order_channel_statuses();
		$vars['form_action'] = $this->query_base.'view_profile&profile_id='.$profile_id;
		$vars['updated'] = $this->EE->input->get('updated');
		$vars['orders'] = $this->EE->input->get('orders');
		
		$this->EE->view->cp_page_title = $profile_data['name'];
		return $this->EE->load->view('view_profile', $vars, TRUE);
	}
	
	public function edit_profile()
	{
		$vars = array();
		$profile_id = $this->EE->input->get('profile_id', FALSE);
		if(!$profile_id)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_found'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
	
		$profile_data = $this->EE->profiles->get_profile(array('id' => $profile_id));
		if(!$profile_data)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_found'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
	
		$this->EE->load->library('form_validation');
		$this->EE->form_validation->set_rules('name', 'Name', 'required');
		$this->EE->form_validation->set_error_delimiters('<div class="ct_admin_error">', '</div>');
		
		if ($this->EE->input->post('type') != '' && $this->EE->input->post('type') != $profile_data['type'])
		{
			$this->EE->form_validation->set_rules('file_upload', 'File', 'required');
		}
				
		if ($this->EE->form_validation->run() == TRUE)
		{
			$data = $_POST;
			$data['file_upload_name'] = $profile_data['file_upload_name'];
			if($this->EE->profiles->update_profile($data, array('id' => $profile_id)))
			{
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('profile_updated'));
				$this->EE->functions->redirect($this->url_base.'view_profile&profile_id='.$profile_id);
				exit;
			}
			else
			{
				$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profile_not_updated'));
				$this->EE->functions->redirect($this->url_base.'edit_profile&profile_id='.$profile_id);
				exit;
			}
		}
	
		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->javascript->compile();
	
		$this->add_breadcrumb($this->url_base.'profiles', lang('profiles'));
		$this->add_breadcrumb($this->url_base.'view_profile'.AMP.'profile_id='.$profile_data['id'], $profile_data['name']);
	
		$vars['profile_id'] = $profile_id;
		$vars['profile_data'] = $profile_data;
		$vars['profile_types'] = $this->EE->profiles->get_profile_types();
		$vars['form_action'] = $this->query_base.'edit_profile&profile_id='.$profile_id;
		$vars['channel_statuses'] = $this->EE->importer->get_order_channel_statuses();
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('edit_profile').' ('.$profile_data['name'].')';
		return $this->EE->load->view('edit_profile', $vars, TRUE);
	}
	
	public function delete_profile_confirm()
	{
		$profile_ids = $this->EE->input->get_post('toggle', TRUE);
		if(!$profile_ids || count($profile_ids) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('no_profiles'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
	
		$profile_data = array();
		$ids = array();
		foreach($profile_ids AS $id)
		{
			$data = $this->EE->profiles->get_profile(array('id' => $id));
			if(is_array($data) && count($data) != '0')
			{
				$profile_data[] = $data;
				$ids[] = $data['id'];
			}
		}
		
		$this->add_breadcrumb($this->url_base.'profiles', lang('profiles'));
	
		$vars = array();
		$vars['form_action'] = $this->query_base.'delete_profiles';
		$vars['damned'] = $ids;
		$vars['data'] = $profile_data;
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('delete_profiles_confirm');
		return $this->EE->load->view('delete_profile_confirm', $vars, TRUE);
	}
	
	public function delete_profiles()
	{
		$profile_ids = $this->EE->input->get_post('delete', FALSE);
		if($this->EE->profiles->delete_profiles($profile_ids))
		{
			$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('profiles_deleted'));
			$this->EE->functions->redirect($this->url_base.'profiles');
			exit;
		}
		$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('profiles_delete_failure'));
		$this->EE->functions->redirect($this->url_base.'profiles');
		exit;
	
	}	
}