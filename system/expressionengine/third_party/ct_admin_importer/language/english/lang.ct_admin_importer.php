<?php

$lang = array(


// -------------------------------------------
//  Module CP
// -------------------------------------------
'ct_admin_importer_module_name' => 'CT Admin Importer',
'ct_admin_importer_module_description' => 'Allows import/update of data in CartThrob and CT Admin.',
'import_menu' => 'Import',
'importer' => 'Importer',
'importer_main' => 'Profiles',
'value' => 'Value',

//Profiles	
'profiles' => 'Profiles',	
'no_profiles' => 'No Profiles Found',
'profiles_instructions' => 'The Imports are setup with reusability in mind. Create a Profile and you can run it again later.',
'add_profile' => 'Add Profile',
'name' => 'Name',
'name_instructions' => 'What identifier do you want to use to remember this profile?',
'general' => 'General',
'type_instructions' => 'What type of import are we doing?',
'worldship' => 'Worldship',
'file_upload' => 'File',
'file_upload_instructions' => 'The file you want to upload. Be sure the format matches the selected "Type" above.',
'inactive' => 'Inactive',
'active' => 'Active',
'view_profile' => 'View Profile',
'edit_profile' => 'Edit Profile',
'map_profile' => 'Map Profile',
'delete_profiles_question' => 'Are you sure you want to remove the below profiles?',
'profile_map_instructions' => 'Use the below form to tell CT Admin how to relate the uploaded file to your Order channel within ExpressionEngine. First, select <em>how</em> to link the data and, second, which data to sync. ',
'link_column' => 'Linked Column',
'update_columns' => 'Update Columns',
'link_column_instructions' => 'Which field in the upload do you want to relate data to within your ExpressionEngine channel information (entry_id or Transaction number)?',
'update_columns_instructions' => 'Pick the columns you want to update with the import. Be CAREFUL; the old data will be removed permanantly.',
'map_profile_required' => 'Map Profile Required',
'profile_mapped' => 'Profile Mapped!',
'delete_profiles_confirm' => 'Delete Profile(s)',
'profiles_deleted' => 'Profile(s) Deleted',
'profile_added' => 'Profile Added',
'back_to_profile' => 'Back to Profile',
'profile_updated' => 'Profile Updated!',
'update_channel_status' => 'Update Channel Status',
'update_channel_status_instructions' => 'What status do you want to change the Channel Entries when the upload is complete?',
'upload_imported' => 'Upload Imported',
'nothing_to_import' => 'Nothing to Update...',
''=>''
);
