<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
I just needed something like this and I guess it can be useful to others.
Share freely... :-D
 */

$plugin_info = array(
						'pi_name'			=> 'JC Redirect',
						'pi_version'		=> '1.1',
						'pi_author'			=> 'Jensa',
						'pi_author_url'		=> 'http://www.flashgamer.com/expressionengine/',
						'pi_description'	=> 'Redirects admins and users to different URLs (if set), based on their group_id. May also be used for basic redirection otherwise',
						'pi_usage'			=> Jc_redirect::usage()
					);

class Jc_redirect
{

    var $return_data = '';

	function jc_redirect()
	{
		$this->EE =& get_instance();
		
		// Fetch parameters from the plugin tag
		$location = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location'));
		$location1 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location1'));
		$location2 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location2'));
		$location3 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location3'));
		$location4 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location4'));
		$location5 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location5'));
		$location6 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location6'));
		$location7 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location7'));
		$location8 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location8'));
		$location9 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location9'));
		$location10 = str_replace("&#47;", "/", $this->EE->TMPL->fetch_param('location10'));
		
		$mode = $this->EE->TMPL->fetch_param('mode');
		if( !$mode ){ $mode = "group"; }
		
		if( $mode == "group"){ // redirect based on group
			if($this->EE->session->userdata('group_id') == "1" && $location1){
				$this->EE->functions->redirect($location1);
			} else if($this->EE->session->userdata('group_id') == "2" && $location2){
				$this->EE->functions->redirect($location2);
			} else if($this->EE->session->userdata('group_id') == "3" && $location3){
				$this->EE->functions->redirect($location3);
			} else if($this->EE->session->userdata('group_id') == "4" && $location4){
				$this->EE->functions->redirect($location4);
			} else if($this->EE->session->userdata('group_id') == "5" && $location5){
				$this->EE->functions->redirect($location5);
			} else if($this->EE->session->userdata('group_id') == "6" && $location6){
				$this->EE->functions->redirect($location6);
			} else if($this->EE->session->userdata('group_id') == "7" && $location7){
				$this->EE->functions->redirect($location7);
			} else if($this->EE->session->userdata('group_id') == "8" && $location8){
				$this->EE->functions->redirect($location8);
			} else if($this->EE->session->userdata('group_id') == "9" && $location9){
				$this->EE->functions->redirect($location9);
			} else if($this->EE->session->userdata('group_id') == "10" && $location10){
				$this->EE->functions->redirect($location10);
			}else if($location){
				$this->EE->functions->redirect($location);
			}
		} else if( $mode == "plain"){
			if( $location){
				$this->EE->functions->redirect($location);
			}
		}
	}
// END


// ----------------------------------------
//  Plugin Usage
// ----------------------------------------
// This function describes how the plugin is used.
// Make sure and use output buffering

function usage()
{
ob_start(); 
?>

The plugin has two methods "group" (default) and "plain". The first will redirect the user based on the group_id they belong to. The other will just do a plain redirect to whatever URL you set.

==============
Group redirect
==============
Just set location1 for superadmins (since their group_id is 1) or location5 for Members (since their group_id is 5) like this:

{exp:jc_redirect location1="http://myserver.com/admin/" location5="http://myserver.com/members/" location="http://myserver.com/all_others"}

If no group_id is matched, the default location will be used (if set). If no default location is set, no redirect will be sent. If you need more than 10 group_id's, just open the plugin and cut'n'paste.

==============
Plain redirect
==============
Use something like the excellent Stash-extension to check a conditional and then produce the variable {my_url} containing the URL to go to:

{exp:jc_redirect location='{my_url}' mode="plain"}

<?php

$buffer = ob_get_contents();

ob_end_clean(); 

return $buffer;
}
// END
}
// END CLASS

/* End of file pi.jc_redirect.php */
/* Location: ./system/expressionengine/third_party/jc_redirect/pi.jc_redirect.php */