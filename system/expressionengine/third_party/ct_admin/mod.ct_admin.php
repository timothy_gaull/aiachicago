<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Mod Class
 *
 * Module class
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/mod.ct_admin.php
 */
class Ct_admin {

	public $return_data	= '';
	
	/**
	 * The post statuses to ignore within calculations
	 * @var array
	 */
	public $ignore_statuses = array('closed');	
	
	public $order_ignore_sql = '';
	
	public $user_products = array();
	
	public $member_id = FALSE;

	public $discounts = FALSE;	
	
	public function __construct()
	{
		// Make a local reference to the ExpressionEngine super object
		$this->EE =& get_instance();
		$this->EE->load->model('channel_model', 'channel_model');
		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/'); 
		$this->EE->load->library('cartthrob_loader');
		$this->EE->cartthrob_loader->setup($this);
		$this->EE->load->model('order_model', 'order_model');
		
		$this->EE->load->model('ct_admin_settings_model', 'ct_admin_settings', TRUE);
		$this->EE->load->library('ct_admin_lib');
		$this->EE->load->library('ct_admin_orders');
		$this->EE->load->library('ct_admin_reports');
		$this->EE->load->library('ct_admin_customers');
		$this->EE->load->library('ct_admin_products');
		$this->EE->load->library('ct_admin_channel_data');
		$this->EE->load->helper('utilities');
		
		$this->settings = $this->EE->ct_admin_lib->get_settings();		
		$this->number_format_defaults_prefix = $this->EE->cartthrob->store->config('number_format_defaults_prefix');
		$this->order_channel_statuses = $this->EE->ct_admin_orders->get_channel_statuses();
		$this->order_ignore_sql = FALSE;
		$this->success_statuses = array();
		
		if(count($this->settings['success_statuses']) >= '1')
		{
			$temp = array();
			foreach($this->order_channel_statuses AS $key => $status)
			{
				if(in_array($status['status_id'], $this->settings['success_statuses']))
				{
					$temp[] = "'".$status['status']."'";
					$this->success_statuses[] = $status['status'];
				}
			}
				
			if(count($temp) >= '1')
			{
				$this->order_ignore_sql = " AND status IN(".implode(',', $temp).")";
			}
		}
	}
	
	public function void()
	{
		
	}
	
    public function is_shippable()
    {
        foreach ($this->cart->items() as $row_id => $item)
        {        
            $product = ($item->product_id()) ? $this->EE->product_model->get_product($item->product_id()) : FALSE;
            if ($product)
            {
                $data = $this->EE->cartthrob_entries_model->entry_vars($product);
                if($data && isset($data['product_shippable']) && $data['product_shippable'] == 'Yes')
                {
                    return TRUE;
                }
            }
        }
    } 
    
    public function product_sum_report()
    {
    	$seller_id = $this->EE->TMPL->fetch_param('seller_id');
    	$product_id = $this->EE->TMPL->fetch_param('product_id');
    	$month = $this->EE->TMPL->fetch_param('month');
    	$year = $this->EE->TMPL->fetch_param('year');
    	$day = $this->EE->TMPL->fetch_param('day');
    	
    	$where = $this->order_ignore_sql;
    	
        //only setup seller id if we have one
    	//NOTE: locks results down to user products
    	if($seller_id)
    	{
	    	if($seller_id == 'CURRENT_USER')
	    	{
	    		$seller_id = $this->EE->session->userdata('member_id');
	    	}
	    	
	    	$seller_id = (int)$seller_id;
	    	$product_ids = $this->EE->ct_admin_products->get_author_product_ids($seller_id);
	    	if(count($product_ids) == '0')
	    	{
    			return $this->EE->TMPL->no_results();
	    	}
	    	
	    	$where .= " AND ecoi.entry_id IN( ".implode(',',$product_ids)." )";
    	}
    	
    	if($product_id)
    	{
    		$where .= " AND ecoi.entry_id = '".$product_id."'";
    	}
    	
    	if($year)
    	{
    		$where .= " AND year='".$this->EE->db->escape_str($year)."'";
    	}
    	
    	if($month)
    	{
    		$where .= " AND month='".$this->EE->db->escape_str($month)."'";
    	}

    	if($day)
    	{
    		$where .= " AND day='".$this->EE->db->escape_str($day)."'";
    	}    	
    	
    	$report = $this->EE->ct_admin_reports->product_sum_report($where);
    	if(count($report) == '0')
    	{
    		return $this->EE->TMPL->no_results();
    	} 
    	
    	foreach($report AS $key => $value)
    	{
    		$report[$key]['total_price'] = m62_format_money($value['total_price']);
    		$report[$key]['unit_price'] = m62_format_money($value['price']);
    	}

    	$output = $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $report); 
    	return $output;
    }
    
    public function product_totals()
    {
    	$seller_id = $this->EE->TMPL->fetch_param('seller_id');
    	$product_id = $this->EE->TMPL->fetch_param('product_id');
    	$month = $this->EE->TMPL->fetch_param('month');
    	$year = $this->EE->TMPL->fetch_param('year');
    	$day = $this->EE->TMPL->fetch_param('day');
    	    	
    	$where = $this->order_ignore_sql;
    	$ecoi_where = FALSE;
    	$ect_where = FALSE;
        if($seller_id)
    	{
	    	if($seller_id == 'CURRENT_USER')
	    	{
	    		$seller_id = $this->EE->session->userdata('member_id');
	    	}
	    	
	    	$seller_id = (int)$seller_id;
	    	$product_ids = $this->EE->ct_admin_products->get_author_product_ids($seller_id);
	    	if(count($product_ids) == '0')
	    	{
	    		return $this->EE->TMPL->no_results();
	    	}
	    	
	    	$ecoi_where = " AND ecoi.entry_id IN( ".implode(',',$product_ids)." )";
	    	$ect_where = " AND ect.entry_id IN( ".implode(',',$product_ids)." )";
    	}
    	
    	if($product_id)
    	{
    		$where .= " AND ecoi.entry_id = '".$product_id."'";
    	}    	
    	 
    	if($year)
    	{
    		$where .= " AND year='".$this->EE->db->escape_str($year)."'";
    	}
    	 
    	if($month)
    	{
    		$where .= " AND month='".$this->EE->db->escape_str($month)."'";
    	}
    	
    	if($day)
    	{
    		$where .= " AND day='".$this->EE->db->escape_str($day)."'";
    	}	
    	
    	$report = array();
    	$report['items_sold'] = $this->EE->ct_admin_products->get_total_items_sold($where.$ecoi_where." AND ect.channel_id = '".$this->EE->ct_admin_orders->channel_id."'");
    	$report['gross_sales'] = $this->EE->ct_admin_products->get_total_gross_sales($where.$ecoi_where." AND ect.channel_id = '".$this->EE->ct_admin_orders->channel_id."'");
    	//$totals = $this->EE->ct_admin_reports->get_all_totals($where.$ect_where);

    	$report['gross_sales'] = m62_format_money($report['gross_sales']);
    
    	$output = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $report);
    	return $output; 
    }
    
    public function purchased_products()
    {
    	$seller_id = $this->EE->TMPL->fetch_param('seller_id');
    	$product_id = $this->EE->TMPL->fetch_param('product_id');
    	$month = $this->EE->TMPL->fetch_param('month');
    	$year = $this->EE->TMPL->fetch_param('year');
    	$day = $this->EE->TMPL->fetch_param('day');
    	
    	$where = $this->order_ignore_sql;
    	$ecoi_where = FALSE;
    	$ect_where = FALSE;
    	if($seller_id)
    	{
    		if($seller_id == 'CURRENT_USER')
    		{
    			$seller_id = $this->EE->session->userdata('member_id');
    		}
    	
    		$seller_id = (int)$seller_id;
    		$product_ids = $this->EE->ct_admin_products->get_author_product_ids($seller_id);
    		if(count($product_ids) == '0')
    		{
    			return $this->EE->TMPL->no_results();
    		}
    	
    		$ecoi_where = " AND ecoi.entry_id IN( ".implode(',',$product_ids)." )";
    		$ect_where = " AND ect.entry_id IN( ".implode(',',$product_ids)." )";
    	}
    	 
    	if($product_id)
    	{
    		$where .= " AND ecoi.entry_id = '".$product_id."'";
    	}
    	
    	if($year)
    	{
    		$where .= " AND year='".$this->EE->db->escape_str($year)."'";
    	}
    	
    	if($month)
    	{
    		$where .= " AND month='".$this->EE->db->escape_str($month)."'";
    	}
    	 
    	if($day)
    	{
    		$where .= " AND day='".$this->EE->db->escape_str($day)."'";
    	}   

    	$data = $this->EE->ct_admin_products->get_purchased_products($where.$ecoi_where." AND ect.channel_id = '".$this->EE->ct_admin_orders->channel_id."'");
    	if(count($data) == '0')
    	{
    		return $this->EE->TMPL->no_results(); 		
    	}
    	
    	$output = $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $data);
    	return $output;
    }
    
    public function invoice()
    {
    	$order_id = $this->EE->TMPL->fetch_param('order_id');
    	if($order_id == 0)
    	{
    		return $this->EE->TMPL->no_results();
    	} 
    	
    	//check user stuff
    	$customer_id = $this->EE->TMPL->fetch_param('customer_id');
    	if($customer_id)
    	{
    		if($customer_id == 'CURRENT_USER')
    		{
    			$customer_id = $this->EE->session->userdata('member_id');
    		}

    		//does the user get a pass?
    		if(!$this->EE->session->userdata('can_edit_other_entries') || !$this->EE->session->userdata('can_access_cp'))
    		{
    			//check the customer's orders so they only see their stuff
    			$member_data = $this->EE->member_data->get_members(array('members.member_id' => $customer_id));
    			if(isset($member_data['0']) && is_array($member_data['0']))
    			{
    				$member_data = $member_data['0'];
    			}
    			else 
    			{
    				return $this->EE->TMPL->no_results();
    			}
    			
    			$orders = $this->EE->ct_admin_customers->get_orders_by_email($member_data['email']);
    			$display = FALSE;
    			foreach($orders AS $order)
    			{
    				if($order_id == $order['entry_id'])
    				{
    					$display = TRUE;
    					break;
    				}
    			}
    			
    			if(!$display)
    			{
    				return $this->EE->TMPL->no_results();
    			}
    		}
    	}

    	$order_data = $this->EE->ct_admin_channel_data->get_entry(array('entry_id' => explode('|', $order_id)));
    	if(!$order_data)
    	{
    		return $this->EE->TMPL->no_results();
    	}
    	
    	$order_items = $this->EE->ct_admin_orders->get_order_items(explode('|', $order_id));   
    	foreach($order_data AS $key => $value)
    	{
	    	if(isset($order_data[$key]['entry_date']))
	    	{
	    		$order_data[$key]['entry_date'] = strtotime($order_data[$key]['entry_date']);
	    	}
	    	
	    	$conditionals = array();
	    	$product_channel = $this->EE->cartthrob->store->config('product_channels');
	    	$order_products = array();
	    	$product_key = 0;
	    	foreach($order_items AS $k => $v)
	    	{
	    		if($v['order_id'] != $value['entry_id'])
	    		{
	    			continue; //this item isn't a part of this order
	    		}
	    		
	    		$order_items[$k]['item_title'] = $v['title'];
	    		$order_items[$k]['item_price'] = m62_format_money($v['price']);
	    		$order_items[$k]['item_quantity'] = $v['quantity'];
	    		$order_items[$k]['item_total_price'] = m62_format_money($v['quantity']*$v['price']);
	    		$order_items[$k]['sub_items'] = array();
	    		$order_items[$k]['is_package'] = FALSE;
	    		if(isset($v['extra']) && $v['extra'] != '')
	    		{
	    			$options = @unserialize(base64_decode($v['extra']));
	    			if(is_array($options) && count($options) >= '1')
	    			{
	    				if(isset($options['sub_items']) && is_array($options['sub_items']) && count($options['sub_items']) >= '1')
	    				{
	    					$count = 1;
	    					$order_items[$k]['is_package'] = TRUE;
	    					foreach($options['sub_items'] AS $_k => $_v)
	    					{
	    						$order_items[$k]['sub_items'][$_k]['sub_item_title'] = $_v['title'];
	    						$order_items[$k]['sub_items'][$_k]['sub_item_quantity'] = $_v['quantity'];
	    						$order_items[$k]['sub_items'][$_k]['sub_item_count'] = $count;
	    						$count++;
	    					}
	    					
	    				}
	    				else
	    				{
	    					//$cell .= ' ('.implode(' / ', $options).')';
	    				}
	    			}
	    		}
	    		
	    		$order_products[$product_key] = $order_items[$k];
	    		$product_key++;
	    	}
	    	
	
	    	$order_data[$key]['order_items'] = $order_products;
	    	$order_data[$key]['order_id'] = $order_id;
	    	if(isset($order_data['order_details']['order_customer_email']))
	    	{
	    		$order_data[$key]['ee_member_id'] = $this->EE->ct_admin_customers->get_ee_member_id($order_data['order_details']['order_customer_email']);
	    	}
    	}
    	
    	$output = $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $order_data);  	
    	return $output;    	
    }

    public function discount_price()
    {
    	$product_id = $this->EE->TMPL->fetch_param('product_id', FALSE);
    	$orig_price = (float)$this->EE->TMPL->fetch_param('orig_price', FALSE);
    	$price = ltrim($this->EE->TMPL->fetch_param('price', FALSE), '$');
    	if(!$orig_price)
    	{
    		$orig_price = $price;
    	}
    
    	$this->EE->load->library('ct_admin_discounts');
    	if(!is_array($this->discounts) || count($this->discounts) == '0')
    	{
    		$this->discounts = $this->EE->ct_admin_discounts->get_discounts();
    	}
    
    	$new_price = 0;
    	foreach($this->discounts AS $discount)
    	{
    		$method = str_replace('Cartthrob_discount_', '', $discount['discount_type']['type']);
    		if(is_callable(array($this->EE->ct_admin_discounts, $method)))
    		{
    			if($this->EE->ct_admin_discounts->validate($product_id, $price, $discount['discount_type']))
    			{
    				$price = $this->EE->ct_admin_discounts->$method($product_id, $price, $discount['discount_type']);
    			}
    		}
    	}
    
    	$price_data = array(
    			'ct:product_price' => m62_format_money($price)
    	);
    
    	if($orig_price > $price)
    	{
    		$price_data['ct:product_original_price'] = m62_format_money($orig_price);
    	}
    
    	$output = $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $price_data);
    	return $output;
    }
    
    public function discount_info()
    {
    	$discounts = $this->EE->cartthrob->get_discount_data();//$this->EE->discount_model->get_valid_discounts();
    	if(!$discounts)
    	{
    		return $this->EE->TMPL->no_results();
    	}
    
    	$discount_ids = array_keys($discounts);
    	$this->EE->db->select('title, entry_id')->where_in('entry_id', $discount_ids)->where('status', 'open');
    	$data = $this->EE->db->get('channel_titles')->result_array();
    	foreach($data AS $key => $value)
    	{
    		$discount_data[$value['entry_id']] = $value['title'];
    	}
    
    	foreach($discounts AS $discount_id => $discount)
    	{
    		if (empty($discount['type']))
    		{
    			continue;
    		}
    			
    		$plugin = $this->EE->cartthrob->create_child($this->EE->cartthrob, $discount['type'], $discount);
    		if (method_exists($plugin, 'get_discount'))
    		{
    			$discounts[$discount_id]['discount_total'] = $this->EE->cartthrob->round($plugin->get_discount());
    			$discounts[$discount_id]['discount_title'] = $discount_data[$discount['entry_id']];
    			$discounts[$discount_id]['discount_type'] = 'discount';
    		}
    	}
    
    	foreach ($this->EE->cartthrob->cart->coupon_codes() as $coupon_code)
    	{
    		$data = $this->EE->cartthrob->get_coupon_code_data($coupon_code);
    		if ($this->EE->cartthrob->validate_coupon_code($coupon_code) && ! empty($data['type']))
    		{
    			$plugin = $this->EE->cartthrob->create_child($this->EE->cartthrob, $data['type'], $data);
    			if (method_exists($plugin, 'get_discount'))
    			{
    				$discounts[$data['metadata']['entry_id']]['discount_total'] = $this->EE->cartthrob->round($plugin->get_discount());
    				$discounts[$data['metadata']['entry_id']]['discount_title'] = $coupon_code;
    				$discounts[$data['metadata']['entry_id']]['discount_type'] = 'coupon';
    			}
    		}
    	}
    
    	$vars = array();
    	$i = 0;
    	foreach($discounts AS $discount)
    	{
    		foreach($discount AS $key => $value)
    		{
    			$vars[$i]['ct:'.$key] = $value;
    		}
    		$i++;
    	}
    
    	$output = $this->EE->TMPL->parse_variables($this->EE->TMPL->tagdata, $vars);
    	return $output;
    }
    
    public function cart_discount_subtotal()
    {
    	$cart = $this->cart->to_array();
    	if(empty($cart['items']))
    	{
    		return '0';
    	}
    
    	//what products are in the cart?
    	$return = 0;
    	$product_ids = array();
    	foreach($cart['items'] AS $item)
    	{
    		$product_ids[] = $item['product_id'];
    	}
    
    	return $this->EE->number->format(($this->EE->cartthrob->cart->subtotal()-$this->EE->cartthrob->cart->discount()));
    }
    
    public function check_inventory()
    {
    	$entry_id = $this->EE->TMPL->fetch_param('entry_id');
    	$field = $this->EE->TMPL->fetch_param('field');
    
    	$data = $this->EE->product_model->get_product($entry_id);
    	$inventory = unserialize(base64_decode($data['inventory']));
    	$row = array('no_inventory' => true);
    	if(is_array($inventory))
    	{
    		foreach($inventory AS $key => $value)
    		{
    			if(!empty($value[$field]))
    			{
    				$in = (int)$value[$field];
    				if($in >= '1')
    				{
    					$row['no_inventory'] = false;
    					break;
    				}
    			}
    		}
    	}
    	else
    	{
    		$row['no_inventory'] = false;
    	}
    
    	return $this->EE->TMPL->parse_variables_row($this->EE->TMPL->tagdata, $row);
    }   
}