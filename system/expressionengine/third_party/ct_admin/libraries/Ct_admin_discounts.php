<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Product Library
 *
 * Wrapper for the various CartThrob Discount functionality
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/libraries/Ct_admin_discounts.php
 */
class Ct_admin_discounts
{
	public $channel_id;

	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->model('ct_admin_settings_model', 'ct_admin_settings', TRUE);
		$this->EE->load->library('ct_admin_lib');
		$this->channel_id = $this->EE->cartthrob->store->config('discount_channel');
		$this->settings = $this->EE->ct_admin_lib->get_settings();
		$this->config = $this->EE->cartthrob->store->config();
		$this->dbprefix = $this->EE->db->dbprefix;
	}
	
	public function discount_price($entry_id, $default_price = FALSE)
	{
		$discounts = '';
	}

	public function get_discounts()
	{
		return $this->EE->ct_admin_channel_data->get_entries(array('ct.channel_id' => $this->channel_id, 'status' => 'open'));
	}

	public function percentage_off_product($product_id, $price, array $settings, $quanitity = '1')
	{
		$percentage_off = $settings['percentage_off'];
		$discount = $price * $quanitity * ($percentage_off / 100);
		$price = $price-$discount;
		return $price;
	}

	public function percentage_off($product_id, $price, array $settings, $quanitity = '1')
	{
		$percentage_off = $settings['percentage_off'];
		$discount = $price * $settings['percentage_off'] / 100;
		$price = $price-$discount;

		return $price;
	}

	public function amount_off_product($product_id, $price, array $settings, $quanitity = '1')
	{
		$price = $price-$settings['amount_off'];
		return $price;
	}

	public function validate($product_id, $price, array $settings, $quantity = '1')
	{
		$return =  TRUE;
		//we have to check if the discount has been used before
		if(!empty($settings['per_user_limit']) && $settings['per_user_limit'] >= 1)
		{
			//we only want to apply discounts for logged in members 
			if($this->EE->session->userdata('member_id') == '0')
			{
				return FALSE;
			}

			$used_by = explode('|', $settings['used_by']);
			if($this->EE->session->userdata('member_id') >= '1' && in_array($this->EE->session->userdata('member_id'), $used_by))
			{
				//the member has used this discount before. now we have to find out how many and make sure they didn't go over the limit
				$counts = array_count_values($used_by);
				$total_used = $counts[$this->EE->session->userdata('member_id')];
				if(!empty($counts[$this->EE->session->userdata('member_id')]) && $total_used >= $settings['per_user_limit'])
				{
					$return = FALSE;
				}
			}
		}

		//next let's check if it's product specific
		if (!empty($settings['entry_ids']) && $entry_ids = preg_split('/\s*(,|\|)\s*/', trim($settings['entry_ids'])))
		{
			if ($product_id && !in_array($product_id, $entry_ids))
			{
				$return = FALSE;
			}
		}

		return $return;
	}
	
}
