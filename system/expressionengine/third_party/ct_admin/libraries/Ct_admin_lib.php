<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */

 /**
 * CT Admin - Generic methods
 *
 * Library Class
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/libraries/ct_admin_lib.php
 */
class Ct_admin_lib
{
	/**
	 * Flag to render download data in the browser
	 * @var bool
	 */
	public $disable_download = FALSE;
	
	/**
	 * The default URL to use within the CP
	 */
	private $url_base = FALSE;
	
	/**
	 * Set it up
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->model('ct_admin_settings_model', 'ct_admin_settings', TRUE);
		$this->settings = $this->get_settings();
	}

	/**
	 * Returns the array needed for the CP menu
	 * @return multitype:string
	 */
	public function get_right_menu()
	{
		$menu = array(
			'dashboard'		=> $this->url_base.'index',
			'orders'		=> $this->url_base.'orders',
			'customers'		=> $this->url_base.'customers',
			'products'		=> $this->url_base.'products',
			'reports'		=> $this->url_base.'reports'
		);
		
		if($this->EE->session->userdata('group_id') == '1' || (isset($this->settings['allowed_access_levels']) && is_array($this->settings['allowed_access_levels'])))
		{
			if($this->EE->session->userdata('group_id') == '1' || in_array($this->EE->session->userdata('group_id'), $this->settings['allowed_access_levels']))
			{
				$menu['settings'] = $this->url_base.'settings'.AMP.'section=general';
			}
		}
		
		if ($this->EE->extensions->active_hook('ct_admin_modify_main_menu') === TRUE)
		{
			$menu = $this->EE->extensions->call('ct_admin_modify_main_menu', $menu);
			if ($this->EE->extensions->end_script === TRUE) return $menu;
		}		
		
		return $menu;
	}
	
	/**
	 * Creates the Settings menu for the view script
	 * @return multitype:multitype:string  multitype:string unknown
	 */
	public function get_settings_view_menu()
	{
		$menu = array(
			'general' => array('url' => 'general', 'target' => '_self', 'div_class' => 'invoice_button'),
			'cp' => array('url' => 'cp', 'target' => '_self', 'div_class' => 'invoice_button'),
			'limits' => array('url' => 'limits', 'target' => '_self', 'div_class' => 'packing_slip_button'),
			'date_formats' => array('url' => 'date_formats', 'target' => '', 'div_class' => 'edit_in_publish_view_button'),
			'reporting' => array('url' => 'reporting', 'target' => '', 'div_class' => 'edit_in_publish_view_button'),
		);
	
		if ($this->EE->extensions->active_hook('ct_admin_modify_settings_menu') === TRUE)
		{
			$menu = $this->EE->extensions->call('ct_admin_modify_settings_menu', $menu);
			if ($this->EE->extensions->end_script === TRUE) return $menu;
		}
	
		return $menu;
	}	
	
	/**
	 * Wrapper to handle CP URL creation
	 * @param string $method
	 */
	public function _create_url($method)
	{
		return $this->url_base.$method;
	}

	/**
	 * Creates the value for $url_base
	 * @param string $url_base
	 */
	public function set_url_base($url_base)
	{
		$this->url_base = $url_base;
	}
	
	/**
	 * Creates a key => value array for limiting results
	 * @return multitype:string
	 */
	public function perpage_select_options()
	{
		return array(
			'10' => '10 '.lang('results'),
			'25' => '25 '.lang('results'),
			'75' => '75 '.lang('results'),
			'100' => '100 '.lang('results'),
			'150' => '150 '.lang('results')
		);		
	}
	
	/**
	 * Returns the date select options array for HTML forms
	 * @return multitype:NULL string
	 */
	public function date_select_options()
	{
		return array(
			'' => lang('date_range'),
			'1' => lang('past_day'),
			'7' => lang('past_week'),
			'31' => lang('past_month'),
			'182' => lang('past_six_months'),
			'365' => lang('past_year'),
			'custom_date' => lang('any_date')
		);				
	}
	
	/**
	 * Creates the available actions for the Order View page 
	 * @return multitype:NULL string
	 */
	public function get_action_options()
	{
		return array(
			'print_invoices' => lang('print_invoices'),
			'packing_slip' => lang('packing_slip'),
			'change_status' => lang('change_status'),
			'delete' => lang('delete_selected')
		);		
	}
	
	/**
	 * Returns the options for shippable select input field
	 * @return multitype:NULL string
	 */
	public function get_shippable_options()
	{
		return array(
			'' => lang('shippable_status'),
			'shippable' => lang('shippable'),
			'virtual' => lang('virtual')
		);		
	}
	
	/**
	 * Wrapper to craft the Ajax pagination interface
	 * @param string $method
	 * @param int $total
	 * @param int $per_page
	 */
	public function create_pagination($method, $total, $per_page)
	{
		$config = array();
		$config['page_query_string'] = TRUE;
		$config['base_url'] = $this->url_base.'edit_orders_ajax_filter';
		$config['total_rows'] = $total;
		$config['per_page'] = $this->settings['orders_list_limit'];
		$config['page_query_string'] = TRUE;
		$config['full_tag_open'] = '<p id="paginationLinks">';
		$config['full_tag_close'] = '</p>';
		$config['prev_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_prev_button.gif" width="13" height="13" alt="&lt;" />';
		$config['next_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_next_button.gif" width="13" height="13" alt="&gt;" />';
		$config['first_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_first_button.gif" width="13" height="13" alt="&lt; &lt;" />';
		$config['last_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_last_button.gif" width="13" height="13" alt="&gt; &gt;" />';

		$this->EE->pagination->initialize($config);
		return $this->EE->pagination->create_links();		
	}
	
	/**
	 * Wrapper that runs all the tests to ensure system stability
	 * @return array;
	 */
	public function error_check()
	{
		$errors = array();
		if($this->settings['license_number'] == '')
		{
			$errors['license_number'] = 'missing_license_number';
		}
		return $errors;
	}	
	
	/**
	 * Returns an array for configuring the EE pagination mechanism 
	 * @param string $method
	 * @param int 	$total_rows
	 * @param int	 $perpage
	 */
	public function pagination_config($method, $total_rows, $perpage)
	{
		// Pass the relevant data to the paginate class
		$config['base_url'] = $this->_create_url($method);
		$config['total_rows'] = $total_rows;
		$config['per_page'] = $perpage;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = 'rownum';
		$config['full_tag_open'] = '<p id="paginationLinks">';
		$config['full_tag_close'] = '</p>';
		$config['prev_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_prev_button.gif" width="13" height="13" alt="&lt;" />';
		$config['next_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_next_button.gif" width="13" height="13" alt="&gt;" />';
		$config['first_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_first_button.gif" width="13" height="13" alt="&lt; &lt;" />';
		$config['last_link'] = '<img src="'.$this->EE->cp->cp_theme_url.'images/pagination_last_button.gif" width="13" height="13" alt="&gt; &gt;" />';

		return $config;
	}

	/**
	 * Half ass attempt at license verification.
	 * @param string $license
	 */
	public function valid_license($license)
	{
		//return TRUE; //if you want to disable the check uncomment this line. You should pay me though eric@mithra62.com :) 
		return preg_match("/^([a-z0-9]{8})-([a-z0-9]{4})-([a-z0-9]{4})-([a-z0-9]{4})-([a-z0-9]{12})$/", $license);
	}	
	
	/**
	 * Returns the setting array and caches it if none exists
	 */
	public function get_settings()
	{
		if ( ! isset($this->EE->session->cache[__CLASS__]['settings']) )
		{
			$this->EE->session->cache[__CLASS__]['settings'] = $this->EE->ct_admin_settings->get_settings();
		}
	
		return $this->EE->session->cache[__CLASS__]['settings'];
	}	
	
	/**
	 * Forces an array to download as a csv file
	 * @param array $arr
	 * @param bool $keys_as_headers
	 * @param bool $file_name
	 */
	public function download_array(array $arr, $keys_as_headers = TRUE, $file_name = 'download.txt')
	{
		if(!$this->disable_download)
		{
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=\"$file_name\"");
		}
		
		$this->EE->load->library('Export_xls');
		echo $this->EE->export_xls->create($arr, TRUE);
		exit;
	}	
	
	public function escape_csv_value($value, $delim = ',') 
	{
		$value = str_replace('"', '""', $value);
		if(preg_match('/'.$delim.'/', $value) or preg_match("/\n/", $value) or preg_match('/"/', $value))
		{ 
			return '"'.$value.'"'; 
		} 
		else 
		{
			return $value; 
		}
	}	
	
	public function is_installed_module($module_name)
	{
		$data = $this->EE->db->select('module_name')->from('modules')->like('module_name', $module_name)->get();
		if($data->num_rows == '1')
		{
			return TRUE;
		}
	}
	
	public function get_template_options()
	{
		if ( ! isset($this->EE->session->cache[__CLASS__]['template_options']))
		{
			$this->EE->load->model('template_model');
			$query = $this->EE->template_model->get_templates();
			$this->EE->session->cache[__CLASS__]['template_options'][] = '';
			
			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $template)
				{
					$this->EE->session->cache[__CLASS__]['template_options'][$template->template_id] = $template->group_name.'/'.$template->template_name;
				}
			}			
		}
		
		return $this->EE->session->cache[__CLASS__]['template_options'];
	}
	
	/**
	 * Handles the logic for crafting the Invoice or Packing Slip URLs
	 * @param int $order_id
	 * @param string $packingslip
	 * @return string
	 */
	public function get_invoice_url($order_id, $packingslip = FALSE)
	{
		$key = 'invoice_template_url';
		$tail = '';
		if($packingslip)
		{
			$key = 'packingslip_template_url';
			$tail = '&packing_slip=1';
		}
		
		if($this->settings[$key] != '0')
		{ 
			$options = $this->get_template_options();
			
			//clean up the end for slashes
			$url = $this->EE->config->config['site_url'];
			if($url[strlen($url)-1] != '/')
			{
				$url = $url.'/';
			}

			$index_page = $this->EE->config->config['site_index'];
			if($index_page != '/' && $index_page != '')
			{
				if($url[strlen($index_page)-1] != '/')
				{
					$index_page = $index_page.'/';
				}
				$url = $url.$index_page;
			}
			
			if(isset($options[$this->settings[$key]]))
			{
				return $url.$options[$this->settings[$key]].'/'.$order_id;	
			}
		}
		
		return $this->url_base.'order_view&id='.$order_id.'&print_invoice=1'.$tail;
	}
	
	/**
	 * Shitty little utility method for handling EE's dumb JSON handling
	 * @param mixed $j_response
	 * @return string
	 */
	public function json($j_response)
	{
		if(function_exists('json_encode'))	
		{
			return json_encode($j_response);
		}
		
		return $this->EE->javascript->generate_json($j_response, TRUE);
	}
}