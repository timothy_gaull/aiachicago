<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Carts Library
 *
 * Wrapper for the various CartThrob Cart functionality
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/libraries/ct_admin_carts.php
 */
class Ct_admin_carts
{	
	/**
	 * Set it up!
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->library('ct_admin_lib');
		$this->EE->load->library('ct_admin_channel_data');
		$this->EE->load->library('ct_admin_orders');
		$this->EE->load->model('product_model');
		$this->EE->load->model('ct_admin_carts_model', 'ct_admin_carts_model', TRUE);
		
		$this->channel_ids = $this->EE->cartthrob->store->config('product_channels');
		$this->purchased_items_channel = $this->EE->cartthrob->store->config('purchased_items_channel'); 
		$this->settings = $this->EE->ct_admin_lib->get_settings();
		$this->config = $this->EE->cartthrob->store->config();
		$this->dbprefix = $this->EE->db->dbprefix;
		//$this->channel_data = $this->EE->channel_model->get_channel_info($this->channel_id)->row();
	}
	
	/**
	 * Returns the carts data for a product based on $where
	 * @param array $where
	 * @param number $limit
	 * @return array
	 */
	public function get_product_carts(array $where, $limit = 20)
	{
		$this->EE->load->library('ct_admin_customers');
		$cust_email_key = $this->EE->db->escape_str($this->EE->cartthrob->store->config('orders_customer_email'));
		$cust_last_name_key = $this->EE->db->escape_str($this->EE->cartthrob->store->config('orders_billing_last_name'));
		$cust_first_name_key = $this->EE->db->escape_str($this->EE->cartthrob->store->config('orders_billing_first_name'));
				
		$this->EE->db->select('
				SUM(cart.quantity) AS total_in_cart,
				SUM(cart.price*cart.quantity) AS potential_revenue,
				MIN(created_date) AS first_added, 
				MAX(created_date) last_added, 
				m.email, m.member_id, m.username, m.screen_name'
		)->from('ct_admin_cart_items cart');
		
		foreach($where AS $key => $value)
		{
			$this->EE->db->where($key, $value);
		}
		
		$this->EE->db->join('members m', 'cart.member_id = m.member_id', 'left');
		$this->EE->db->group_by('m.email');
		$this->EE->db->limit($limit);
		$data = $this->EE->db->get();
		$carts = $data->result_array();
		return $carts;
	}
	
	/**
	 * Returns all the carts for a given member
	 * @param array $where
	 * @param number $limit
	 * @return Ambigous <multitype:, unknown>
	 */
	public function get_customer_cart_items(array $where, $limit = 20)
	{
		$arr = array();
		$carts = $this->EE->ct_admin_carts_model->get_items($where);
		foreach($carts AS $cart)
		{
			$arr[$cart['cart_id']][] = $cart;
		}
		
		return $arr;
	}
	
	/**
	 * Wrapper to handle the add to cart injection
	 * @param Cartthrob_item_product $item
	 */
	public function save_add_to_cart($item)
	{	
		//seed things up
		$data = $item->to_array();
		if(isset($this->EE->session->userdata['member_id']) && $this->EE->session->userdata['member_id'] >= '1')
		{
			$data['member_id'] = $this->EE->session->userdata['member_id'];
		}
		
		$data['price'] = $item->price();
		$data['weight'] = $item->weight();
		$data['shipping'] = $item->shipping();
		$this->EE->ct_admin_carts_model->add_item($data);
	}
		
}