<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Product Library
 *
 * Wrapper for the various CartThrob Product functionality
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/libraries/ct_admin_products.php
 */
class Ct_admin_products
{	
	
	/**
	 * A mapping of the CartThrob data model with EE
	 * @var array
	 */
	public $ct_product_data_map = array(
		'price' => 'price',
		'shipping' => 'shipping',
		'weight' => 'weight',
		'global_price' => 'global_price',
		'inventory' => 'inventory'
	);
		
	/**
	 * The Product channel ids (note the plural!)
	 * @var array
	 */
	public $channel_ids;
	
	/**
	 * Channel where purchased product details are stored
	 * @var int
	 */
	public $purchased_items_channel;
	
	/**
	 * The CartThrob specific settings
	 * @var array
	 */
	public $config;
	
	/**
	 * The database prefix variable
	 * @var string
	 */
	public $dbprefix;
	
	/**
	 * CT Admin specific settings
	 * @var array
	 */
	public $settings;
	
	/**
	 * Set it all up
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->library('ct_admin_channel_data');
		$this->EE->load->library('ct_admin_orders');
		$this->EE->load->model('product_model');
		
		$this->channel_ids = $this->EE->cartthrob->store->config('product_channels');
		$this->purchased_items_channel = $this->EE->cartthrob->store->config('purchased_items_channel'); 
		$this->settings = $this->EE->ct_admin_lib->get_settings();
		$this->config = $this->EE->cartthrob->store->config();
		$this->dbprefix = $this->EE->db->dbprefix;
	}

	/**
	 * Utiltity to only return products a user can view stats for
	 * @param int $author_id
	 * @return array
	 */
	public function get_author_product_ids($author_id)
	{
		if ( ! isset($this->EE->session->cache[__CLASS__]['settings']))
		{		
			$data = $this->EE->db->select(array('entry_id'))
								->from('channel_titles')
								->where(array('author_id' => $author_id))
								->where("channel_id IN(".implode(',',$this->channel_ids).")")
								->get()->result_array();
			$return = array();
			foreach($data AS $item)
			{
				$return[] = $item['entry_id'];
			}
			
			$this->EE->session->cache[__CLASS__][__METHOD__] = $return;
		}
			
		return $this->EE->session->cache[__CLASS__][__METHOD__];
	}
	
	/**
	 * Returns the amount of orders in the system
	 * @param string	$where
	 * @return int
	 */
	public function get_total_items_sold($where = FALSE)
	{
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		$sql = "SELECT COUNT(ect.entry_id) AS total
				FROM ".$this->dbprefix."cartthrob_order_items ecoi, ".$this->dbprefix."channel_titles ect
				WHERE
				ect.entry_id = ecoi.order_id
				$where
		";
		$data = $this->EE->db->query($sql)->result_array();
		if(count($data) == '0')
		{
			return '0';
		}
	
		if($data && is_array($data) && array_key_exists('0', $data))
		{
			if(isset($data['0']['total']))
			{
				return $data['0']['total'];
			}
		}
	}

	/**
	 * Computes the total sold based on $where
	 * @param string $where
	 * @return string
	 */
	public function get_total_gross_sales($where = FALSE)
	{
		$sql = "SELECT SUM( price ) AS total
				FROM ".$this->dbprefix."cartthrob_order_items ecoi, ".$this->dbprefix."channel_titles ect
				WHERE ect.entry_id = ecoi.order_id
				$where
		";
		
		$data = $this->EE->db->query($sql)->result_array();
		if(count($data) == '0')
		{
			return '0';
		}	

		if($data && is_array($data) && array_key_exists('0', $data))
		{
			if(isset($data['0']['total']) && $data['0']['total'] != '')
			{
				return $data['0']['total'];
			}
		}	
		return '0';	
	}
	
	/**
	 * Returns all the purchased products based on $where
	 * @param string $where
	 * @return Ambigous <number, unknown>
	 */
	public function get_purchased_products($where = FALSE)
	{
		$sql = "SELECT ecoi.title AS product_title, ecoi.*, ect.*, ecd.*
				FROM ".$this->dbprefix."cartthrob_order_items ecoi, ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				WHERE ect.entry_id = ecoi.order_id
				AND ecd.entry_id = ect.entry_id
				$where
		";	

		$data = $this->EE->db->query($sql)->result_array();
		if($data && is_array($data) && array_key_exists('0', $data))
		{
			$data = $this->EE->ct_admin_channel_data->_translate_custom_fields($data, $this->EE->ct_admin_orders->channel_id);
		}	

		//now some cleanup
		$keys = array();
		foreach($data AS $key => $value)
		{
			$data[$key]['combined_price'] = $value['quantity']*$value['price'];
			$keys[] = $value['order_id'];
		}

		$product_license_key = $this->EE->cartthrob->store->config('purchased_items_license_number_field');
		if(count($keys) >= '1' && $product_license_key)
		{
			$key = 'field_id_'.$product_license_key;
			$sql = "SELECT $key AS product_license_key, entry_id
					FROM ".$this->dbprefix."channel_data ecd
					WHERE
					ecd.entry_id IN(".implode(',',$keys).")
			";
			$licenses = $this->EE->db->query($sql)->result_array();	
			foreach($data AS $key => $value)
			{
				foreach($licenses AS $l_key => $license)
				{
					if($value['entry_id'] == $license['entry_id'])
					{
						$data[$key]['product_license_key'] = $license['product_license_key'];
					}
				}
			}
		}
		
		return $data;
	}
	
	/**
	 * Returns the amount of products in the system
	 * @param string	$where
	 */
	public function get_total_products($where = FALSE)
	{
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		$sql = "SELECT COUNT( DISTINCT(coi.entry_id)) AS total
				FROM ".$this->dbprefix."channel_titles ct, ".$this->dbprefix."cartthrob_order_items coi, ".$this->dbprefix."channel_data cd
				WHERE ct.entry_id = coi.entry_id
				AND ct.entry_id = cd.entry_id				
					$where
		";

		$data = $this->EE->db->query($sql)->result_array();
		if($data && is_array($data) && array_key_exists('0', $data))
		{
			if(isset($data['0']['total']))
			{
				return $data['0']['total'];
			}
		}
	}
	
	/**
	 * Returns an array of the statuses for the product channel
	 * @return array
	 */
	public function get_channel_statuses()
	{
		$channel_info = $this->EE->channel_model->get_channel_info($this->channel_ids['0']);
		$status_group = FALSE;
		foreach($channel_info->result_array() AS $row)
		{
			$status_group = $row['status_group'];
			break;
		}
		
		if($status_group)
		{
			return $this->EE->channel_model->get_channel_statuses($status_group)->result_array();
		}
	}	
	
	/**
	 * Grabs the products
	 * @param int $per_page
	 * @param int $row_num
	 * @param string $order
	 * @param string $where
	 * @return array
	 */
	public function get_products($per_page, $row_num, $order = 'order_total DESC', $where = FALSE)
	{
		//EE stores everything as tinytext so we have to cast the order_total so it'll sort properly
		$parts = explode(' ', $order);
		$direction = $parts['1'];
		$order = ($parts['0'] == 'order_total' ? 'CAST(order_total  AS UNSIGNED)' : $parts['0']);

		//now we can run it
		$sql = "SELECT ct.entry_id, ct.title,
				ct.status,
				entry_date,
				SUM(coi.quantity) AS total_sold, SUM(quantity*price) total_sales,
				AVG(price) AS average_paid_price
				FROM ".$this->dbprefix."channel_titles ct, ".$this->dbprefix."cartthrob_order_items coi, ".$this->dbprefix."channel_data cd
				WHERE ct.entry_id = coi.entry_id
				AND ct.entry_id = cd.entry_id
				$where
				GROUP BY ct.entry_id
				ORDER BY $order $direction
				LIMIT $row_num, $per_page
		";

		return $this->EE->db->query($sql)->result_array();		
	}
	
	/**
	 * Grabs the first date an order was entered
	 * @param string $format
	 * @return multitype:NULL
	 */
	public function get_first_date($where = FALSE)
	{
		$sql = "SELECT month, year, day FROM ".$this->dbprefix."channel_titles ct WHERE entry_id >= '1' $where ORDER BY entry_date ASC LIMIT 1";
		$data = $this->EE->db->query($sql)->row();
		if(!is_array($data))	
		{
			return array('month' => $data->month, 'year' => $data->year, 'day' => $data->day);
		}
	}
	
	/**
	 * Creates the Channel Options list for Product Channels
	 * @return array
	 */
	public function get_channel_options()
	{
		$data = $this->EE->db->select(array('channel_id', 'channel_title'))->from('channels')->where_in('channel_id', $this->channel_ids)->get();
		$data = $data->result_array();
		$options = array();
		foreach($data AS $key => $value)
		{
			$options[$value['channel_id']] = $value['channel_title'];
		}
		
		return $options;		
	}
	
	/**
	 * View for the Order datatable JSON
	 * @param int $perpage
	 * @param string $url_base
	 */
	public function json_ordering($perpage, $url_base)
	{
	
		//error_reporting(0);
		$this->EE->output->enable_profiler(FALSE);
		$this->EE->load->helper('text');
	
		$col_map = array('title', 'status', 'entry_date', 'total_sold', 'average_paid_price', 'total_sales');
		$id = ($this->EE->input->get_post('id')) ? $this->EE->input->get_post('id') : '';
		$keywords = ($this->EE->input->get_post('order_keywords')) ? $this->EE->input->get_post('order_keywords') : FALSE;
		$status = ($this->EE->input->get_post('f_status') && $this->EE->input->get_post('f_status') != '') ? $this->EE->input->get_post('f_status') : FALSE;
		$date_range = ($this->EE->input->get_post('date_range') && $this->EE->input->get_post('date_range') != '') ? $this->EE->input->get_post('date_range') : FALSE;
		$keyword = ($this->EE->input->get_post('k_search') && $this->EE->input->get_post('k_search') != '') ? $this->EE->input->get_post('k_search') : FALSE;
		$total_range = ($this->EE->input->get_post('total_range') && $this->EE->input->get_post('total_range') != '') ? $this->EE->input->get_post('total_range') : FALSE;
		$channel_id = ($this->EE->input->get_post('channel_id') && $this->EE->input->get_post('channel_id') != '') ? $this->EE->input->get_post('channel_id') : FALSE;
			
		$perpage = ($this->EE->input->get_post('perpage')) ? $this->EE->input->get_post('perpage') : $this->settings['products_list_limit'];
		$offset = ($this->EE->input->get_post('iDisplayStart')) ? $this->EE->input->get_post('iDisplayStart') : 0; // Display start point
		$sEcho = $this->EE->input->get_post('sEcho');
	
		$order = array();
	
		if ($this->EE->input->get('iSortCol_0') !== FALSE)
		{
			for ( $i=0; $i < $this->EE->input->get('iSortingCols'); $i++ )
			{
				if (isset($col_map[$this->EE->input->get('iSortCol_'.$i)]))
				{
					$order[$col_map[$this->EE->input->get('iSortCol_'.$i)]] = ($this->EE->input->get('sSortDir_'.$i) == 'asc') ? 'asc' : 'desc';
				}
			}
		}
	
		$tdata = array();
		$i = 0;
	
		if (count($order) == 0)
		{
			$order = 'entry_date DESC';
		}
		else
		{
			$sort = '';
			foreach($order AS $key => $value)
			{
				$sort = $key.' '.$value;
			}
			$order = $sort;
		}
	
		$where = $this->_build_filter_where($keyword, $date_range, $status, $total_range, $channel_id);

		$total = $this->get_total_products();
		$j_response['sEcho'] = $sEcho;
		$j_response['iTotalRecords'] = $total;
		$j_response['iTotalDisplayRecords'] = $this->get_total_products($where);

		$data = $this->get_products($perpage, $offset, $order, $where);
		$order_channel_statuses = $this->get_channel_statuses();
	
		$statuses = array();
		foreach($order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = $status['status'];
		}
	
		foreach ($data as $item)
		{
			$m[] = '<a href="'.$url_base.'product_view'.AMP.'id='.$item['entry_id'].'">'.$item['title'].'</a>';
			$m[] = '<span style="color:#'.m62_status_color($item['status'], $order_channel_statuses).'; cursor:pointer;" class="order_status_td" id="order_status_td_'.$item['entry_id'].'" rel="'.$item['entry_id'].'">'.lang($item['status']).'</span>'.form_dropdown('status', $statuses, $item['status'], 'id="change_status_'.$item['entry_id'].'" rel="'.$item['entry_id'].'" class="aj_order_status_change" style="display:none;"');
			$m[] = m62_format_date($item['entry_date'], FALSE, TRUE);
			$m[] = $item['total_sold'];
			$m[] = m62_format_money($item['average_paid_price']);
			$m[] = m62_format_money($item['total_sales']);
			//$m[] = '';
			//$m[] = '<input class="toggle" id="edit_box_'.$item['entry_id'].'" type="checkbox" name="toggle[]" value="'.$item['entry_id'].'" />';
	
			$tdata[$i] = $m;
			$i++;
			unset($m);
		}
	
		$j_response['aaData'] = $tdata;
		return $this->EE->ct_admin_lib->json($j_response);
	}

	/**
	 * Wrapper to abstract the compilation of the SQL WHERE clauses
	 * @param string $keyword
	 * @param string $date_range
	 * @param string $status
	 * @param string $total_range
	 * @param string $channel_id
	 * @return string
	 */
	public function _build_filter_where($keyword = FALSE, $date_range = FALSE, $status = FALSE, $total_range = FALSE, $channel_id = FALSE)
	{
		$where = '';
		if($status)
		{
			$where .= " AND status = '".$status."'";
		}
	
		if($date_range && $date_range != 'custom_date')
		{
			if(is_numeric($date_range))
			{
				$where .= " AND entry_date > ".(mktime()-($date_range*24*60*60));
			}
			else
			{
				$parts = explode('to', $date_range);
				if(count($parts) == '2')
				{
					$start = strtotime($parts['0']);
					$end = strtotime($parts['1']);
					$where .= " AND entry_date BETWEEN $start AND $end";
				}
			}
		}
		
		if($channel_id)
		{
			$where .= " AND ct.channel_id = '$channel_id'";
		}
	
		if($keyword && $channel_id)
		{
			$cols = array();
			$channel_data = $this->EE->channel_model->get_channel_info($channel_id)->row_array();
			$channel_fields = $this->EE->channel_model->get_channel_fields($channel_data['field_group'])->result_array();
			foreach($channel_fields AS $key => $value)
			{
				$check = $value['field_id'];
				if($check != '')
				{
					$cols[] = "field_id_$check LIKE '%$keyword%'";
				}
			}
				
			$cols[] = "coi.title LIKE '%$keyword%'";
	
			if(count($cols) >= 1)
			{
				$where .= " AND (".implode(' OR ', $cols).") ";
			}
		}
	
		if($total_range)
		{
			$parts = explode("-", $total_range);
			if(count($parts) == '2')
			{
				$parts = array_map('trim', $parts);
				if(isset($parts['0']) && is_numeric($parts['0']) && isset($parts['1']) && is_numeric($parts['1']))
				{
					$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
					$where .= " AND CAST(field_id_$orders_total_key  AS UNSIGNED) BETWEEN '".$parts['0']."' AND '".$parts['1']."'";
				}
			}
		}
	
		return $where;
	}	
	
	/**
	 * Returns a single product
	 * @param int $product_id
	 * @param string $where
	 * @return array
	 */
	public function get_product($product_id, $where = FALSE)
	{
		$sql = "SELECT ct.entry_id, ct.title,
				ct.status,
				entry_date,
				ct.channel_id,
				SUM(quantity) AS total_sold, SUM(quantity*price) AS total_sales,
				AVG(price) AS average_paid_price
				FROM ".$this->dbprefix."channel_titles ct, ".$this->dbprefix."cartthrob_order_items coi
				WHERE ct.entry_id = coi.entry_id
				AND ct.entry_id = '$product_id'
				$where
				GROUP BY ct.entry_id
				LIMIT 1
		";
		
		$product = $this->EE->db->query($sql)->row_array();		
		$product = array_merge($product, $this->EE->product_model->get_product($product_id));
		
		$defaults = array('average_paid_price' => '0', 'total_sold' => '0', 'total_sales' => '0');
		foreach($defaults AS $key => $value)
		{
			if( empty($product[$key]) )
			{
				$product[$key] = $value;
			}
		}
		
		return $product;
	}
	
	/**
	 * Returns the exported product data
	 * @param string $where
	 * @return array
	 */
	public function get_export_products($where = FALSE)
	{
		$status = ($this->EE->input->get_post('f_status') && $this->EE->input->get_post('f_status') != '') ? $this->EE->input->get_post('f_status') : FALSE;
		$date_range = ($this->EE->input->get_post('date_range') && $this->EE->input->get_post('date_range') != '') ? $this->EE->input->get_post('date_range') : FALSE;
		$keyword = ($this->EE->input->get_post('product_keywords') && $this->EE->input->get_post('product_keywords') != '') ? $this->EE->input->get_post('product_keywords') : FALSE;
		$channel_id = ($this->EE->input->get_post('channel_id') && $this->EE->input->get_post('channel_id') != '') ? $this->EE->input->get_post('channel_id') : FALSE;
	
		$where .= $this->_build_filter_where($keyword, $date_range, $status, FALSE, $channel_id);
		$sql = "SELECT ".$this->settings['product_export_select']."
				FROM ".$this->dbprefix."channel_titles ct, ".$this->dbprefix."cartthrob_order_items coi, ".$this->dbprefix."channel_data cd
						WHERE ct.entry_id = coi.entry_id
						AND ct.entry_id = cd.entry_id
						$where
						GROUP BY ct.entry_id
		";
		
		$data = $this->EE->db->query($sql)->result_array();
		return $data;
	}	
	

	/**
	 * Creates the Product menu for the view script
	 * @param string $product_id
	 * @param array $product_data
	 * @return multitype:multitype:string  multitype:string unknown
	 */
	public function get_product_view_menu($product_id, array $product_data)
	{
		$publishview_url = BASE.'&C=content_publish&M=entry_form&channel_id='.$product_data['channel_id'].'&entry_id='.$product_id;
		$menu = array(
				'edit_in_publish_view' => array('url' => $publishview_url, 'target' => ''),
		);
	
		if ($this->EE->extensions->active_hook('ct_admin_modify_product_menu') === TRUE)
		{
			$menu = $this->EE->extensions->call('ct_admin_modify_product_menu', $menu);
			if ($this->EE->extensions->end_script === TRUE) return $menu;
		}
	
		return $menu;
	}
	
	/**
	 * Parses the product sum report data and sets up the details
	 * @param array $data
	 * @return multitype:string mixed
	 */
	public function setup_meta(array $data)
	{
		$return = array();
		$first_purchase = FALSE;
		$last_purchase = FALSE;
		foreach($data AS $key => $value)
		{
			if( ! $last_purchase )
			{
				$last_purchase = $value['last_purchase'];
				$first_purchase = $value['first_purchase'];
				continue;
			}
			
			if($value['first_purchase'] <= $first_purchase)
			{
				$first_purchase = $value['first_purchase'];
			}
			
			if($value['last_purchase'] >= $last_purchase)
			{
				$last_purchase = $value['last_purchase'];
			}			
		}
		
		$return['first_order']['entry_date'] = $first_purchase;
		$return['last_order']['entry_date'] = $last_purchase;
		return $return;		
	}
	
	/**
	 * Wrapper to allow for the primary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_product_hook_view(array $product_details)
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_product_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_product_view', $product_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}
	
	/**
	 * Wrapper to allow for the secondary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_product_hook_secondary_view(array $product_details)
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_product_secondary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_product_secondary_view', $product_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}

	/**
	 * Wrapper to allow for the tertiary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_product_hook_tertiary_view(array $product_details)
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_product_tertiary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_product_tertiary_view', $product_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}	
	
	/**
	 * Formats the products data into a standard format
	 * @param array $products
	 * @return multitype:array
	 */
	public function format_products($products)
	{
		foreach($products AS $key => $item)
		{
			if(isset($item['extra']) && $item['extra'] != '')
			{
				$products[$key]['extra'] = @unserialize(base64_decode($item['extra']));
				if( is_array($products[$key]['extra']) )
				{
					//setup the option keys and default values for later use in teh views
					$keys = array();
					foreach($products[$key]['extra'] AS $option_key => $option_value)
					{
						if($option_key != 'sub_items')
						{
							$keys[$option_key] = $option_value;
						}
					}
					$products[$key]['extra']['_option_keys'] = $keys;
				}
			}
		}
		
		return $products;
	}
		
	private function parse_ct_product_data($product)
	{
		$arr = array();
		foreach($this->ct_product_data_map AS $key => $value)
		{
			if(isset($this->config[$value]))
			{
				$_key = $this->_make_key($this->config[$value]);
				if($_key)
				{
					$arr[$key] = $product[$_key];
				}
				else
				{
					$arr[$key] = '';
				}
			}
			else
			{
				$arr[$key] = '';
			}
		}
	
		return array_merge($product, $arr);
	}	
}