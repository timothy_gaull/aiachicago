<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Order Library
 *
 * Wrapper for the various CartThrob Order functionality
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/libraries/ct_admin_orders.php
 */
class Ct_admin_orders
{	
	/**
	 * A mapping of the EE fields we'll be using for orders
	 * @var array
	 */
	public $ee_data_map = array(
		'entry_id' => 'entry_id', 
		'title' => 'title', 
		'status' => 'status', 
		'item_count' => 'item_count',
		'product_count' => 'product_count',
		'entry_date' => 'entry_date'
	);	
	
	/**
	 * A mapping of the CartThrob data model with EE
	 * @var array
	 */
	public $ct_order_data_map = array(
		//'order_items' => 'orders_items_field',
		'order_total' => 'orders_total_field', 
		'order_subtotal' => 'orders_subtotal_field', 
		'order_shipping' => 'orders_shipping_field', 
		'order_coupons' => 'orders_coupon_codes', 
		'order_tax' => 'orders_tax_field', 
		'order_shipping_option' => 'orders_shipping_option', 
		'order_error' => 'orders_error_message_field',
		'order_language' => 'orders_language_field',
		'order_discount' => 'orders_discount_field', 
		'orders_customer_email' => 'orders_customer_email', 
		'order_customer_full_name' => 'order_customer_full_name', 
		'order_customer_phone' => 'order_customer_phone', 
		'order_transaction_id' => 'orders_transaction_id', 
		'order_last_four' => 'orders_last_four_digits', 
		'order_ip_address' => 'order_ip_address', 
		'orders_billing_first_name' => 'orders_billing_first_name', 
		'orders_billing_last_name' => 'orders_billing_last_name', 
		'orders_billing_address' => 'orders_billing_address', 
		'orders_billing_address2' => 'orders_billing_address2', 
		'orders_billing_city' => 'orders_billing_city', 
		'orders_billing_state' => 'orders_billing_state', 
		'orders_billing_zip' => 'orders_billing_zip', 
		'orders_billing_country' => 'orders_billing_country', 
		'orders_country_code' => 'orders_country_code', 
		'orders_billing_company' => 'orders_billing_company', 
		'orders_shipping_first_name' => 'orders_shipping_first_name', 
		'orders_shipping_last_name' => 'orders_shipping_last_name', 
		'orders_shipping_address' => 'orders_shipping_address', 
		'orders_shipping_address2' => 'orders_shipping_address2', 
		'orders_shipping_city' => 'orders_shipping_city', 
		'orders_shipping_state' => 'orders_shipping_state', 
		'orders_shipping_zip' => 'orders_shipping_zip',
		'orders_shipping_country' => 'orders_shipping_country', 
		'orders_shipping_country_code' => 'orders_shipping_country_code', 
		'orders_shipping_company' => 'orders_shipping_company'
	);	
	
	/**
	 * Set it up!
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->model('channel_model');
		$this->channel_id = $this->EE->cartthrob->store->config('orders_channel');
		$this->settings = $this->EE->ct_admin_lib->get_settings();
		$this->config = $this->EE->cartthrob->store->config();
		$this->dbprefix = $this->EE->db->dbprefix;
		$this->channel_data = $this->EE->channel_model->get_channel_info($this->channel_id)->row();
		$this->channel_fields = $this->EE->channel_model->get_channel_fields($this->channel_data->field_group)->result_array();
		
		$this->EE->load->library('Ct_admin_products');
		if( $this->settings['set_sql_big_selects'] )
		{
			//we do lots of JOINS and some systems don't like that but this can fix them
			$this->EE->db->query("SET SQL_BIG_SELECTS=1"); 
		}
	}
	
	/**
	 * Grabs the first date an order was entered
	 * @param string $format
	 * @return multitype:NULL
	 */
	public function get_first_date($format = FALSE)
	{
		$sql = "SELECT month, year, day FROM ".$this->dbprefix."channel_titles ect WHERE channel_id = '".$this->channel_id."' ORDER BY entry_date ASC LIMIT 1";
		$data = $this->EE->db->query($sql)->row();
		if(!is_array($data))	
		{
			return array('month' => $data->month, 'year' => $data->year, 'day' => $data->day);
		}
	}
	
	/**
	 * Returns the status based on $where
	 * @param array $where
	 * @return boolean
	 */
	public function get_order_status(array $where)
	{
		$data = $this->EE->db->select(array('status'))->from('channel_titles')->where($where)->get();
		if($data->num_rows != '1')
		{
			return FALSE;
		}
		
		$data = $data->row();
		return $data->status;	
	}
	
	/**
	 * Returns an array with the highest and lowest orders
	 * @return multitype:NULL
	 */
	public function get_minmax_orders()
	{
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		$sql = "SELECT MIN(CAST(field_id_$orders_total_key AS UNSIGNED)) AS min_order, 
					   MAX(CAST(field_id_$orders_total_key AS UNSIGNED)) AS max_order
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				
				WHERE ect.entry_id = ecd.entry_id 
				AND ecd.channel_id = '".$this->channel_id."'
		";
		$data = $this->EE->db->query($sql)->row();
		
		$return = array('min_order' => $data->min_order, 'max_order' => $data->max_order);
		return $return;
	}
	
	/**
	 * Returns an array of the statuses for the order channel
	 * @return array
	 */
	public function get_channel_statuses()
	{
		$channel_info = $this->EE->channel_model->get_channel_info($this->channel_id);
		$status_group = FALSE;
		foreach($channel_info->result_array() AS $row)
		{
			$status_group = $row['status_group'];
			break;
		}
		if($status_group)
		{
			return $this->EE->channel_model->get_channel_statuses($status_group)->result_array();
		}		
	}
	
	/**
	 * Wrapper to handle order retrieval
	 * @param int 		$per_page
	 * @param int 		$row_num
	 * @param string 	$order
	 * @param string	$where
	 */
	public function get_orders($per_page, $row_num, $order = 'entry_date DESC', $where = FALSE)
	{
		//EE stores everything as tinytext so we have to cast the order_total so it'll sort properly
		$parts = explode(' ', $order);
		$direction = $parts['1'];
		$order = ($parts['0'] == 'order_total' ? 'CAST(order_total  AS UNSIGNED)' : $parts['0']);

		//now we can run it
		$cust_email_key = $this->EE->cartthrob->store->config('orders_customer_email');
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		$cust_last_name_key = $this->EE->cartthrob->store->config('orders_billing_last_name');
		$cust_first_name_key = $this->EE->cartthrob->store->config('orders_billing_first_name');
		$sql = "SELECT ect.entry_id, ect.title, em.member_id,
				field_id_$cust_first_name_key AS first_name, 
				field_id_$cust_last_name_key AS last_name,
				field_id_$cust_email_key AS email,
				ect.status,
				field_id_$orders_total_key AS order_total,
				entry_date 
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				LEFT JOIN ".$this->dbprefix."members em ON field_id_$cust_email_key = em.email
				WHERE ect.entry_id = ecd.entry_id
				AND ecd.channel_id = '".$this->channel_id."'
				$where
				GROUP BY ect.entry_id
				ORDER BY $order $direction
				LIMIT $row_num, $per_page
		";
		return $this->EE->db->query($sql)->result_array();		
	}
	
	/**
	 * Returns the amount of orders in the system
	 * @param string	$where
	 */
	public function get_total_orders($where = FALSE)
	{
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		$sql = "SELECT COUNT(ect.entry_id) AS total
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				WHERE 
				ect.channel_id = '".$this->channel_id."'
				AND ecd.entry_id = ect.entry_id
				$where
		";		
		$data = $this->EE->db->query($sql)->result_array();
		if($data && is_array($data) && array_key_exists('0', $data))
		{
			if(isset($data['0']['total']))
			{
				return $data['0']['total'];
			}
		}
	}
	
	/**
	 * Returns the amount of money in the system
	 * @param string	$where
	 */
	public function get_total_sales($where = FALSE)
	{
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		if($orders_total_key == '')
		{
			return FALSE;
		}
		
		$sql = "SELECT SUM(field_id_$orders_total_key) AS total
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				WHERE ect.entry_id = ecd.entry_id 
				$where
				AND ecd.channel_id = '".$this->channel_id."'
		";
				
		$data = $this->EE->db->query($sql)->result_array();
		if($data && is_array($data) && array_key_exists('0', $data))
		{
			if(isset($data['0']['total']))
			{
				return $data['0']['total'];
			}
		}
	}	
	
	/**
	 * Returns the average price for an order
	 * @param string $where
	 * @return boolean|number
	 */
	public function get_average_order($where = FALSE)
	{
		$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
		if($orders_total_key == '')
		{
			return FALSE;
		}
		
		$sql = "SELECT AVG(field_id_$orders_total_key) AS average_order
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				WHERE ect.entry_id = ecd.entry_id 
				$where
				AND ecd.channel_id = '".$this->channel_id."'
		";		
		$data = $this->EE->db->query($sql)->result_array();
		if($data && is_array($data) && array_key_exists('0', $data))
		{
			if(isset($data['0']['average_order']))
			{
				return round($data['0']['average_order']);
			}
		}		
	}
	
	/**
	 * Wrapper to return an order 
	 * @param int $order_id
	 */
	public function get_order($order_id)
	{ 
		return $this->parse_ct_order_data($this->EE->order_model->get_order($order_id));	
	}
	
	/**
	 * Wrapper to update an order
	 * @param $order_id
	 * @param $data
	 */
	public function update_order($order_id, $data)
	{
		$notifications = $this->EE->cartthrob->store->config('notifications');
		$prev_details = $this->get_order($order_id);
		$update = $this->EE->order_model->update_order($order_id, $data);
		if($update)
		{				
			if($prev_details['status'] != $data['status'])
			{		
				$data['entry_id'] = $data['order_id'];
				$data['previous_status'] = $prev_details['status'];
				
				//janky but we have to make up some data for the notification
				$this->EE->db->select('channel_id, url_title')->where('entry_id', $data['order_id']); ;
				$missing = $this->EE->db->get('channel_titles');
				if($missing->num_rows == '1')
				{
					$missing = $missing->row();
					$data['channel_id'] = $missing->channel_id;
					$data['url_title'] = $missing->url_title;
				}
				else
				{
					$data['channel_id'] = '';
					$data['url_title'] = '';
				}
				
				$this->send_status_notification($prev_details, $data);
			}
			
			return TRUE;
		}
	}
	
	/**
	 * Wrapper for the notifications to send on status change
	 * @param array $prev_details
	 * @param array $data
	 */
	public function send_status_notification($prev_details, $data)
	{
		$this->EE->load->library('cartthrob_emails');
		$emails = $this->EE->cartthrob_emails->get_email_for_event(NULL, $prev_details['status'],  $data['status']);
		if(count($emails) >= '1')
		{
			foreach($emails as $email_content)
			{
				if(!empty($email_content['to']) && $email_content['to']  == "{customer_email}")
				{
					if(!empty($prev_details['orders_customer_email']) && $prev_details['orders_customer_email'] != '')
					{
						$email_content['to'] = $prev_details['orders_customer_email'];
					}
				}
		
				if(!empty($email_content['from']) && $email_content['from']  == "{customer_email}")
				{
					if(!empty($prev_details['orders_customer_email']) && $prev_details['orders_customer_email'] != '')
					{
						$email_content['from'] = $prev_details['orders_customer_email'];
					}
				}
		
				if(!empty($email_content['from_name']) && $email_content['from_name']  == "{customer_name}")
				{
					if(!empty($prev_details['orders_billing_first_name']) && $prev_details['orders_billing_first_name'] != '' && !empty($prev_details['orders_billing_last_name']) && $prev_details['orders_billing_last_name'] != '')
					{
						$email_content['from_name'] = $prev_details['orders_billing_first_name'].' '.$prev_details['orders_billing_last_name'];
					}
				}
		
				$this->EE->cartthrob_emails->send_email($email_content, $data);
			}
		}		
	}
	
	/**
	 * Returns the items related to an order
	 * @param int $order_id
	 */
	public function get_order_items($order_id)
	{
		
		if ( ! is_array($order_id))
		{
			$this->EE->db->where('order_id', $order_id);
		}
		else
		{
			$this->EE->db->where_in('order_id', $order_id);
		}		
		
		$order_items = $this->EE->db->order_by('row_order', 'asc')->get('cartthrob_order_items')->result_array();
		$order_items = $this->EE->ct_admin_products->format_products($order_items);
		
		if ($this->EE->extensions->active_hook('ct_admin_modify_order_items') === TRUE)
		{
			$order_items = $this->EE->extensions->call('ct_admin_modify_order_items', $order_items);
			if ($this->EE->extensions->end_script === TRUE) return $order_items;
		}
		
		return $order_items;
	}
	
	/**
	 * Removes all the items from an order
	 * @param int $order_ids
	 */
	public function delete_order_items($order_ids)
	{
		if ($this->EE->extensions->active_hook('ct_admin_delete_order_items_pre') === TRUE)
		{
			$order_ids = $this->EE->extensions->call('ct_admin_delete_order_items_pre', $order_ids);
			if ($this->EE->extensions->end_script === TRUE) return $order_ids;
		}
				
		$this->EE->order_model->delete_order_items($order_ids);
		
		if ($this->EE->extensions->active_hook('ct_admin_delete_order_items_post') === TRUE)
		{
			$order_ids = $this->EE->extensions->call('ct_admin_delete_order_items_post', $order_ids);
			if ($this->EE->extensions->end_script === TRUE) return $order_ids;
		}		
	}
	
	/**
	 * Removes orders
	 * @param array $order_ids
	 */
	public function delete_orders($order_ids)
	{
		if(!is_array($order_ids) || count($order_ids) == '0')
		{
			return FALSE;
		}
		$this->EE->load->library('api');
		$this->EE->api->instantiate('channel_entries');	

		if ($this->EE->extensions->active_hook('ct_admin_delete_orders_pre') === TRUE)
		{
			$order_ids = $this->EE->extensions->call('ct_admin_delete_orders_pre', $order_ids);
			if ($this->EE->extensions->end_script === TRUE) return $order_ids;
		}
				
		$this->EE->api_channel_entries->delete_entry($order_ids);
		$this->delete_order_items($order_ids);

		if ($this->EE->extensions->active_hook('ct_admin_delete_orders_post') === TRUE)
		{
			$order_ids = $this->EE->extensions->call('ct_admin_delete_orders_post', $order_ids);
			if ($this->EE->extensions->end_script === TRUE) return $order_ids;
		}
				
		return TRUE;	
	}
	
	/**
	 * View for the Order datatable JSON
	 * @param int $perpage
	 * @param string $url_base
	 */
	public function json_ordering($perpage, $url_base)
	{

		$this->EE->output->enable_profiler(FALSE);
		$this->EE->load->helper('text');

		$col_map = array('title', 'first_name', 'status', 'entry_date', 'order_total');
		$id = ($this->EE->input->get_post('id')) ? $this->EE->input->get_post('id') : '';
		$keywords = ($this->EE->input->get_post('order_keywords')) ? $this->EE->input->get_post('order_keywords') : FALSE;
		$status = ($this->EE->input->get_post('f_status') && $this->EE->input->get_post('f_status') != '') ? $this->EE->input->get_post('f_status') : FALSE;
		$date_range = ($this->EE->input->get_post('date_range') && $this->EE->input->get_post('date_range') != '') ? $this->EE->input->get_post('date_range') : FALSE;
		$keyword = ($this->EE->input->get_post('k_search') && $this->EE->input->get_post('k_search') != '') ? $this->EE->input->get_post('k_search') : FALSE;
		$total_range = ($this->EE->input->get_post('total_range') && $this->EE->input->get_post('total_range') != '') ? $this->EE->input->get_post('total_range') : FALSE;
		$shipping_status = ($this->EE->input->get_post('shippable') && $this->EE->input->get_post('shippable') != '') ? $this->EE->input->get_post('shippable') : FALSE;

		$perpage = ($this->EE->input->get_post('perpage')) ? $this->EE->input->get_post('perpage') : $this->settings['orders_list_limit'];
		$offset = ($this->EE->input->get_post('iDisplayStart')) ? $this->EE->input->get_post('iDisplayStart') : 0; // Display start point
		$sEcho = $this->EE->input->get_post('sEcho');

		$order = array();
		
		if ($this->EE->input->get('iSortCol_0') !== FALSE)
		{
			for ( $i=0; $i < $this->EE->input->get('iSortingCols'); $i++ )
			{
				if (isset($col_map[$this->EE->input->get('iSortCol_'.$i)]))
				{
					$order[$col_map[$this->EE->input->get('iSortCol_'.$i)]] = ($this->EE->input->get('sSortDir_'.$i) == 'asc') ? 'asc' : 'desc';
				}
			}
		}

		$tdata = array();
		$i = 0;
		
		if (count($order) == 0)
		{
			$order = 'entry_date DESC';
		}
		else
		{
			$sort = '';
			foreach($order AS $key => $value)
			{
				$sort = $key.' '.$value;
			}
			$order = $sort;
		}

		$where = $this->_build_filter_where($keyword, $date_range, $status, $total_range, $shipping_status);
		
		$total = $this->get_total_orders();
		$j_response['sEcho'] = $sEcho;
		$j_response['iTotalRecords'] = $total;
		$j_response['iTotalDisplayRecords'] = $this->get_total_orders($where);
		
				
		$data = $this->get_orders($perpage, $offset, $order, $where);
		$order_channel_statuses = $this->get_channel_statuses();
		
		$statuses = array();
		foreacH($order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = $status['status'];
		}
		
		foreach ($data as $item)
		{			
			$m[] = '<a href="'.$url_base.'order_view'.AMP.'id='.$item['entry_id'].AMP.'lightbox=yes" class="fancybox_overlay"><img style="padding-right:5px; cursor:pointer;" rel="'.$item['entry_id'].'" src="'.$this->EE->config->config['theme_folder_url'].'/cp_themes/default/images/button_close_subadmin.gif" /></a> <a href="'.$url_base.'order_view'.AMP.'id='.$item['entry_id'].'">'.$item['title'].'</a>';

			if($item['email'] != '')
			{
				$m[] = '<a href="'.$url_base.'customer_view'.AMP.'email='.$item['email'].'">'.$item['first_name'].' '.$item['last_name'].'</a>';
			}
			else
			{
				$m[] = $item['first_name'].' '.$item['last_name'];
			}
			
			$m[] = '<span style="color:#'.m62_status_color($item['status'], $order_channel_statuses).'; cursor:pointer;" class="order_status_td" id="order_status_td_'.$item['entry_id'].'" rel="'.$item['entry_id'].'">'.lang($item['status']).'</span>'.form_dropdown('status', $statuses, $item['status'], 'id="change_status_'.$item['entry_id'].'" rel="'.$item['entry_id'].'" class="aj_order_status_change" style="display:none;"');
			$m[] = m62_format_date($item['entry_date'], FALSE, TRUE);
			$m[] = m62_format_money($item['order_total']);
			$m[] = '<input class="toggle export_toggle" id="edit_box_'.$item['entry_id'].'" type="checkbox" name="toggle[]" value="'.$item['entry_id'].'" />';		

			$tdata[$i] = $m;
			$i++;
			unset($m);
		}		

		$j_response['aaData'] = $tdata;	
		return $this->EE->ct_admin_lib->json($j_response);	
	}	
	
	/**
	 * Creates the array for exporting orders
	 * @return multitype:array
	 */
	public function get_export_orders()
	{
		$status = $this->EE->input->get_post('f_status');
		$date_range = $this->EE->input->get_post('date_range');
		$keyword = $this->EE->input->get_post('order_keywords');
		$total_range = $this->EE->input->get_post('total_range');
		$shipping_status = $this->EE->input->get_post('shippable');
		$order_ids = trim($this->EE->input->get_post('order_ids'));
		$where = $this->_build_filter_where($keyword, $date_range, $status, $total_range, $shipping_status);	
		
		//check if we're filtering by specifically checked entries
		if($order_ids && $order_ids != '')	
		{
			$order_ids = str_replace('|', ',', $order_ids);
			$where .= " AND ecd.entry_id IN($order_ids)";
		}
		
		$sql = "SELECT ".$this->settings['order_export_select']."
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd, ".$this->dbprefix."cartthrob_order_items ecoi
				WHERE 
				ect.entry_id = ecd.entry_id
				AND ecoi.order_id = ect.entry_id
				AND ecd.channel_id = '".$this->channel_id."'
				$where
				GROUP BY ecoi.order_id
				ORDER BY ect.entry_id DESC
		";
		
		$data = $this->EE->db->query($sql)->result_array();	
		$data = $this->EE->ct_admin_channel_data->_translate_custom_fields($data, $this->channel_id);
		
		//now we have to clean things up a bit so the export isn't retarded
		$i = 0;
		$arr = array();
		$channel_data = $this->EE->channel_model->get_channel_info($this->channel_id)->row();
		$channel_fields = $this->EE->channel_model->get_channel_fields($channel_data->field_group)->result_array();
		$fields = array('entry_id' => 'entry_id', 'title' => 'title', 'entry_date' => 'entry_date', 'ip_address');
		foreach($channel_fields AS $field)
		{
			$fields[$field['field_name']] = $field['field_name'];
		}
		
		foreach($data AS $order)
		{
			foreach($order AS $key => $value)
			{
				if(array_key_exists($key, $fields))
				{
					$arr[$i][$fields[$key]] = $value;
				}
			}
			$i++;
		}
		
		if ($this->EE->extensions->active_hook('ct_admin_modify_order_export_data') === TRUE)
		{
			$arr = $this->EE->extensions->call('ct_admin_modify_order_export_data', $arr);
			if ($this->EE->extensions->end_script === TRUE) return $arr;
		}		
		
		return $arr;
	}	
	
	private function parse_ct_order_data($customer)
	{	
		$arr = array();
		foreach($this->ee_data_map AS $key => $value)
		{
			if(isset($customer[$key]))
			{
				$arr[$key] = $customer[$value];
			}
			else
			{
				$arr[$key] = '';
			}
		}
				
		//check ct 
		foreach($this->ct_order_data_map AS $key => $value)
		{
			if(isset($this->config[$value]))
			{			
				$_key = $this->_make_key($this->config[$value]);
				if($_key)
				{
					$arr[$key] = $customer[$_key];
				}
				else
				{
					$arr[$key] = '';
				}
			}
			else
			{
				$arr[$key] = '';
			}				
		}

		return array_merge($customer, $arr);
	}

	public function _build_filter_where($keyword = FALSE, $date_range = FALSE, $status = FALSE, $total_range = FALSE, $shipping_status = FALSE)
	{
		$where = '';
		if($status)
		{
			$where .= " AND status = '".$status."'";
		}
		
		if($date_range && $date_range != 'custom_date')
		{
			if(is_numeric($date_range))
			{
				$where .= " AND entry_date > ".(mktime()-($date_range*24*60*60));
			}
			else
			{
				$parts = explode('to', $date_range);
				if(count($parts) == '2')
				{
					$start = strtotime($parts['0'].' 23:59:00');
					$end = strtotime($parts['1'].' 23:59:00');
					$where .= " AND entry_date BETWEEN $start AND $end";
				}
			}
		}
		
		if($keyword)
		{
			$cols = array();
			foreach($this->channel_fields AS $key => $value)
			{
				$check = $value['field_id'];
				if($check != '')
				{
					$cols[] = "field_id_$check LIKE '%$keyword%'";
				}
			}
			
			$cols[] = "ect.title LIKE '%$keyword%'";

			if(count($cols) >= 1)
			{
				$where .= " AND (".implode(' OR ', $cols).") ";
			}
		}
		
		if($total_range)
		{
			$parts = explode("-", $total_range);
			if(count($parts) == '2')
			{
				$parts = array_map('trim', $parts);
				if(isset($parts['0']) && is_numeric($parts['0']) && isset($parts['1']) && is_numeric($parts['1']))
				{
					$orders_total_key = $this->EE->cartthrob->store->config('orders_total_field');
					$where .= " AND CAST(field_id_$orders_total_key  AS UNSIGNED) BETWEEN '".$parts['0']."' AND '".$parts['1']."'";
				}
			}
		}
		
		if($shipping_status)
		{
			switch($shipping_status)
			{
				case 'virtual':
					$where .= " AND ect.entry_id IN(SELECT order_id FROM ".$this->dbprefix."cartthrob_order_items WHERE no_shipping = '1')";
				break;
				
				default:
					$where .= " AND ect.entry_id IN(SELECT order_id FROM ".$this->dbprefix."cartthrob_order_items WHERE no_shipping != '1')";
				break;
			}
			
		}
				
		return $where;
	}
	
	private function _make_key($key, $pre = '')
	{
		if($key == '')
		{
			return FALSE;
		}
		return $pre.'field_id_'.$key;
	}	
	
	/**
	 * Wrapper to update update an order's status
	 * @param array:string $order_ids
	 * @param string $status
	 * @return boolean
	 */
	public function upate_status($order_ids, $status)
	{
		if(!is_array($order_ids))
		{
			$order_ids = array($order_ids);
		}

		$data = array('status' => $status);
		foreach($order_ids AS $order_id)
		{
			$data['order_id'] = $order_id;
			$this->update_order($order_id, $data);
		}
		
		return TRUE;
	}
	
	/**
	 * Creates the Order menu for the view script
	 * @param string $order_id
	 * @param array $order_data
	 * @return multitype:multitype:string  multitype:string unknown
	 */
	public function get_order_view_menu($order_id, array $order_data)
	{
	
		$publishview_url = BASE.'&C=content_publish&M=entry_form&channel_id='.$this->EE->ct_admin_orders->channel_id.'&entry_id='.$order_id;
		$invoice_url = $this->EE->ct_admin_lib->get_invoice_url($order_id);
		$packingslip_url = $this->EE->ct_admin_lib->get_invoice_url($order_id, TRUE);
	
		$menu = array(
				'print_invoice' => array('url' => $invoice_url, 'target' => '_blank', 'div_class' => 'invoice_button'),
				'packing_slip' => array('url' => $packingslip_url, 'target' => '_blank', 'div_class' => 'packing_slip_button'),
				'edit_in_publish_view' => array('url' => $publishview_url, 'target' => '', 'div_class' => 'edit_in_publish_view_button'),
		);
	
		if ($this->EE->extensions->active_hook('ct_admin_modify_order_menu') === TRUE)
		{
			$menu = $this->EE->extensions->call('ct_admin_modify_order_menu', $menu);
			if ($this->EE->extensions->end_script === TRUE) return $menu;
		}
	
		return $menu;
	}	
	
	/**
	 * Creates the Order Details array for the view
	 * @param array $order_details
	 * @return array
	 */
	public function setup_order_details_view(array $order_details)
	{
		$return['order_transaction_id'] = '#'.$order_details['order_transaction_id'];
		$return['order_shipping'] = m62_format_money($order_details['order_shipping']);
		$return['order_coupons'] = ($order_details['order_coupons'] == '' ? 'N/A' : $order_details['order_coupons']);
		$return['order_shipping_option'] = ($order_details['order_shipping_option'] == '' ? 'N/A' : $order_details['order_shipping_option']);
		$return['order_discount'] = ($order_details['order_discount'] == '' ? 'N/A' : m62_format_money($order_details['order_discount']));
		$return['order_tax'] = m62_format_money($order_details['order_tax']);
		
		if ($this->EE->extensions->active_hook('ct_admin_modify_order_details_block') === TRUE)
		{
			$return = $this->EE->extensions->call('ct_admin_modify_order_details_block', $return, $order_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}		
		return ($return);
	}
	
	/**
	 * Wrapper to allow for the primary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_order_hook_view(array $order_details)
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_order_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_order_view', $order_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}	
	
	/**
	 * Wrapper to allow for the secondary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_order_hook_secondary_view(array $order_details)
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_order_secondary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_order_secondary_view', $order_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}

	/**
	 * Wrapper to allow for the secondary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_order_hook_tertiary_view(array $order_details)
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_order_tertiary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_order_tertiary_view', $order_details);
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}
}