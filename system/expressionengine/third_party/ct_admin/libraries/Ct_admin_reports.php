<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */

 /**
 * CT Admin - Reports
 *
 * Contains most wrapper methods for reporting
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/libraries/ct_admin_reports.php
 */
class Ct_admin_reports
{	
	/**
	 * Set it all up!
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		$this->EE->load->library('ct_admin_cache');
		$this->channel_id = $this->EE->cartthrob->store->config('orders_channel');
		$this->settings = $this->EE->ct_admin_lib->get_settings();
		$this->config = $this->EE->cartthrob->store->config();
		$this->dbprefix = $this->EE->db->dbprefix;		
	}
	
	/**
	 * Creates the Reports menu for the view script
	 * @return multitype:multitype:string  multitype:string unknown
	 */
	public function get_reports_view_menu()
	{
		$menu = array(
			'summary' => array('url' => 'reports', 'target' => '_self', 'div_class' => 'invoice_button'),
			'inventory' => array('url' => 'inventory_report', 'target' => '_self', 'div_class' => 'invoice_button'),
			'product_sum' => array('url' => 'product_sum_report', 'target' => '_self', 'div_class' => 'packing_slip_button'),
			'country' => array('url' => 'country_report', 'target' => '', 'div_class' => 'edit_in_publish_view_button')
		);
	
		if($this->settings['inventory_report_enable'] != '1')
		{
			unset($menu['inventory']);
		}
		
		if ($this->EE->extensions->active_hook('ct_admin_modify_reports_menu') === TRUE)
		{
			$menu = $this->EE->extensions->call('ct_admin_modify_reports_menu', $menu);
			if ($this->EE->extensions->end_script === TRUE) return $menu;
		}
	
		return $menu;
	}	
	
	/**
	 * Creates the Product Sum Report
	 * @param string $where
	 * @return array
	 */
	public function product_sum_report($where = FALSE)
	{
		$sql = "SELECT 
				SUM( price ) AS total_price, 
				ecoi.title, price, 
				SUM( quantity ) AS total_quantity, 
				ecoi.entry_id, ecoi.order_id, ect.url_title, 
				ecd2.channel_id AS product_channel_id,
				MIN(entry_date) AS first_purchase, 
				MAX(entry_date) AS last_purchase
				FROM ".$this->dbprefix."cartthrob_order_items ecoi, ".$this->dbprefix."channel_titles ect, exp_channel_data ecd2
				WHERE ect.entry_id = ecoi.order_id
				AND ecd2.entry_id = ecoi.entry_id
				$where
				GROUP BY ecoi.entry_id, price
		";

		return $this->EE->db->query($sql)->result_array();
	}
	
	/**
	 * Returns the total amount of money a store has earned based on $where
	 * @param string $where
	 * @return string
	 */
	public function get_totals($where = FALSE)
	{		
		$total_field = 'field_id_'.$this->EE->cartthrob->store->config('orders_total_field');	
		$order_channel = $this->EE->cartthrob->store->config('orders_channel');
		$sql = "SELECT SUM($total_field) AS total 
				FROM ".$this->dbprefix."channel_titles ect, exp_channel_data ecd 
				WHERE ecd.entry_id = ect.entry_id AND ect.channel_id = '$order_channel' $where
				GROUP BY ect.channel_id
		";
		
		$data = $this->EE->db->query($sql)->row();
		if(isset($data->total))
		{
			return $data->total;
		}
		else
		{
			return '0';
		}
	}

	/**
	 * Returns an array of countries with how many orders have occured there
	 * @param string $where
	 * @return void|multitype:unknown
	 */
	public function get_order_countries($where = FALSE)
	{
		$col = $this->EE->cartthrob->store->config('orders_billing_country');
		if($col == '')
		{
			return;
		}		
		$country_field = 'field_id_'.$col;
		$sql = "SELECT COUNT( $country_field ) AS total_orders, $country_field AS country
		FROM ".$this->dbprefix."channel_data ecd, ".$this->dbprefix."channel_titles ect
		WHERE ect.entry_id = ecd.entry_id
		AND $country_field != ''
		$where
		GROUP BY $country_field
		";

		$data = $this->EE->db->query($sql)->result_array();	
		$return = array();
		if(count($data) >= '1')	
		{
			foreach($data AS $item)
			{
				$return[$item['country']] = $item['total_orders'];
			}
		}
		
		return $return;
	}
	
	public function get_all_totals($where = FALSE)
	{
		$total_sum = $tax_sum = $shipping_sum = $discount_sum = FALSE;
		if($this->EE->cartthrob->store->config('orders_total_field') != '')
		{
			$total_field = 'field_id_'.$this->EE->cartthrob->store->config('orders_total_field');	
			$total_sum = ", SUM($total_field) AS total";
			$total_sum .= ", COUNT($total_field) AS total_orders";
		}
		
		if($this->EE->cartthrob->store->config('orders_tax_field') != '')
		{
			$tax_field = 'field_id_'.$this->EE->cartthrob->store->config('orders_tax_field');
			$tax_sum = ", SUM($tax_field) AS tax";
		}
		
		if($this->EE->cartthrob->store->config('orders_shipping_field') != '')
		{
			$shipping_field = 'field_id_'.$this->EE->cartthrob->store->config('orders_shipping_field');
			$shipping_sum = ", SUM($shipping_field) AS shipping";
		}
		
		if($this->EE->cartthrob->store->config('orders_discount_field') != '')
		{
			$discount_field = 'field_id_'.$this->EE->cartthrob->store->config('orders_discount_field');
			$discount_sum = ", SUM($discount_field) AS discount";
		}
		
		$order_channel = $this->EE->cartthrob->store->config('orders_channel');	;
		$sql = "SELECT DATE_FORMAT(FROM_UNIXTIME(entry_date), \"%M %Y\") AS order_range $total_sum $tax_sum $shipping_sum $discount_sum, entry_date
				FROM ".$this->dbprefix."channel_titles ect, exp_channel_data ecd 
				WHERE ecd.entry_id = ect.entry_id AND ect.channel_id = '$order_channel' $where
				GROUP BY order_range ORDER BY entry_date DESC
		";
		
		$data = $this->EE->db->query($sql)->result_array();
		foreach($data AS $key => $value)
		{
			$parts = explode(" ", $value['order_range']);
			$value['order_range'] = lang($parts['0']).( ! empty($parts['1']) ? ' '.$parts['1'] : '');
			$data[$key]['name'] = $value['order_range'];
			if(!isset($data[$key]['discount']))
			{
				$data[$key]['discount'] = '0';
			}
			
			if(!isset($data[$key]['shipping']))
			{
				$data[$key]['shipping'] = '0';
			}

			if(!isset($data[$key]['tax']))
			{
				$data[$key]['tax'] = '0';
			}

			if(!isset($data[$key]['total']))
			{
				$data[$key]['total'] = '0';
			}			
		}

		return $data;
	}
	
	public function clean_month_chart_data($all_totals, $start, $stop)
	{
		$interval = m62_date_interval($start, $stop, "%M");
		$arr = array();
		for($i=0; $i<=$interval;$i++)
		{
			$math = $interval-$i;
			$date = date('F Y', mktime(0, 0, 0, date("m")-$math, 1, date("Y")));
			$found = false;
			foreach($all_totals AS $month)
			{
				if($date == $month['order_range'])
				{
					$found = true;
					$arr[$i] = $month;
					break;
				}
			}
				
			if(!$found)
			{
				$arr[$i] = array(
					'order_range' => $date, 
					'total' => '0', 
					'total_orders' => '0',
					'tax' => '0',
					'shipping' => '0',
					'discount' => '0',
					'name' => $date,
					'entry_date' => strtotime($date),
				);
			}			
		}
		
		$arr = array_reverse($arr);
			
		return $arr;
	}
	
	public function get_chart_order_history($range = 14, $where = FALSE)
	{
		$start_date = date('Y-m-d', mktime(0, 0, 0, date("m")  , (date("d")-$range), date("Y")));
		$orders_total_key = $this->config['orders_total_field'];
		$orders_sub_total_key = $this->EE->cartthrob->store->config('orders_subtotal_field');
		if($orders_total_key == '' || $orders_sub_total_key == '')
		{
			return FALSE;
		}
		
		$sql = "SELECT entry_date, SUM(field_id_$orders_total_key) AS total, SUM(field_id_$orders_sub_total_key) AS subtotal,
				CONCAT_WS('-', year, month, day) AS order_date
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				WHERE ect.entry_id = ecd.entry_id 
				AND entry_date >= '".strtotime($start_date)."'
				AND entry_date <= '".time()."'
				AND ecd.channel_id = '".$this->channel_id."'
				$where
				GROUP BY order_date
		";
				
		$data = $this->EE->db->query($sql)->result_array();
		
		//wrap up the data so it's ready for the chart
		$total_times = 0;
		$arr = array();
		for($i=0; $i<=$range;$i++)
		{
			$math = $range-$i;
			$date = date('Y-m-d', mktime(0, 0, 0, date("m")  , date("d")-$math, date("Y"))); 
			$found = false;
			foreach($data AS $day)
			{
				if($date == $day['order_date'])
				{
					$found = true;
					$arr[$day['order_date']] = $day;
					break;
				}
			}
			
			if(!$found)
			{
				$arr[$date] = array('order_date' => $date, 'total' => '0', 'subtotal' => '0');
			}
		}

		return $arr;
	}
	
	public function get_chart_order_date($range = 14, $where = FALSE, $month = FALSE, $year = FALSE, $day = FALSE)
	{
		if(!$month)
		{
			$month = date("n");
		}
		
		if(!$year)
		{
			$year = date('Y');
		}
		
		if(!$day)
		{
			$day = date('j');
		}		
		
		$orders_total_key = $this->config['orders_total_field'];
		$orders_sub_total_key = $this->EE->cartthrob->store->config('orders_subtotal_field');
		if($orders_total_key == '' || $orders_sub_total_key == '')
		{
			return FALSE;
		}
		
		$sql = "SELECT entry_date, SUM(field_id_$orders_total_key) AS total, SUM(field_id_$orders_sub_total_key) AS subtotal,
				CONCAT_WS('-', year, month, day) AS order_date
				FROM ".$this->dbprefix."channel_titles ect, ".$this->dbprefix."channel_data ecd
				WHERE ect.entry_id = ecd.entry_id 
				AND ecd.channel_id = '".$this->channel_id."'
				$where
				GROUP BY order_date
				ORDER BY order_date DESC
		";

		$data = $this->EE->db->query($sql)->result_array();
		
		
		//wrap up the data so it's ready for the chart
		$total_times = 0;
		$arr = array();
		for($i=0; $i<$range;$i++)
		{
			$month = (int)$month;
			$day = (int)$day;
			$new_day = ($day+$i);
			$date = date('Y-m-d', mktime(0, 0, 0, $month, $new_day, $year)); 
			$found = false;
			foreach($data AS $day)
			{
				if($date == $day['order_date'])
				{
					$found = true;
					$arr[$day['order_date']] = $day;
					break;
				}
			}
			
			if(!$found)
			{
				$arr[$date] = array('order_date' => $date, 'total' => '0', 'subtotal' => '0');
			}
		}
		
		return $arr;
	}	

	public function low_stock_report()
	{
		$fields = $this->EE->cartthrob->store->config('product_channel_fields');
		$key = FALSE;
		if(is_array($fields) && count($fields) >= '1')
		{
			$key = FALSE;
			foreach($fields AS $field)
			{
				if(isset($field['inventory']) && is_numeric($field['inventory']))
				{
					$key = $field['inventory'];
					break;
				}
			}
			
			if($key)
			{
				$product_channels = $this->EE->cartthrob->store->config('product_channels');
				if(isset($product_channels) && is_array($product_channels))
				{
					$return = array();
					$product_channels = array_unique($product_channels);
					foreach($product_channels AS $channel)
					{
						$sql = "SELECT title, field_id_$key AS inventory, ect.entry_id FROM `".$this->dbprefix."channel_titles` ect, ".$this->dbprefix."channel_data ecd 
								WHERE 
								ect.channel_id = '$channel' 
								AND ect.entry_id = ecd.entry_id
								AND field_id_$key != ''
						";
						
						$query = $this->EE->db->query($sql);
						if($query->num_rows >= '1')
						{
							$return = array_merge($query->result_array(), $return); 
						}
					}
					
					$low_stock_report = array();
					foreach($return AS $report)
					{
						//first check if we're dealing with a custom field.
						if(is_numeric($report['inventory']))
						{
							$low_stock_report[] = array(
								'title' => $report['title'],
								'inventory' => $report['inventory'],
								'entry_id' => $report['entry_id'],
								'option_value' => ''
							);
						}
						else
						{
							$options = unserialize(base64_decode($report['inventory']));
							foreach($options AS $option)
							{
								$low_stock_report[] = array(
									'title' => $report['title'].' ('.$option['option_name'].')',
									'inventory' => $option['inventory'],
									'entry_id' => $report['entry_id'],
									'option_value' => $option['option_value']
								);								
							}
						}
					}					
					
					return $low_stock_report;
				}
			}
		}		
	}
	
	public function get_ip_to_country()
	{
		$data = $this->EE->ct_admin_cache->read_cache(md5(__METHOD__));
		if (!$data)
		{
			$col = $this->EE->cartthrob->store->config('orders_customer_ip_address');
			if($col == '')
			{
				return;
			}
			$ip_col = 'field_id_'.$col.' AS ip_addy';
			$this->EE->db->select($ip_col);
			$this->EE->db->where('channel_id', $this->channel_id);
			$query = $this->EE->db->get('channel_data')->result_array();
			$countries = array();
			foreach($query AS $ip)
			{
				$query = $this->EE->db->query("SELECT country FROM exp_ip2nation WHERE ip < INET_ATON('".$this->EE->db->escape_str($ip['ip_addy'])."') ORDER BY ip DESC LIMIT 0,1");
				if($query->num_rows == '1')
				{
					$row = $query->row();
					if(!isset($countries[$row->country]))
					{
						$countries[$row->country] = 1;
					}
					else
					{
						$countries[$row->country]++;
					}
				}
			}

			$data = serialize($countries);
			$this->EE->ct_admin_cache->create_cache_file($data, md5(__METHOD__));
		}
		
		return unserialize($data);
	}
	
	public function hook_reports(&$hook_reports)
	{
		if ($this->EE->extensions->active_hook('ct_admin_custom_reports') === TRUE)
		{
			$hook_reports=$this->EE->extensions->call('ct_admin_custom_reports', $hook_reports);
			if ($this->EE->extensions->end_script === TRUE) return $hook_reports;
		}

		return $hook_reports;
	}
	
	/**
	 * Wrapper to allow for the primary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_history_report_hook_view()
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_history_report_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_history_report_view');
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}
	
	/**
	 * Wrapper to allow for the secondary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_history_report_hook_secondary_view()
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_history_report_secondary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_history_report_secondary_view');
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}
	
	/**
	 * Wrapper to allow for the tertiary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_history_report_hook_tertiary_view()
	{
		$return = '';
		if ($this->EE->extensions->active_hook('ct_admin_history_report_tertiary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('ct_admin_history_report_tertiary_view');
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}

	/**
	 * Wrapper to allow for the primary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_dashboard_hook_view()
	{
		$return = '';
		if ($this->EE->extensions->active_hook('setup_dashboard_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('setup_dashboard_view');
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}
	
	/**
	 * Wrapper to allow for the secondary view hook
	 * @param array $order_details
	 * @return string
	 */
	public function setup_dashboard_hook_secondary_view()
	{
		$return = '';
		if ($this->EE->extensions->active_hook('setup_dashboard_secondary_view') === TRUE)
		{
			$return .= $this->EE->extensions->call('setup_dashboard_secondary_view');
			if ($this->EE->extensions->end_script === TRUE) return $return;
		}
	
		return $return;
	}	
}