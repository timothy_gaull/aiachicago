<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */

$help_menu = array( 
			'index' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/dashboard/', 
			'orders' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/orders/', 
			'customers' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/customers/', 
			'order_view' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/order-view/',
			'customer_view' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/customer-view/',
			'products' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/products/',
			'product_view' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/product-view/',
			'reports' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/reports/',
			'history_report' => 'http://www.mithra62.com/docs/detail/ct-admin-instructions/monthly-history/',
			'settings' => 'http://www.mithra62.com/docs/detail/ct-admin-configuration/'
); 
/* End of file help_menu.php */ 
 