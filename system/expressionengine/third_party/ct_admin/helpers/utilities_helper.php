<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Helper Functions
 *
 * Helper Functions
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/helpers/utilities_helper.php
 */

if ( ! function_exists('m62_format_number'))
{
	/**
	 * Format Number
	 * Formats a number according to how CartThrob would do things
	 * @param string $number
	 * @param bool $float
	 */
	function m62_format_number($number, $float = TRUE)
	{
		if(strpos($number, ',') === FALSE)
		{
			$EE =& get_instance();
			$float = ($float ? $EE->cartthrob->store->config('number_format_defaults_decimals') : FALSE);
			$number = number_format(
				$number, 
				$float,
				$EE->cartthrob->store->config('number_format_defaults_dec_point'),
				$EE->cartthrob->store->config('number_format_defaults_thousands_sep')
			);
		}
		return $number;
	}
	
	/**
	 * Format Money
	 * Formats money strings according to how CartThrob settings
	 * @param string $number
	 */
	function m62_format_money($number)
	{
		if(is_numeric($number))
		{
			$EE =& get_instance();
			$number = $EE->cartthrob->store->config('number_format_defaults_prefix').m62_format_number($number, '2');
		}
		else 
		{
			return lang('na');
		}
		
		return $number;
	}
	
	function m62_format_date($date, $format = FALSE, $html = FALSE)
	{
		$EE =& get_instance();
		$settings = $EE->ct_admin_lib->get_settings();
		if($settings['relative_time'] == '1')
		{
			if($html)
			{
				$date = '<span class="ct_admin_timeago" title="'.m62_convert_timestamp($date).'">'.m62_relative_datetime($date).'</span>';
			}
			else 
			{
				$date = m62_relative_datetime($date);
			}
		}
		else
		{
			$date = m62_convert_timestamp($date, $format);
		}
		
		return $date;
	}

	/**
	 * Timestamp Format
	 * Wrapper that takes a string and converts it according to CT Admin settings
	 * @param string $date
	 * @param string $format
	 */
	function m62_convert_timestamp($date, $format = FALSE)
	{
		if(!is_numeric($date))
		{
			$date = strtotime($date);
		}
		
		$EE =& get_instance();
		$EE->load->helper('date');
		if(!$format)
		{
			$format = $EE->ct_admin_lib->settings['ct_date_format'];
		}
		
		return mdate($format, $date);		
	}
	
	/**
	 * Returns the status color based on $status
	 * @param string $status
	 * @param array $statuses
	 * @return boolean|array
	 */
	function m62_status_color($status, array $statuses = array())
	{
		if(!is_array($statuses))
		{
			return FALSE;
		}

		foreach($statuses AS $color)
		{
			if($status == $color['status'])
				return $color['highlight'];
		}
	}
	
	/**
	 * Translates a 2 digit country code to the name
	 * @param string $code
	 * @return string
	 */
	function m62_country_code($code)
	{
		include APPPATH .'config/countries.php';
		if(isset($countries[$code]))
		{
			return $countries[$code];
		}
		else
		{
			return $code;
		}
	}
	
	/**
	 * Wrapper to creates an invoice URL (or packingslip) in a view
	 * @param int $entry_id
	 * @param string $packingslip
	 */
	function m62_get_invoice_url($entry_id, $packingslip = FALSE)
	{
		$EE =& get_instance();
		return $EE->ct_admin_lib->get_invoice_url($entry_id, $packingslip);
	}
	
	/**
	 * Creates a date in human readable format (1 hour, 7 years, etc...)
	 * @param string $timestamp
	 * @return string
	 */
	function m62_relative_datetime($timestamp)
	{
		if(!$timestamp)
		{
			return 'N/A';
		}

		if(!is_numeric($timestamp))
		{
			$timestamp = (int)strtotime($timestamp);
		}
		
		if($timestamp == '0')
		{
			return 'N/A';
		}
	
		$difference = time() - $timestamp;
		$periods = array("sec", "min", "hour", "day", "week","month", "year", "decade");
		$lengths = array("60","60","24","7","4.35","12","10");
		$total_lengths = count($lengths);
	
		if ($difference > 0)
		{
			// this was in the past
			$ending = "ago";
		}
		else
		{
			// this was in the future
			$difference = -$difference;
			$ending = " from now";
		}
	
		for($j = 0; $difference > $lengths[$j] && $total_lengths > $j; $j++)
		{
			$difference /= $lengths[$j];
		}
	
		$difference = round($difference);
		if($difference != 1)
		{
			$periods[$j].= "s";
		}
	
		$text = "$difference $periods[$j] $ending";
		return $text;
	}	
	
	/**
	 * Abstraction of the PHP 5.3 DateTime::interval() method
	 * @param obj $_date1
	 * @param obj $_date2
	 * @param string $format
	 * @return mixed
	 */
	function m62_date_interval($_date1, $_date2, $format)
	{
		if( $_date1 instanceof DateTime === FALSE)
		{
			$_date1 = new DateTime(m62_convert_timestamp($_date1, '%Y-%M-%d'));
		}
		
		if( $_date2 instanceof DateTime === FALSE)
		{
			$_date2 = new DateTime(m62_convert_timestamp($_date2, '%Y-%M-%d'));
		}
		
	    //Make sure $date1 is ealier
	    $date1 = ($_date1 <= $_date2 ? $_date1 : $_date2);
	    $date2 = ($_date1 <= $_date2 ? $_date2 : $_date1);
	
	    //Calculate R values
	    $R = ($_date1 <= $_date2 ? '+' : '-');
	    $r = ($_date1 <= $_date2 ? '' : '-');
	
		//Calculate total days
		$total_days = round(abs($date1->format('U') - $date2->format('U'))/86400);
	
		//A leap year work around - consistent with DateInterval
		$leap_year = ( $date1->format('m-d') == '02-29' ? true : false);
		if( $leap_year ){
			$date1->modify('-1 day');
		}
	
		$periods = array( 'years'=>-1,'months'=>-1,'days'=>-1,'hours'=>-1);
	
		foreach ($periods as $period => &$i ){
	
			if($period == 'days' && $leap_year )
				$date1->modify('+1 day');
	
			while( $date1 <= $date2 ){
				$date1->modify('+1 '.$period);
				$i++;
			}
	
			//Reset date and record increments
			$date1->modify('-1 '.$period);
		}
		
		if( ! empty($periods['years']) && $periods['years'] >= 1 )
		{
			$periods['months'] = $periods['months']+($periods['years']*12); //hack to ensure month math works proper
		}

		extract($periods);
	
		//Minutes, seconds
		$seconds = round(abs($date1->format('U') - $date2->format('U')));
		$minutes = floor($seconds/60);
		$seconds = $seconds - $minutes*60;
	
		$replace = array(
			'/%y/' => $years,
			'/%Y/' => $years,
			'/%m/' => $months,
			'/%M/' => $months,
			'/%d/' => $days,
			'/%D/' => $days,
			'/%a/' => $total_days,
			'/%h/' => $hours,
			'/%H/' => $hours,
			'/%i/' => $minutes,
			'/%I/' => $minutes,
			'/%s/' => $seconds,
			'/%S/' => $seconds,
			'/%r/' => $r,
			'/%R/' => $R
		);
	
		return preg_replace(array_keys($replace), array_values($replace), $format);
	}	
}