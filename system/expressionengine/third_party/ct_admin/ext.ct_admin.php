<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2014, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @since		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - Ext Class
 *
 * Extension class
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/ext.ct_admin.php
 */
class Ct_admin_ext 
{	
	/**
	 * The EE Extension array (CT Admin doesn't use it though)
	 * @var array
	 */
	public $settings = array();
	
	/**
	 * The Extension description
	 * @var string
	 */
	public $description	= 'Extension for modifying how CartThrob works';
	
	/**
	 * Do we use settings (we do, but not EE settings...)
	 * @var string
	 */
	public $settings_exist	= 'y';
	
	/**
	 * Where are the docs located?
	 * @var string
	 */
	public $docs_url = 'http://mithra62.com/docs/view/ct-admin-installation'; 
	
	/**
	 * Weird thing EE requires so the extension is installed automatically
	 * @var array
	 */
	public $required_by = array('module');	
	
	/**
	 * Set it up!
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		
		include PATH_THIRD.'ct_admin/config'.EXT;
		
		$this->version = $config['version'];
		$this->name = $config['name'];		
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/'); 
		$this->EE->load->library('cartthrob_loader');
		$this->EE->cartthrob_loader->setup($this);
				
		$this->EE->load->library('email');	
		$this->EE->lang->loadfile('ct_admin');	
		
		if ($this->EE->input->post('clear_cart'))
		{
			$this->delete_saved_cart();
		}
		//$this->EE->load->remove_package_path(PATH_THIRD.'cartthrob/'); 
	}
	
	/**
	 * The Extension settings form.
	 * Simple hack to redirect the user to the module settings
	 */
	public function settings_form()
	{
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=ct_admin'.AMP.'method=settings');
		exit;
	}
	
	/**
	 * Handles the routing and logic for the Dashboard routing
	 * @param array $menu
	 * @return array
	 */
	public function dashboard_route($menu)
	{
		if ($this->EE->extensions->last_call !== FALSE)
		{
			$menu = $this->EE->extensions->last_call;
		}

		if( ! $this->EE->input->get_post('C') || $this->EE->input->get_post('C') == 'homepage' )
		{
			$this->EE->load->library('ct_admin_lib');
			$settings = $this->EE->ct_admin_lib->get_settings();
			if($settings['override_cp_dashboard'] == '1')
			{
				$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=ct_admin'.AMP.'method=index');
				exit;
			}
		}
		
		return $menu;
	}
	
	/**
	 * Handles the logic for updating a cart
	 */
	public function update_saved_cart()
	{
		$this->EE->load->model('ct_admin_carts_model', 'ct_admin_carts', TRUE);
		$check = $this->EE->ct_admin_carts->get_items(array('cart_id' => $this->EE->ct_admin_carts->cart_id));
		$removed = array();
		if(isset($_POST['delete']) && is_array($_POST['delete']))
		{
			foreach($_POST['delete'] AS $key => $value)
			{
				$removed[] = $key;
				$this->EE->ct_admin_carts->delete_items(array('cart_id' => $this->EE->ct_admin_carts->cart_id, 'row_id' => $key));
			}
		}
		
		
		foreach ($this->cart->items() as $row_id => $item)
		{	
			if(in_array($row_id, $removed))	
			{
				continue;
			}
			
			$data = array();
			foreach ($_POST as $key => $value)
			{
				if (!isset($value[$row_id]))
				{
					continue;
				}
				
				if(in_array($key, $item->default_keys()))
				{
					$data[$key] = $this->EE->security->xss_clean($value[$row_id]);
					if(is_array($data[$key]))
					{
						$data[$key] = base64_encode(serialize($data[$key]));
					}
				}
			}
			
			$where = array('cart_id' => $this->EE->ct_admin_carts->cart_id, 'row_id' => $row_id);
			$this->EE->ct_admin_carts->update_item($data, $where, FALSE);
		}
	}
	
	/**
	 * Handles the add to cart logic
	 * @param Cartthrob_item_product $item
	 * @return Cartthrob_item_product
	 */
	public function save_add_to_cart($item)
	{
		//fix for updating individual items issue within CT
		if( ! $item || $this->EE->session->userdata['member_id'] == '0') //only member's carts should be saved
		{
			return $item;
		}
		
		$this->EE->load->library('Ct_admin_carts');
		$this->EE->ct_admin_carts->save_add_to_cart($item);
		
		return $item;
	}

	/**
	 * Handles removing a cart and all its items
	 * @param string $thing
	 * @return string
	 */
	public function delete_saved_cart($thing = FALSE)
	{
		$this->EE->load->model('ct_admin_carts_model', 'ct_admin_carts', TRUE);
		
		//we have to get rid of any references to each product from the user from archived carts for logged in customers
		if($this->EE->session->userdata['member_id'] >= 1)
		{
			foreach ($this->cart->items() as $row_id => $item)
			{
				$product_id = $item->product_id();
				if($product_id)
				{
					$where = array(
						'product_id' => $product_id,
						'member_id' => $this->EE->session->userdata['member_id']
					);
					$this->EE->ct_admin_carts->delete_items($where);
				}
			}
		}
		
		$this->EE->ct_admin_carts->delete_items(array('cart_id' => $this->EE->ct_admin_carts->cart_id));
		return $thing;
	}
	
	/**
	 * Adds a new parameter to add_to_cart tags to limit the number of products a cart can have on an item
	 */
	public function cart_max_quantity()
	{
		if(!isset($this->EE->TMPL))
		{
			return;
		}
		
		$return = $this->EE->TMPL->fetch_param('return');
		$max_quantity = $this->EE->TMPL->fetch_param('max_quantity');
		$entry_id = $this->EE->TMPL->fetch_param('entry_id');
		if($max_quantity >= '1' && $entry_id >= '1')
		{
			$cart = $this->EE->cartthrob->cart->filter_items(array('entry_id' => $entry_id));
			if(isset($cart['0']))
			{
				$in_cart = $cart['0']->quantity();	
				if($max_quantity <= $in_cart )
				{
					$this->EE->functions->redirect($this->EE->functions->create_url($return));
					exit;
				}
			}
		}
	}
	
	/**
	 * Ensures a coupon is valid in order for the store to accept it upon checkout
	 * Essentially, it just makes the Coupon code field required 
	 */
	public function require_valid_coupon()
	{			
		$this->EE->load->library('ct_admin_coupons');
		$this->EE->load->library('ct_admin_lib');
		$settings = $this->EE->ct_admin_lib->get_settings();

		if($settings['require_valid_coupon'] != '1')
		{
			return;
		}
		
		$coupon_code = $this->EE->input->post('coupon_code', TRUE);
		if(!$this->EE->ct_admin_coupons->is_valid_code($coupon_code))
		{
			show_error(lang('please_enter_valid_coupon'));
		}
	}
	
	/**
	 * Handles the redirection when an entry in the Order channel occurs
	 * @param int $entry_id
	 * @param array $meta
	 * @param array $data
	 */	
	public function redirect_cp_order_edit($entry_id, $meta, $data)
	{	
		//only route on editing an entry NEVER ON CREATION :|
		if(!isset($data['entry_id']) || $data['entry_id'] == '0')
		{
			return;
		}
				
		if(isset($meta['channel_id']) && $meta['channel_id'] == $this->EE->cartthrob->store->config('orders_channel'))
		{
			$this->EE->load->library('ct_admin_lib');
			$settings = $this->EE->ct_admin_lib->get_settings();
			if($settings['override_cp_order_edit_routing'] == '1')
			{
				$this->query_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=ct_admin'.AMP.'method=';
				$this->url_base = BASE.AMP.$this->query_base;				
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('order_updated'));
				$this->EE->functions->redirect($this->url_base.'order_view&id='.$entry_id);
				exit;
			}			
		}	
	}
	
	/**
	 * Handles the redirection when an entry in the Product channel occurs
	 * @param int $entry_id
	 * @param array $meta
	 * @param array $data
	 */
	public function redirect_cp_product_edit($entry_id, $meta, $data)
	{
		//only route on editing an entry NEVER ON CREATION :|
		if(!isset($data['entry_id']) || $data['entry_id'] == '0')
		{
			return;
		}
				
		if(isset($meta['channel_id']) && in_array($meta['channel_id'],$this->EE->cartthrob->store->config('product_channels')))
		{
			$this->EE->load->library('ct_admin_lib');
			$settings = $this->EE->ct_admin_lib->get_settings();
			if($settings['override_cp_product_routing'] == '1')
			{
				//we only want to redirect if the product has been sold otherwise default to normal
				$this->EE->load->library('ct_admin_products');
				$product_info = $this->EE->ct_admin_products->get_product($entry_id);
				if(count($product_info) >= 1)
				{
					$this->query_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=ct_admin'.AMP.'method=';
					$this->url_base = BASE.AMP.$this->query_base;				
					$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('product_updated'));
					$this->EE->functions->redirect($this->url_base.'product_view&id='.$entry_id);
					exit;
				}
			}			
		}	
	}
	
	/**
	 * Adds CT Admin menu items to the CartThrob menu if it's enabled
	 * @param array $menu
	 * @return multitype:string
	 */
	public function cp_menu_array($menu)
	{
		if ($this->EE->extensions->last_call !== FALSE)
		{
			$menu = $this->EE->extensions->last_call;
		}
				
		if(isset($menu['cartthrob']))
		{
			$this->query_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=ct_admin'.AMP.'method=';
			$this->url_base = BASE.AMP.$this->query_base;
						
			$menu['cartthrob']['ct_admin_nav'] = array(
				'dashboard' => $this->url_base.'index', 
				'orders' => $this->url_base.'orders', 
				'customers' => $this->url_base.'customers', 
				'products' => $this->url_base.'products',
				'reports' => $this->url_base.'reports',
				'0' => '----',
				'settings' => $this->url_base.'settings',										
			);
		}
		
		return $menu;
	}
	
	public function activate_extension() 
	{
		return TRUE;
	}
	
	public function update_extension($current = '')
	{
		return TRUE;
	}

	public function disable_extension()
	{
		return TRUE;

	}
}