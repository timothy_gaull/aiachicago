<?php 
$this->table->set_heading(
lang('product'),
lang('quantity'),
lang('unit_price'),
lang('total')
);
foreach($order_items AS $item)
{
	$options = array();
	if($item['entry_id'] >= '1')
	{
		$cell = '<a href="'.$url_base.'product_view'.AMP.'id='.$item['entry_id'].'">'.$item['title'].'</a>';
	}
	else
	{
		$cell = $item['title'];
	}
	
	$package = FALSE;
	if(isset($item['extra']) && $item['extra'] != '')
	{
		if((is_array($item['extra']) && count($item['extra']) >= '1') && !isset($item['extra']['row_id']))
		{
			//print_r($options);
			$cell .= '<div class="subtext">(';
			if(isset($item['extra']['sub_items']) && is_array($item['extra']['sub_items']) && count($item['extra']['sub_items']) >= '1')
			{
				$package = TRUE;
				$cell .= count($item['extra']['sub_items']).' '.lang('product_package');
			}
			else
			{
				$options = $item['extra'];
				unset($options['_option_keys']);
				foreach($options AS $key => $value)
				{
					if($value == '')
					{
						unset($options[$key]);
					}
				}				
				$cell .= implode(' / ', $options);
			}
				
			$cell .= ')</div>';
		}
	}
	$data = array(
			$cell,
			$item['quantity'],
			m62_format_money($item['price']),
			m62_format_money($item['quantity']*$item['price'])
	);
	$this->table->add_row($data);

	if($package)
	{
		//we have a package style product so we have to spit those out too
		foreach($item['extra']['sub_items'] AS $sub_item)
		{
			$extra = '';
			$product_options = array();
			if( !empty($item['extra']['_option_keys']) && is_array($item['extra']['_option_keys']) )
			{
				foreach($item['extra']['_option_keys'] AS $option_key => $option_value)
				{
					if( !empty($sub_item[$option_key]) )
					{
						if( $sub_item[$option_key] != '')
						{
							$product_options[] = $sub_item[$option_key];
						}
					}
				}
				
				if( count($product_options) >= '1')
					$extra = '<div class="subtext">('.implode(' / ', $product_options).')</div>';
			}
				
			$cell = '<a href="'.$url_base.'product_view'.AMP.'id='.$sub_item['entry_id'].'">'.$sub_item['title'].'</a>'.$extra;
			$data = array(
					'<div class="ct_admin_package_sub_item">'.$cell.'</div>',
					'-',
					'-',
					'-'
			);
			$this->table->add_row($data);
		}
	}
}

echo $this->table->generate();
// Clear out of the next one
$this->table->clear();
?>