<?php $this->load->view('errors'); ?>

<div class="ct_top_nav">
	<div class="ct_nav">
	
		<?php 
		foreach($menu_data AS $key => $value): ?>
		<span class="button"> 
			<a class="nav_button <?php echo ($type == $value['url'] ? 'current' : ''); ?>" href="<?php echo $url_base.'settings&section='.$value['url']; ?>"><?php echo lang($key.'_settings_menu')?></a>
		</span>
		<?php endforeach; ?>	
			
	</div>
</div>

<?php 

$tmpl = array (
	'table_open'          => '<table class="mainTable" border="0" cellspacing="0" cellpadding="0">',

	'row_start'           => '<tr class="even">',
	'row_end'             => '</tr>',
	'cell_start'          => '<td style="width:50%;">',
	'cell_end'            => '</td>',

	'row_alt_start'       => '<tr class="odd">',
	'row_alt_end'         => '</tr>',
	'cell_alt_start'      => '<td>',
	'cell_alt_end'        => '</td>',

	'table_close'         => '</table>'
);

$this->table->set_template($tmpl); 
$this->table->set_empty("&nbsp;");
?>
<div class="clear_left shun"></div>

<?php echo form_open($query_base.'settings', array('id'=>'my_accordion'))?>
<input type="hidden" value="yes" name="go_settings" />
<input type="hidden" value="<?php echo $type; ?>" name="section" />

<?php 

switch($type)
{
	case 'limits':
	case 'date_formats':
	case 'reporting':
	case 'general':
	case 'cp':
		$this->load->view('settings/'.$type);
	break;
	
	default:
		$this->load->view('settings/general');
	break;
}

?>
<div class="tableFooter">
	<div class="tableSubmit">
		<?php echo form_submit(array('name' => 'submit', 'value' => lang('submit'), 'class' => 'submit'));?>
	</div>
</div>	
<?php echo form_close()?>