<?php 
$this->load->view('overlay/header'); 
?>

<h1><?=$page_title; ?></h1>
<?=form_open($query_base.'abandoned_cart', array('id'=>'my_accordion'))?>
<?php
$this->table->set_template($cp_table_template);
$this->table->set_empty("&nbsp;");
?>	

<div>


<br clear="all" />		
<?php 
$this->load->view('errors', array('lightbox' => 'yes')); 
?>
<br clear="all" />
<?php 
$this->table->set_heading(lang('name'), lang('email'), lang('potential_revenue'), lang('total_in_cart'),lang('first_added'),lang('last_added'));
$data = array(
		'<a href="'.$url_base.'customer_view'.AMP.'email='.$abandoned_cart_meta['email'].'" target="_parent">'.$abandoned_cart_meta['customer_data']['cust_first_name'].' '.$abandoned_cart_meta['customer_data']['cust_last_name'].'</a>', 
		'<a href="mailto:'.$abandoned_cart_meta['email'].'">'.$abandoned_cart_meta['email'].'</a>',
		m62_format_money( $abandoned_cart_meta['total_in_cart']*$product_data['price'] ),
		$abandoned_cart_meta['total_in_cart'].' ('.count($abandoned_carts).')',
		m62_format_date($abandoned_cart_meta['first_added'], FALSE, TRUE),
		m62_format_date($abandoned_cart_meta['last_added'], FALSE, TRUE)
);
$this->table->add_row($data);
echo $this->table->generate();
$this->table->clear();
?>
</div>

<?php foreach($abandoned_carts AS $cart_id => $cart_items): ?>
<h3 class="accordion "><?php echo lang('cart').' "'. $cart_id.'" ('.count($cart_items).') ';?>
</h3>
<div id="cart_products_<?php echo $cart_id; ?>"><?php 
$this->table->set_heading(
lang('quantity'),
lang('unit_price'),
lang('total'),
lang('added'),
lang('last_modified')
);
foreach($cart_items AS $item)
{
	$data = array(
		$item['quantity'],
		m62_format_money($product_data['price']),
		m62_format_money($item['quantity']*$product_data['price']),
		m62_format_date($item['created_date'], FALSE, TRUE),
		m62_format_date($item['last_modified'], FALSE, TRUE)
	);
	$this->table->add_row($data);
}

echo $this->table->generate();
// Clear out of the next one
$this->table->clear();
?></div>

<?php endforeach; ?>

<?php //echo $hook_secondary_view; ?>
<?=form_close()?>
<?php $this->load->view('overlay/footer'); ?>