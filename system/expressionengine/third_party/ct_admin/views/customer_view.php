<?php 
$this->load->view('errors'); 
?>
<?=form_open($query_base.'update_customer', array('id'=>'my_accordion'))?>
<?php
$this->table->set_template($cp_table_template);
$this->table->set_empty("&nbsp;");

?>	
<div>
<?php $this->load->view('includes/sub_menu'); ?>
<br clear="all" />
<div>
<?php 
$this->table->set_heading(lang('name'), lang('email'), lang('first_order'),lang('last_order'),lang('total_amount_spent'));
$customer_email = (isset($customer_data['cust_email_address']) && $customer_data['cust_email_address'] != '' ? $customer_data['cust_email_address'] : $customer_email);
$data = array(
		$customer_data['cust_first_name'].' '.$customer_data['cust_last_name'], 
		'<a href="mailto:'.$customer_email.'">'.$customer_email.'</a>', 
		m62_format_date($first_order, FALSE, TRUE),
		m62_format_date($last_order, FALSE, TRUE),
		m62_format_money($total_amount_spent, FALSE, TRUE)
);
$this->table->add_row($data);
echo $this->table->generate();
$this->table->clear();
?>
</div>

<?php echo $hook_view; ?>

<h3 class="accordion"><?=lang('customers_orders').' ('.$total_orders.') ';?></h3>
<div id="customer_orders"><?php 
// Add Markup into the table
$this->table->set_heading(
	lang('orders'),
	lang('status'),
	lang('order_total'),
	lang('date'),
	''
);
foreach($order_data AS $order)
{
	$packingslip_url = m62_get_invoice_url($order['entry_id'], TRUE);
	$invoice_url = m62_get_invoice_url($order['entry_id']);
	
	$data = array(
		'<a href="'.$url_base.'order_view'.AMP.'id='.$order['entry_id'].AMP.'lightbox=yes" class="fancybox_overlay"><img style="padding-right:5px; cursor:pointer;" rel="'.$order['entry_id'].'"  class="" src="'.$theme_folder_url.'/cp_themes/default/images/button_close_subadmin.gif" /></a> <a href="'.$url_base.'order_view'.AMP.'id='.$order['entry_id'].'">'.$order['title'].'</a>', 
		'<span style="color:#'.m62_status_color($order['status'], $order_channel_statuses).'; cursor:pointer;" class="order_status_td" id="order_status_td_'.$order['entry_id'].'" rel="'.$order['entry_id'].'">'.lang($order['status']).'</span>'.form_dropdown('status', $statuses, $order['status'], 'id="change_status_'.$order['entry_id'].'" rel="'.$order['entry_id'].'" class="aj_order_status_change" style="display:none;"'),
		m62_format_money($order['order_total']),
		'<!-- '.$order['entry_date'].'-->'.m62_format_date($order['entry_date'], FALSE, TRUE),
		'<a class="nav_button" href="'.$packingslip_url.'" target="_blank" title="'.lang('packing_slip').'"><img src="'.$theme_folder_url.'/third_party/ct_admin/images/packingslip.png" alt="'.lang('packing_slip').'" /></a>'.
		'<a class="nav_button" href="'.$invoice_url.'" target="_blank" title="'.lang('invoice').'"><img src="'.$theme_folder_url.'/third_party/ct_admin/images/invoice.png" alt="'.lang('invoice').'" /></a>'
	);
	$this->table->add_row($data);
}

echo $this->table->generate();
// Clear out of the next one
$this->table->clear();
?>
</div>

<h3 class="accordion"><?=lang('customers_products').' ('.$total_products.') ';?></h3>
<div id="customers_products">
<?php 
// Add Markup into the table 
$this->table->set_heading(
	lang('product'),
	lang('status'),
	lang('orders'),
	lang('quantity'),
	
	lang('unit_price'),
	lang('total')
);

foreach($product_data AS $item)
{
	$cell = '<a href="'.$url_base.'product_view'.AMP.'id='.$item['entry_id'].'">'.$item['title'].'</a>';
	$package = FALSE;
	if(isset($item['extra']) && $item['extra'] != '')
	{
		if((is_array($item['extra']) && count($item['extra']) >= '1') && !isset($item['extra']['row_id']))
		{
			//print_r($options);
			$cell .= '<div class="subtext">(';
			if(isset($item['extra']['sub_items']) && is_array($item['extra']['sub_items']) && count($item['extra']['sub_items']) >= '1')
			{
				$package = TRUE;
				$cell .= count($item['extra']['sub_items']).' '.lang('product_package');
			}
			else
			{
				$options = $item['extra'];
				unset($options['_option_keys']);
				foreach($options AS $key => $value)
				{
					if($value == '')
					{
						unset($options[$key]);
					}
				}				
				$cell .= implode(' / ', $options);
			}

			$cell .= ')</div>';
		}
	}
	$data = array(
		$cell,
		'<span style="color:#'.m62_status_color($item['status'], $order_channel_statuses).'">'.lang($item['status']).'</span>',
		$item['product_sales'],
		$item['quantity'],
		m62_format_money(($item['price'])),
		m62_format_money($item['price']*$item['product_sales'])
	);
	$this->table->add_row($data);

	if($package)
	{
		//we have a package style product so we have to spit those out too

		//first, let's get the default options
		$sub_options = array();
		foreach($options As $key => $value)
		{
			if($key != 'sub_items')
			{
				$sub_options[] = $key;
			}
		}

		foreach($item['extra']['sub_items'] AS $sub_item)
		{
			$extra = '';
			$product_options = array();
			if( !empty($item['extra']['_option_keys']) && is_array($item['extra']['_option_keys']) )
			{
				foreach($item['extra']['_option_keys'] AS $option_key => $option_value)
				{
					if( !empty($sub_item[$option_key]) )
					{
						$product_options[] = $sub_item[$option_key];
					}
				}

				if( count($product_options) >= '1')
					$extra = '<div class="subtext">('.implode(' / ', $product_options).')</div>';
			}

			$cell = '<a href="'.$url_base.'product_view'.AMP.'id='.$sub_item['entry_id'].'">'.$sub_item['title'].'</a>'.$extra;
			$data = array(
					'<div class="ct_admin_package_sub_item">'.$cell.'</div>',
					'-',
					'-',
					'-',
					'-',
					'-'
			);
			$this->table->add_row($data);
		}
	}
}
								
echo $this->table->generate(); 
// Clear out of the next one 
$this->table->clear(); 
?>
</div>

<?php echo $hook_secondary_view; ?>

<h3 class="accordion"><?=lang('customer_address')?></h3>
<div><?php 
$this->table->set_heading(
			'&nbsp;',
			''
			);
			
			if(isset($customer_data['ee_member_id']))
			{
				$this->table->add_row(lang('first_name'), form_input('cust_first_name', $customer_data['cust_first_name']));
				$this->table->add_row(lang('last_name'), form_input('cust_last_name', $customer_data['cust_last_name']));
				$this->table->add_row(lang('address'), form_input('cust_address', $customer_data['cust_address']));
				$this->table->add_row(lang('address2'), form_input('cust_address2', $customer_data['cust_address2']));
				$this->table->add_row(lang('city'), form_input('cust_city', $customer_data['cust_city']));
				$this->table->add_row(lang('state'), form_input('cust_state', $customer_data['cust_state']));
				$this->table->add_row(lang('zip'), form_input('cust_zip', $customer_data['cust_zip']));
				$this->table->add_row(lang('country'), form_input('cust_country', $customer_data['cust_country']));
				$this->table->add_row('Country Code', form_input('cust_country_code', $customer_data['cust_country_code']));				
			}
			else
			{
				$this->table->add_row(lang('first_name'), $customer_data['cust_first_name']);
				$this->table->add_row(lang('last_name'), $customer_data['cust_last_name']);
				$this->table->add_row(lang('address'), $customer_data['cust_address']);
				$this->table->add_row(lang('address2'), $customer_data['cust_address2']);
				$this->table->add_row(lang('city'), $customer_data['cust_city']);
				$this->table->add_row(lang('state'), $customer_data['cust_state']);
				$this->table->add_row(lang('zip'), $customer_data['cust_zip']);	
				if(isset($customer_data['cust_country']))
				{
					$this->table->add_row(lang('country'), $customer_data['cust_country']);
				}
				
				if(isset($customer_data['cust_country_code']))
				{
					$this->table->add_row('Country Code', $customer_data['cust_country_code']);	
				}		
			}

			echo $this->table->generate();
			// Clear out of the next one
			$this->table->clear();
			?></div>
<h3 class="accordion"><?=lang('shipping_address')?></h3>
<div><?php 
$this->table->set_heading(
			'&nbsp;',
			''
			);
			if(isset($customer_data['ee_member_id']))
			{	
				$this->table->add_row(lang('first_name'), form_input('cust_shipping_first_name', $customer_data['cust_shipping_first_name']));
				$this->table->add_row(lang('last_name'), form_input('cust_shipping_last_name', $customer_data['cust_shipping_last_name']));
				$this->table->add_row(lang('address'), form_input('cust_shipping_address', $customer_data['cust_shipping_address']));
				$this->table->add_row(lang('address2'), form_input('cust_shipping_address2', $customer_data['cust_shipping_address2']));
				$this->table->add_row(lang('city'), form_input('cust_shipping_city', $customer_data['cust_shipping_city']));
				$this->table->add_row(lang('state'), form_input('cust_shipping_state', $customer_data['cust_shipping_state']));
				$this->table->add_row(lang('zip'), form_input('cust_shipping_zip', $customer_data['cust_shipping_zip']));
			}
			else
			{
				$this->table->add_row(lang('first_name'), $customer_data['cust_shipping_first_name']);
				$this->table->add_row(lang('last_name'), $customer_data['cust_shipping_last_name']);
				$this->table->add_row(lang('address'), $customer_data['cust_shipping_address']);
				$this->table->add_row(lang('address2'), $customer_data['cust_shipping_address2']);
				$this->table->add_row(lang('city'), $customer_data['cust_shipping_city']);
				$this->table->add_row(lang('state'), $customer_data['cust_shipping_state']);
				$this->table->add_row(lang('zip'), $customer_data['cust_shipping_zip']);
			}
				

			echo $this->table->generate();
			// Clear out of the next one
			$this->table->clear();
			?></div>
</div>

<?php echo $hook_tertiary_view; ?>
<?=form_close()?>

<div id="ct_admin_preview_modal">
	<div id="ct_admin_preview_modal_content"></div>
</div>