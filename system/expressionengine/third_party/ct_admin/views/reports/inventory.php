<?php $this->load->view('errors'); ?>
<?php $this->load->view('reports/sub_menu', array('active_url' => 'inventory_report')); ?>
<?php echo form_open($query_base.'reports', array('id'=>'my_accordion'))?> 
<?php
$this->table->set_template($cp_table_template); 
$this->table->set_empty("&nbsp;"); 
?> 
<div> 
	<?php if(isset($enable_low_stock_report)): ?>
	<h3 class="accordion"><?=lang('low_stock_report')?></h3> 
	<div id="low_stock_report"> 
		<div class="ct_top_nav">
			<div class="ct_nav">
				<span class="button"> 
					<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=low_stock_report'; ?>">Export</a>
				</span>
			</div>
		</div>	<br />
		<?php 
		if($low_stock_report && count($low_stock_report) != '0')
		{
			
			// Add Markup into the table 
			$this->table->set_heading(
				lang('product'),
				lang('quantity')
			);
			
			foreach($low_stock_report AS $report)
			{
				$url = $url_base.'product_view'.AMP.'id='.$report['entry_id'];
				//first check if we're dealing with a custom field.
				if(is_numeric($report['inventory'])) 
				{
					$data = array(
							'<a href="'.$url.'">'.$report['title'].'</a>', 
							$report['inventory']
					);
					$this->table->add_row($data);					
				}
			}
											
			echo $this->table->generate(); 
			// Clear out of the next one 
			$this->table->clear(); 
		}
		else
		{
			echo '<br />'.lang('nothing_to_report');
		}
		?>
	</div>
	<?php endif; ?>
</div>		
<?php echo form_close()?>