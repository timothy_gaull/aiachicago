<div class="ct_top_nav">
	<div class="ct_nav">
	
		<?php 
		foreach($menu_data AS $key => $value): ?>
		<span class="button"> 
			<a class="nav_button <?php echo ($active_url == $value['url'] ? 'current' : ''); ?>" href="<?php echo $url_base.$value['url']; ?>"><?php echo lang($key.'_reports_menu')?></a>
		</span>
		<?php endforeach; ?>	
			
	</div>
</div>