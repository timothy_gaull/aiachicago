<?php $this->load->view('errors'); ?>
<?php $this->load->view('reports/sub_menu', array('active_url' => 'product_sum_report')); ?>
<?php echo form_open($query_base.'reports', array('id'=>'my_accordion'))?> 
<?php
$this->table->set_template($cp_table_template); 
$this->table->set_empty("&nbsp;"); 
?> 
<div> 
	
	<h3 class="accordion"><?=lang('report')?></h3> 
		<div id="product_sum_report"> 
			<div class="ct_top_nav">
				<div class="ct_nav">
					<span class="button"> 
						<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=product_sum_report'; ?>">Export</a>
					</span>
					<span class="button"> 
						<a class="nav_button" href="javascript:;" id="product_sum_chart_opener">Chart</a>
					</span>						
				</div>
			</div> <br />		
		<?php 
		if($product_sum_report && count($product_sum_report) != '0')
		{
			?>			
			<?php 
			// Add Markup into the table 
			$this->table->set_heading(
				lang('product'),
				lang('quantity'),
				lang('unit_price'),
				lang('total'),
				//lang('first_purchase'),
				lang('last_purchase')
			);
			foreach($product_sum_report AS $report)
			{
				$data = array(
						'<a href="'.$url_base.'product_view'.AMP.'id='.$report['entry_id'].'">'.$report['title'].'</a> ('.m62_format_money($report['price']).')', 
						$report['total_quantity'], 
						m62_format_money($report['price']), 
						m62_format_money($report['total_quantity']*$report['price']),
						//'<!-- '.$report['first_purchase'].'-->'.m62_convert_timestamp($report['first_purchase']),
						'<!-- '.$report['last_purchase'].'-->'.m62_format_date($report['last_purchase'], FALSE, TRUE)
				);
				$this->table->add_row($data);
			}
											
			echo $this->table->generate(); 
			// Clear out of the next one 
			$this->table->clear(); 
		}
		else
		{
			echo '<br />'.lang('nothing_to_report');
		}
		?>
		<div id="product_chart" title="<?=lang('product_sum_report')?>">
			<div id="product_chart_div"></div>
		    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
		    <script type="text/javascript">
		      google.load("visualization", "1", {packages:["corechart"]});
		      google.setOnLoadCallback(drawChart);
		      function drawChart() {
		        var data = new google.visualization.DataTable();
		        data.addColumn('string', 'Product/Price');
		        data.addColumn('number', 'Total');
		        data.addRows([
					<?php
					$items = array(); 
					$i = 1;
					$total = count($product_sum_report);
					foreach($product_sum_report AS $report)
					{
						echo "['".addslashes($report['title'])." (".$report['total_quantity']."x".m62_format_money($report['price']).")',    ".$report['total_quantity']*$report['price']."]";
						if($i != $total)
						{
							echo ",";
						}
						$i++;
					}
					?>
		        ]);

		        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
				var chart_width = 820;				
		        var options = {
		            width: chart_width, 
		            height: 390, 
		            title: 'Revenue',
		            chartArea: { width: 680, height:300, top: 30,left:50},
		            backgroundColor: 'none'		          
		        };
		
		        var chart = new google.visualization.PieChart(document.getElementById('product_chart_div'));

		        var formatter = new google.visualization.NumberFormat({prefix: '<?php echo $number_prefix; ?>', negativeColor: 'red', negativeParens: true});
		        formatter.format(data, 1);
		        			        
		        chart.draw(data, options);
		      }
		    </script>
		    
			<div id="chart_div"></div>
	    </div>
		
					
	</div>
</div>		
<?php echo form_close()?>