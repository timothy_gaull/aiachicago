<?php $this->load->view('errors'); ?>
<?php $this->load->view('reports/sub_menu', array('active_url' => '')); ?>
<?php echo form_open($query_base.'reports', array('id'=>'my_accordion'))?> 
<?php
$this->table->set_template($cp_table_template); 
$this->table->set_empty("&nbsp;"); 
?> 
<div> 
<br clear="all" />
	<div> 	
		<?php 

			$this->table->set_heading(
				lang('current_day_total'),
				lang('current_month_total'),
				lang('current_year_total'),
				lang('total_sales')
			);
			$data = array(
				m62_format_money($current_day_total), 
				m62_format_money($current_month_total), 
				m62_format_money($current_year_total),
				m62_format_money($total_sales)
			);
			$this->table->add_row($data);	
			echo $this->table->generate(); 
			// Clear out of the next one 
			$this->table->clear(); 
		
		?>	
	</div>
	
	<h3 class="accordion"><?=lang('details')?></h3> 
	<div id="details">
		<div id="chart_div"></div>
	    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	    <script type="text/javascript">

			function getHistoryReportUrl(date_str){
			
				var date = Date.parse(date_str + ',1');
				if(!isNaN(date)){
					var month = new Date(date).getMonth() + 1;
					if( month <= 9 )
					{
						month = '0'+month;
					}
					var year = new Date(date).getFullYear();
					return 'month='+month+'&year='+year;
				}
				return -1;
			}
	      google.load("visualization", "1", {packages:["corechart"]});
	      google.setOnLoadCallback(drawChart);
	      function drawChart() {
	        var data = new google.visualization.DataTable();
	        data.addColumn('string', 'Year');
	        data.addColumn('number', 'Totals');
	        data.addColumn('number', 'Discount');
	        data.addColumn('number', 'Tax');
	        data.addColumn('number', 'Shipping');
	        data.addRows(<?php echo count($all_totals);?>);
	        <?php 
	        $i = 0;
	        $totals = array_reverse($all_totals, TRUE);
	        foreach($totals AS $report)
	        { 
	        	echo "data.setValue($i, 0, '".m62_convert_timestamp(strtotime($report['name']), "%M %Y")."');";
	        	echo "data.setValue($i, 1, ".$report['total'].");";
	        	echo "data.setValue($i, 2, ".$report['discount'].");";
	        	echo "data.setValue($i, 3, ".$report['tax'].");";
	        	echo "data.setValue($i, 4, ".$report['shipping'].");";
	        	$i++;
	        }
	        ?>
	
	        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			var chart_width = document.getElementById("chart_div").offsetWidth+10;
			var area_width = chart_width+20;
			//alert(chart_width);

	        var options = {
		            width: chart_width, 
		            height: 248, 
		            backgroundColor: 'none',
		            hAxis: {slantedText: true},
		            legend: { position: 'right' }, 
		            legend:'in',
		            chartArea: {
		            	width: area_width, 
		            	height: "160",
		            	top: 20,
		            	left:40
		            } 
		    };

	        var formatter = new google.visualization.NumberFormat({prefix: '<?php echo $number_prefix; ?>', negativeColor: 'red', negativeParens: true});
	        formatter.format(data, 1);
	        formatter.format(data, 2);		    
	        chart.draw(data, options);

	        google.visualization.events.addListener(chart, 'onmouseover', function () {
	        	$("body").css("cursor", "pointer");
	        });

	        google.visualization.events.addListener(chart, 'onmouseout', function () {
	        	$("body").css("cursor", "auto");
	        });	        
	        	        
	        google.visualization.events.addListener(chart, 'select', function() {
	            // grab a few details before redirecting
	            var selection = chart.getSelection();
	            var row = selection[0].row;
	            var col = selection[0].column;
	            var date = data.getValue(row, 0);
	            var url = getHistoryReportUrl(date);
	            location.href = '<?php echo html_entity_decode($url_base.'history_report'.AMP); ?>' + url;
	          });		        
	
	
	      }
	    </script>	
	</div>	
	
	
	<h3 class="accordion"><?php echo lang('monthly_history_report')?></h3> 
	<div id="monthly_history_report"> 
		<div class="ct_top_nav">
			<div class="ct_nav">
				<span class="button"> 
					<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=monthly_history_report'; ?>">Export</a>
				</span>		
				
				
			</div>
		</div><br />	
		<?php 
		
		if(is_array($all_totals) && count($all_totals) >= 1)
		{
			$this->table->set_heading(
				lang('date'),
				lang('tax'),
				lang('shipping'),
				lang('discount'),
				lang('total')
			);
			
			foreach($all_totals AS $report)
			{
			$data = array(
					'<!-- '.$report['entry_date'].'--><a href="'.$url_base.'history_report&month='.date('m', $report['entry_date']).'&year='.date('Y', $report['entry_date']).'">'.$report['name'].'</a>', 
					m62_format_money($report['tax']), 
					m62_format_money($report['shipping']), 
					m62_format_money($report['discount']), 
					m62_format_money($report['total'])
			);
			$this->table->add_row($data);				 
			}
			echo $this->table->generate();
			$this->table->clear(); 			
		}
		else
		{
			echo '<br />'.lang('nothing_to_report');
		}
		?>
	</div>
	
</div>		
<?php echo form_close()?>