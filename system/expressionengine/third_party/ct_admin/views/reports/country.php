<?php $this->load->view('errors'); ?>
<?php $this->load->view('reports/sub_menu', array('active_url' => 'country_report')); ?>
<?php echo form_open($query_base.'reports', array('id'=>'my_accordion'))?> 
<?php
$this->table->set_template($cp_table_template); 
$this->table->set_empty("&nbsp;"); 
?> 
<div> 
	<h3 class="accordion"><?=lang('report')?></h3>
	
	<?php if($country_data): ?>
	<br clear="all" />
		
	<div id="country_report"> 
	
		<div id='location_chart'></div>
		<script type='text/javascript' src='https://www.google.com/jsapi'></script>
		
		<script type='text/javascript'>
		//var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		var chart_width = document.getElementById("location_chart").offsetWidth-30;
		var area_width = chart_width+20;  
		google.load('visualization', '1', {'packages': ['geochart']});
		google.setOnLoadCallback(drawRegionsMap);
	
		function drawRegionsMap() {
			var data = new google.visualization.DataTable();
			data.addColumn('string', 'Country');
			data.addColumn('number', 'Popularity');
			data.addRows([
			<?php
			$arr = array(); 
			foreach($country_data AS $key => $value)
			{
				$arr[] = "['".m62_country_code($key)."', ".$value."]";
			}
			
			echo implode(',', $arr);
			?>
			]);
	
			var options = {width:chart_width, height:'500', backgroundColor:'none'};
			var chart = new google.visualization.GeoChart(document.getElementById('location_chart'));
			chart.draw(data, options);
		}	
		</script>
	</div>
	<?php endif; ?>	
</div>		
<?php echo form_close()?>