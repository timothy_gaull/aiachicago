<?php if( $submit_action == 'packing_slip'): ?>
<h3>Packing Slips for:</h3>
<?php else: ?>
<h3>Printing Invoices for:</h3>
<?php endif; ?>
<p>
<?php foreach($data AS $item): ?>
	<?php echo $item['title'];?><br />
<?php endforeach; ?>
</p>
<p>
<a href="<?php echo $view_action_url; ?>" target="_blank">
	<?php if( $submit_action == 'packing_slip'): ?>Print the Packing Slips<?php else: ?>Print the Invoices<?php endif; ?>
</a> (will open in new window)
</p>

<h3>Update Status</h3>
<p>Once you print your invoices/packing slips you can update all these Order's status here.</p>
<?=form_open($form_action)?>
<?php foreach($damned as $id):?>
	<?=form_hidden('orders[]', $id)?>
<?php endforeach;?>
<p>
	<?=form_dropdown('status', $statuses, $status_selected, 'id="change_status"').NBS.NBS?>
	<?=form_submit(array('name' => 'submit', 'value' => lang('change_status'), 'class' => 'submit'))?>
</p>

<?=form_close()?>