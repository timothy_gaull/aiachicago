<?=form_open($form_action)?>
<?php foreach($damned as $id):?>
	<?=form_hidden('orders[]', $id)?>
<?php endforeach;?>

<p class="notice"><?=lang('action_can_not_be_undone')?></p>

<h3><?=lang($change_status_question); ?></h3>
<p>
<?php foreach($data AS $item): ?>
	<?php echo $item['title'];?><br />
<?php endforeach; ?>
</p>

<p>
	<?=form_dropdown('status', $statuses, $status_selected, 'id="change_status"').NBS.NBS?>
	<?=form_submit(array('name' => 'submit', 'value' => lang('change_status'), 'class' => 'submit'))?>
</p>

<?=form_close()?>