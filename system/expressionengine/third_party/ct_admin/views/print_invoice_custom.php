<?php
/**
 * WAIT!! Don't edit this file.
 * If you want to customize the print invoice rename this file to or use the template tags:
 * print_invoice_custom.php
 *
 * This will prevent the template from being overwritten when you upgrade :)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo lang('print_invoice_title'); ?></title>
<style>
body, td, th, input, select, textarea, option, optgroup {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
h1 {
	text-transform: uppercase;
	color: #CCCCCC;
	text-align: right;
	font-size: 24px;
	font-weight: normal;
	padding-bottom: 5px;
	margin-top: 0px;
	margin-bottom: 15px;
	border-bottom: 1px solid #CDDDDD;
}
.content-wrap {
	width: 100%;
	margin-bottom: 20px;
}
.heading td {
	background: #ECF1F4;
}
.address, .product {
	border-collapse: collapse;
}
.address {
	width: 100%;
	margin-bottom: 20px;
	border-top: 1px solid #CDDDDD;
	border-right: 1px solid #CDDDDD;
}
.address th, .address td {
	border-left: 1px solid #CDDDDD;
	border-bottom: 1px solid #CDDDDD;
	padding: 5px;
}
.address td {
	width: 50%;
}
.product {
	width: 100%;
	margin-bottom: 20px;
	border-top: 1px solid #CDDDDD;
	border-right: 1px solid #CDDDDD;
}
.product td {
	border-left: 1px solid #CDDDDD;
	border-bottom: 1px solid #CDDDDD;
	padding: 5px;
}
.package_sub_item {
	padding-left:20px;
	background:url('<?php echo $theme_folder_url; ?>third_party/ct_admin/images/line_break.png') no-repeat;
}

</style>
</head>
<body>
<?php foreach($order_details AS $order_id => $order): ?>
	<div style="page-break-after: always;">
		<?php if($order['order_transaction_id'] != ''): ?>
		<h1><?php echo ($packing_slip ? 'Packing Slip for order number:' : 'Invoice'); ?> ##<?php echo $order['order_transaction_id']; ?></h1>
		<?php endif; ?>

		<div class="content-wrap">
		<table width="100%">
			<tr>
				<td>
					<?php echo $site_name; ?><br />
					<?php echo $store_email; ?><br />
					<?php echo $site_url; ?>
				</td>
				<td align="right" valign="top">
					<table>
					<tr>
						<td><b><?php echo lang('order_date'); ?>:</b></td>
						<td><?php echo m62_convert_timestamp($order['entry_date']); ?></td>
					</tr>
					<tr>
						<td><b><?php echo ($order['order_transaction_id'] != '' ? lang('order_transaction_id').':' : ''); ?></b></td>
						<td>
							<?php if($order['order_transaction_id'] != ''): ?>
							#<?php echo $order['order_transaction_id']; ?>
							<?php endif; ?>
						</td>
					</tr>
					</table>
				</td>
			</tr>
		</table>
		</div>
		<table class="address">
			<tr class="heading">
				<td width="50%"><b>To</b></td>
				<td width="50%"><b>Ship To</b></td>
			</tr>
			<tr>
			<td>
				<?php echo $order['order_business_name']; ?><br />
				<?php echo $order['orders_billing_first_name'].' '.$order['orders_billing_last_name']; ?><br />
				<?php echo ($order['orders_billing_address'] ? $order['orders_billing_address'].'<br />' : ''); ?>
				<?php echo ($order['orders_billing_address2'] ? $order['orders_billing_address2'].'<br />' : ''); ?>
				<?php echo ($order['orders_billing_city'] ? $order['orders_billing_city'].',' : ''); ?> <?php echo $order['orders_billing_state']; ?> <?php echo $order['orders_billing_zip']; ?><br/>
				<?php echo $order['orders_customer_email']?>
			</td>
			<td>
				<?php echo $order['order_business_name']; ?><br />
				<?php echo $order['orders_shipping_first_name'].' '.$order['orders_shipping_last_name']; ?><br />
				<?php echo ($order['orders_shipping_address'] ? $order['orders_shipping_address'].'<br />' : ''); ?>
				<?php echo ($order['orders_shipping_address2'] ? $order['orders_shipping_address2'].'<br />' : ''); ?>
				<?php echo ($order['orders_shipping_city'] ? $order['orders_shipping_city'].',' : ''); ?> <?php echo $order['orders_shipping_state']; ?> <?php echo $order['orders_shipping_zip']; ?>
			</td>
			</tr>
		</table>
		<table class="product">
			<tr class="heading">
				<td><b><?php echo lang('product'); ?></b></td>
				<td align="right"><b>Quantity</b></td>
				<?php if(!$packing_slip): ?>
					<td align="right"><b>Unit Price</b></td>
					<td align="right"><b>Total</b></td>
				<?php endif; ?>
			</tr>
			<?php foreach($order['order_items'] AS $item): ?>

			<tr>
			<td>
				<?php
				$cell = $item['title'];
				$package = FALSE;
				if(isset($item['extra']) && $item['extra'] != '')
				{
					if(is_array($item['extra']) && count($item['extra']) >= '1')
					{
						if(isset($item['extra']['sub_items']) && is_array($item['extra']['sub_items']) && count($item['extra']['sub_items']) >= '1')
						{
							$package = TRUE;
							$cell .= count($item['extra']['sub_items']).' '.lang('product_package');
						}
						else
						{
							if( !empty($item['extra']['_option_keys']) )
							{
								unset($item['extra']['_option_keys']);
							}
							$cell .= ' ('.implode(' / ', $item['extra']).')';
						}
					}
				}

				echo $cell;

				?>
			</td>
			<td align="right"><?php echo $item['quantity']; ?></td>
			<?php if(!$packing_slip): ?>
				<td align="right"><?php echo $number_format_defaults_prefix.$item['price']; ?></td>
				<td align="right"><?php echo $number_format_defaults_prefix.($item['quantity']*$item['price']); ?></td>
			<?php endif; ?>
			</tr>

			<?php  if($package): ?>
				<?php foreach($item['extra']['sub_items'] AS $sub_item) :

						$extra = '';
						$product_options = array();
						if( !empty($item['extra']['_option_keys']) && is_array($item['extra']['_option_keys']) )
						{
							foreach($item['extra']['_option_keys'] AS $option_key => $option_value)
							{
								if( !empty($sub_item[$option_key]) )
								{
									$product_options[] = $sub_item[$option_key];
								}
							}

							if( count($product_options) >= '1')
							$extra = '<div class="subtext">('.implode(' / ', $product_options).')</div>';
						}

						$cell = $sub_item['title'].$extra;
						?>
						<tr>
							<td><div class="package_sub_item"><?php echo $cell; ?></div></td>
							<td align="right">-</td>
							<?php if(!$packing_slip): ?>
							<td align="right">-</td>
							<td align="right">-</td>
							<?php endif; ?>
						</tr>
					<?php endforeach; ?>
			<?php endif; ?>

			<?php endforeach; ?>
			<?php if(!$packing_slip): ?>
			<tr>
				<td align="right" colspan="3"><b>Sub-Total:</b></td>
				<td align="right"><?php echo $number_format_defaults_prefix.$order['order_subtotal']; ?></td>
			</tr>
			<tr>
				<td align="right" colspan="3"><b>Shipping:</b></td>
				<td align="right"><?php echo $number_format_defaults_prefix.$order['order_shipping']; ?></td>
			</tr>
			<tr>
				<td align="right" colspan="3"><b>Tax:</b></td>
				<td align="right"><?php echo $number_format_defaults_prefix.$order['order_tax']; ?></td>
			</tr>
			<tr>
				<td align="right" colspan="3"><b>Discount:</b></td>
				<td align="right"><?php echo $number_format_defaults_prefix.$order['order_discount']; ?></td>
			</tr>
			<tr>
				<td align="right" colspan="3"><b>Total:</b></td>
				<td align="right"><?php echo $number_format_defaults_prefix.$order['order_total']; ?></td>
			</tr>
			<?php endif; ?>
		</table>
	</div>
<?php endforeach; ?>
</body>
</html>
