<div class="ct_top_nav">
	<div class="ct_nav">
	
		<?php foreach($menu_data AS $key => $value): ?>
		<span class="button"> 
			<a class="nav_button" href="<?php echo $value['url']; ?>" target="<?php echo ($value['target'] == '' ? '_parent' : $value['target']); ?>"><?php echo lang($key)?></a>
		</span>
		<?php endforeach; ?>		
	</div>
</div>