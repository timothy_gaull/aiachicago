<?php $this->load->view('errors'); ?>
<?=form_open($query_base.'update_product', array('id'=>'my_accordion'))?>
<?php
echo form_hidden('product_id', $product_id);
$this->table->set_template($cp_table_template);
$this->table->set_empty("&nbsp;");
?>	
<?php $this->load->view('includes/sub_menu'); ?>

<br clear="all" />
<div>
<?php 
$this->table->set_heading(lang('status'), lang('price'), lang('total_sold'),lang('total_sales'), lang('total_customers'), lang('first_order'), lang('last_order'));
$data = array(
	'<span style="color:#'.m62_status_color($product_details['status'], $order_channel_statuses).'">'.lang($product_details['status']).'</span>', 
	'<span title="'.lang('current_price').'">'.m62_format_money($product_details['price']).'</span> <span title="'.lang('average_paid_price').'">('.m62_format_money($product_details['average_paid_price']).')</span>',
	'<span title="'.lang('successful_sold_quantity').'">'.number_format($successful_quantity).'</span> <span title="'.lang('total_sold_quantity').'">('.number_format($product_details['total_sold']).')</span>',
	'<span title="'.lang('successful_amount_earned').'">'.m62_format_money($successful_sold).'</span> <span title="'.lang('total_processed_amount').'">('.m62_format_money($product_details['total_sales']).')</span>',
	number_format($total_customers),
	m62_format_date($product_meta['first_order']['entry_date'], FALSE, TRUE),
	m62_format_date($product_meta['last_order']['entry_date'], FALSE, TRUE)
);
$this->table->add_row($data);
echo $this->table->generate();
$this->table->clear();
?>
</div>

<?php echo $hook_view; ?>


<h3 class="accordion"><?php echo lang('details')?></h3> 
<div id="monthly_history_report"> 
		
	<div id="chart_div"></div>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
	function getHistoryReportUrl(date_str){
		
		var date = Date.parse(date_str + ',1');
		if(!isNaN(date)){
			var month = new Date(date).getMonth() + 1;
			if( month <= 9 )
			{
				month = '0'+month;
			}
			var year = new Date(date).getFullYear();
			return 'month='+month+'&year='+year;
		}
		return -1;
	}    
      google.load("visualization", "1", {packages:["corechart"]});
	  var chart_width = document.getElementById("chart_div").offsetWidth-60;
	  var area_width = chart_width+20;

      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Year');
        data.addColumn('number', 'Totals');
        data.addColumn('number', 'Discount');
        data.addRows(<?php echo count($all_totals);?>);
        <?php 
        $i = 0;
        $totals = array_reverse($all_totals, TRUE);
        foreach($totals AS $report)
        { 
        	echo "data.setValue($i, 0, '".m62_convert_timestamp(strtotime($report['name']), "%M %Y")."');";
        	echo "data.setValue($i, 1, ".$report['total'].");";
        	echo "data.setValue($i, 2, ".$report['discount'].");";
        	$i++;
        }
        ?>

        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		//var chart_width = 820;
		//alert(chart_width);

        var options = {
	            width: chart_width, 
	            height: 248, 
	            legend:'in', 
	            select: 'myClickHandler',
	            hAxis: {slantedText: true},
	            backgroundColor: 'none',
	            chartArea: {
	            	width: area_width, 
	            	height: "160",
	            	top: 20,
	            	left:50
	            }           
			};

        var formatter = new google.visualization.NumberFormat({prefix: '<?php echo $number_prefix; ?>', negativeColor: 'red', negativeParens: true});
        formatter.format(data, 1);
        formatter.format(data, 2);		    
        chart.draw(data, options);

        google.visualization.events.addListener(chart, 'onmouseover', function () {
        	$("body").css("cursor", "pointer");
        });

        google.visualization.events.addListener(chart, 'onmouseout', function () {
        	$("body").css("cursor", "auto");
        });	        
        	        
        google.visualization.events.addListener(chart, 'select', function() {
            // grab a few details before redirecting
            var selection = chart.getSelection();
            var row = selection[0].row;
            var col = selection[0].column;
            var date = data.getValue(row, 0);
            var url = getHistoryReportUrl(date);
            location.href = '<?php echo html_entity_decode($url_base.'history_report'.AMP.'product_id='.$product_id.AMP); ?>' + url;
          });        


      }
    </script>		
</div>

<h3 class="accordion"><?php echo lang('monthly_history_report')?></h3> 
<div id="monthly_history_report_table"> 
	<div class="ct_top_nav">
		<div class="ct_nav">
			<span class="button"> 
				<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=product_monthly_history_report'.AMP.'product_id='.$product_id; ?>">Export</a>
			</span>
		</div>
	</div><br />
	<?php 
	
	if(is_array($all_totals) && count($all_totals) >= 1)
	{
		$this->table->set_heading(
			lang('date'),
			lang('tax'),
			lang('shipping'),
			lang('discount'),
			lang('total')
		);

		foreach($all_totals AS $report)
		{
			$report_link = ($report['total'] == '0' ? $report['name'] : '<a title="'.$report['name'].'" href="'.$url_base.'history_report&month='.date('m', $report['entry_date']).'&year='.date('Y', $report['entry_date']).'&product_id='.$product_id.'">'.$report['name'].'</a>'); 
			$data = array(
					'<!-- '.$report['entry_date'].'-->'.$report_link, 
					m62_format_money($report['tax']), 
					m62_format_money($report['shipping']), 
					m62_format_money($report['discount']), 
					m62_format_money($report['total'])
			);
			$this->table->add_row($data);				 
		}
		echo $this->table->generate();
		$this->table->clear(); 			
	}
	else
	{
		echo '<br />'.lang('nothing_to_report');
	}

	?>
</div>

<?php echo $hook_secondary_view; ?>

<h3 class="accordion"><?=lang('product_sum_report')?></h3> 
	<div id="product_sum_report"> 
		<div class="ct_top_nav">
			<div class="ct_nav">
			<!-- 
				<span class="button"> 
					<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=product_sum_report&product_id='.$product_id; ?>">Export</a>
				</span>
			 -->
				<span class="button"> 
					<a class="nav_button" href="javascript:;" id="product_sum_chart_opener">Chart</a>
				</span>						
			</div>
		</div> <br />		
	<?php 
	if($product_sum_report && count($product_sum_report) != '0')
	{
		?>			
		<?php 
		// Add Markup into the table 
		$this->table->set_heading(
			//lang('product'),
			lang('unit_price'),
			lang('quantity'),
			lang('total'),
			lang('first_purchase'),
			lang('last_purchase')
		);
		foreach($product_sum_report AS $report)
		{
			$data = array(
					//'<a href="?'.AMP.'D=cp'.AMP.'C=content_publish'.AMP.'M=entry_form'.AMP.'channel_id='.$report['product_channel_id'].AMP.'entry_id='.$report['entry_id'].'">'.$report['title'].'</a>', 
					m62_format_money($report['price']), 
					$report['total_quantity'],
					m62_format_money($report['total_quantity']*$report['price']),
					'<!-- '.$report['first_purchase'].'-->'.m62_format_date($report['first_purchase'], FALSE, TRUE),
					'<!-- '.$report['last_purchase'].'-->'.m62_format_date($report['last_purchase'], FALSE, TRUE)
			);
			$this->table->add_row($data);
		}
										
		echo $this->table->generate(); 
		// Clear out of the next one 
		$this->table->clear(); 
	}
	else
	{
		echo '<br />'.lang('nothing_to_report');
	}
	?>
	<div id="product_chart" title="<?=lang('product_sum_report')?>">
		<div id="product_chart_div"></div>
	    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
	    <script type="text/javascript">
	      google.load("visualization", "1", {packages:["corechart"]});
	      google.setOnLoadCallback(drawChart);
	      function drawChart() {
	        var data = new google.visualization.DataTable();
	        data.addColumn('string', 'Product/Price');
	        data.addColumn('number', 'Total');
	        data.addRows([
				<?php
				$items = array(); 
				$i = 1;
				$total = count($product_sum_report);
				foreach($product_sum_report AS $report)
				{
					echo "['".addslashes($report['title'])." (".$report['total_quantity']."x".m62_format_money($report['price']).")',    ".$report['total_quantity']*$report['price']."]";
					if($i != $total)
					{
						echo ",";
					}
					$i++;
				}
				?>
	        ]);

	        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
			var chart_width = 820;				
	        var options = {
	            width: chart_width, 
	            height: 390, 
	            title: 'Revenue',
	            chartArea: { width: 680, height:300, top: 30,left:50},
	            backgroundColor: 'none'		          
	        };
	
	        var chart = new google.visualization.PieChart(document.getElementById('product_chart_div'));

	        var formatter = new google.visualization.NumberFormat({prefix: '<?php echo $number_prefix; ?>', negativeColor: 'red', negativeParens: true});
	        formatter.format(data, 1);
	        			        
	        chart.draw(data, options);
	      }
	    </script>
    </div>	
</div>


<?php if(count($abandoned_carts) > 0) { ?>
<h3 class="accordion"><?php echo count($abandoned_carts); ?> <?=lang('abandoned_cart_groups')?></h3>
<div id="product_abandoned_carts">
	<div class="ct_top_nav">
		<div class="ct_nav">
			<span class="button"> 
				<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=product_abandoned_carts&product_id='.$product_id; ?>">Export</a>
			</span>					
		</div>
	</div> <br />

<?php 
	$this->table->set_heading(
		lang('screen_name'),
		lang('email'),
		lang('potential_revenue'),
		lang('total_in_cart'),
		lang('first_added'),
		lang('last_added')
	);
	
	foreach($abandoned_carts as $cart)
	{
		$customer_link = lang('guests');
		$customer_email = lang('guests');
		if( ! empty($cart['email']) )
		{
			$customer_link = '<a href="'.$url_base.'product_abandoned_cart_overlay'.AMP.'product_id='.$product_details['entry_id'].AMP.'member_id='.$cart['member_id'].'" class="fancybox_overlay"><img style="padding-right:5px; cursor:pointer;" rel="'.$product_details['entry_id'].'"  class="" src="'.$theme_folder_url.'/cp_themes/default/images/button_close_subadmin.gif" /></a> <a href="'.$url_base.'customer_view'.AMP.'email='.$cart['email'].'">'.$cart['screen_name'].'</a>';
			$customer_email = '<a href="mailto:'.$cart['email'].'">'.$cart['email'].'</a>';
		}
		$this->table->add_row(
			$customer_link,
			$customer_email,
			m62_format_money( $cart['potential_revenue'] ),
			$cart['total_in_cart'],
			m62_format_date($cart['first_added'], FALSE, TRUE),
			m62_format_date($cart['last_added'], FALSE, TRUE)
		);
	}
	echo $this->table->generate();
?>

</div>	
<?php } ?>

<h3 class="accordion"><?php echo lang('top')?> <?php echo count($customers); ?> <?=lang('customers')?></h3>
<div id="product_customers">
	<div class="ct_top_nav">
		<div class="ct_nav">
			<span class="button"> 
				<a class="nav_button " href="<?php echo $url_base.'export'.AMP.'type=product_product_customers&product_id='.$product_id; ?>">Export</a>
			</span>					
		</div>
	</div> <br />

<?php 
if(count($customers) > 0)
{		
	$this->table->set_heading(
		lang('name'),
		lang('email'),
		lang('sales'),
		lang('orders'),
		lang('last_order')
	);
	
	foreach($customers as $customer)
	{				  
		$this->table->add_row(
			'<a href="'.$url_base.'customer_view'.AMP.'email='.$customer['email'].'">'.$customer['first_name'].' '.$customer['last_name'].'</a>',
			'<a href="mailto:'.$customer['email'].'">'.$customer['email'].'</a>',
			m62_format_money($customer['total_sales']),
			$customer['total_orders'],
			m62_format_date($customer['last_order'], FALSE, TRUE)
		);
	}
	echo $this->table->generate();
?>


<?php } else { ?>
<?php echo lang('no_matching_customers')?>
<?php } ?>
</div>	

<?php echo $hook_tertiary_view; ?>
<?=form_close()?>

<div id="ct_admin_preview_modal">
	<div id="ct_admin_preview_modal_content"></div>
</div>