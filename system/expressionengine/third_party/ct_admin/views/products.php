<?php 
$this->load->view('errors'); 
?>
<div class="clear_left shun"></div>
<?php 
if(count($products) > 0)
{
?>
	<div style="clear:left"></div>
	
    <?=form_open($query_base.'edit_products_ajax_filter', array('id' => 'product_form'))?>
	<div id="filterMenu">
		<fieldset>
			<legend><?=lang('total_products')?> <?=m62_format_number($total_products, FALSE); ?></legend>


			<div class="group">
				<?=form_dropdown('channel_id', $channel_options, $default_channel_id, 'id="channel_id"')?> 
				<?=form_dropdown('status', $success_statuses, $status_selected, 'id="f_status"').NBS.NBS?>
				<?=form_dropdown('date_range', $date_select_options, $date_selected, 'id="date_range"').NBS.NBS?>
				<input type="hidden" value="" name="perpage" id="f_perpage" />
				<input type="hidden" value="<?php echo $default_start_date; ?>" id="default_start_date" />
				<input type="hidden" value="<?php //echo $minmax_orders['min_product']; ?>" id="filter_min_product" />
				<input type="hidden" value="<?php //echo $minmax_orders['max_product']; ?>" id="filter_max_product" />
				
			</div>
			<div id="custom_date_picker" style="display: none; margin: 0 auto 25px auto;width: 500px; height: 245px; padding: 5px 15px 5px 15px; border: 1px solid black;  background: #FFF;">
				<div id="cal1" style="width:250px; float:left; text-align:center;">
					<p style="text-align:left; margin-bottom:5px"><?=lang('start_date', 'custom_date_start')?>:&nbsp; <input type="text" name="custom_date_start" id="custom_date_start" value="yyyy-mm-dd" size="12" /></p>
					<span id="custom_date_start_span"></span>
				</div>
				<div id="cal2" style="width:250px; float:left; text-align:center;">
					<p style="text-align:left; margin-bottom:5px"><?=lang('end_date', 'custom_date_end')?>:&nbsp; <input type="text" name="custom_date_end" id="custom_date_end" value="yyyy-mm-dd" size="12" /></p>
					<span id="custom_date_end_span"></span>          
				</div>
			</div>
			
			<!--  
			<div id="total_range_picker" style="display: none; margin: 0 auto 25px auto;width: 500px; height: 60px; padding: 5px 15px 5px 15px; border: 1px solid black;  background: #FFF;">
				<input type="text" id="amount" style="border:0; color:#f6931f; font-weight:bold;" />
				<div id="slider-range"></div>
			</div>			
			-->
			
									
			<p>
				<?=form_label(lang('keywords').NBS, 'k_search', array('class' => 'field js_hide'))?>
				<?=form_input(array('id'=>'product_keywords', 'name'=>'product_keywords', 'class'=>'field', 'placeholder' => lang('keywords'), 'value'=>$order_keywords))?>
				&nbsp;&nbsp;
				<?=form_submit('submit', lang('search'), 'id="filter_products_submit" class="submit"')?>
				&nbsp;&nbsp;
				<?=form_submit('submit', lang('export'), 'id="export_submit" class="submit"')?>				 
			</p>
		</fieldset>
	</div>
    <?=form_close()?>	
<?php 

	echo form_open($query_base.'action_product_confirm', array('id' => 'products_form_actions')); 
	
	$this->table->set_template($cp_pad_table_template);
	$this->table->set_heading(
		lang('product_title').'/'.lang('edit'),
		lang('status'),
		lang('creation_date'),
		lang('total_sold'),
		lang('average_paid_price'),
		lang('total_sales')
		//form_checkbox('select_all', 'true', FALSE, 'class="toggle_all" id="select_all"').NBS.lang('select_all', 'select_all')
	);

	foreach($products as $product)
	{
		$toggle = array(
				  'name'		=> 'toggle[]',
				  'id'		=> 'edit_box_'.$product['entry_id'],
				  'value'		=> $product['entry_id'],
				  'class'		=>'toggle',
				  'disabled'		=> 'disabled'
				  );
		
		$customer_link = 'customer_view&email=';
		$this->table->add_row(
			'<a href="'.$url_base.'product_view'.AMP.'id='.$product['entry_id'].'">'.$product['title'].'</a>',
			'<span style="color:#'.m62_status_color($product['status'], $order_channel_statuses).'">'.lang($product['status']).'</span>',
			m62_format_date($product['entry_date'], FALSE, TRUE),
			$product['total_sold'],
			m62_format_money($product['average_paid_price']),
			m62_format_money($product['total_sales'])
			//form_checkbox($toggle)
		);
	}
	
	echo $this->table->generate();
	
?>
<div class="tableFooter">
	<div class="tableSubmit">
		<?php //echo form_dropdown('submit_action', $action_options, FALSE, 'id="submit_action"'); ?>	
		&nbsp;&nbsp;	
		<?php //echo form_dropdown('change_status', $change_statuses, FALSE, 'id="change_status" style="display:none"'); ?>	
		&nbsp;&nbsp;				
		<?php //echo form_submit('submit', lang('delete_selected'), 'class="submit" id="submit"');?>
		&nbsp;&nbsp;			
	</div>

	<span class="js_hide"><?php echo $pagination?></span>	
	<span class="pagination" id="filter_pagination"></span>
</div>	
<?php echo form_close()?>

<?php } else { ?>
<?php echo lang('no_matching_products')?>
<?php } ?>