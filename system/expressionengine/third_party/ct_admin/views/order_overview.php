<?php $this->load->view('overlay/header'); ?>

<h1><?=$order_details['title']; ?></h1>
<?=form_open($query_base.'update_order', array('id'=>'my_accordion'))?>
<?php
echo form_hidden('order_id', $order_id);
$this->table->set_template($cp_table_template);
$this->table->set_empty("&nbsp;");
?>	

<div>
<?php $this->load->view('includes/sub_menu'); ?>

<br clear="all" />		
<?php 
$this->load->view('errors', array('lightbox' => 'yes')); 
?>
<br clear="all" />
<?php 
if(is_array($statuses) && count($statuses) != '0')
{
	$options = array();
	foreach($statuses AS $status)
	{
		$options[$status['status']] = lang($status['status']);
	}
}
$this->table->set_heading(lang('name'), lang('email'), lang('order_date'),lang('status'),lang('subtotal'),lang('total'));
$data = array(
		'<a href="'.$url_base.'customer_view'.AMP.'email='.$order_details['orders_customer_email'].'" target="_parent">'.$order_details['orders_billing_first_name'].' '.$order_details['orders_billing_last_name'].'</a>', 
		'<a href="mailto:'.$order_details['orders_customer_email'].'">'.$order_details['orders_customer_email'].'</a>', 
		m62_format_date($order_details['entry_date'], FALSE, TRUE),
		'<span style="color:#'.m62_status_color($order_details['status'], $order_channel_statuses).'">'.lang($order_details['status']).'</span>',
		m62_format_money($order_details['order_subtotal']),
		m62_format_money($order_details['order_total'])
);
$this->table->add_row($data);
echo $this->table->generate();
$this->table->clear();
?>
</div>

<h3 class="accordion"><?=lang('order_details')?></h3>
<div>
<?php 
$this->table->set_heading('&nbsp;','');
foreach($order_details_view AS $key => $value)
{
	$headers[] = lang($key);
	$rows[] = $value;
	//$this->table->add_row(lang($key), $value);
}
$this->table->set_heading($headers);
$this->table->add_row($rows);

echo $this->table->generate();
// Clear out of the next one
$this->table->clear();
?>
</div>

<?php //echo $hook_view; ?>

<h3 class="accordion "><?=lang('products').' ('.count($order_items).') ';?>
</h3>
<div id="order_products"><?php $this->load->view('orders/items'); ?></div>

<?php //echo $hook_secondary_view; ?>
<?=form_close()?>
<?php 
$this->load->view('overlay/footer'); 
?>