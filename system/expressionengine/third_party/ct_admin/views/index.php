<?php 
$this->load->view('errors'); 
$tmpl = array (
	'table_open'          => '<table class="mainTable" border="0" cellspacing="0" cellpadding="0">',

	'row_start'           => '<tr class="even">',
	'row_end'             => '</tr>',
	'cell_start'          => '<td style="width:50%;">',
	'cell_end'            => '</td>',

	'row_alt_start'       => '<tr class="odd">',
	'row_alt_end'         => '</tr>',
	'cell_alt_start'      => '<td>',
	'cell_alt_end'        => '</td>',

	'table_close'         => '</table>'
);

$this->table->set_template($tmpl); 
$this->table->set_empty("&nbsp;");
?>
<table width="100%">
	<tr>
		<td width="50%">
		<?php 
		$this->table->set_heading(lang('store_overview'),' ');
		$this->table->add_row(lang('total_sales'), m62_format_money($total_sales));
		$this->table->add_row(lang('this_years_sales'), m62_format_money($this_years_sales));
		$this->table->add_row(lang('average_order'), m62_format_money($average_order));
		$this->table->add_row(lang('this_years_orders'), $this_years_orders);
		$this->table->add_row(lang('this_months_orders'), '<a href="'.$url_base.'history_report'.AMP.'month='.date('m').AMP.'year='.date('Y').'">'.$this_months_orders.'</a>');
		$this->table->add_row(lang('todays_orders'), count($todays_orders));		
		$this->table->add_row(lang('total_orders'), '<a href="'.$url_base.'orders">'.m62_format_number($total_successful_orders, FALSE).' ('.m62_format_number($total_orders, FALSE).')</a>');
		$this->table->add_row(lang('total_customers'), '<a href="'.$url_base.'customers">'.$total_customers.'</a>');
		
		echo $this->table->generate();
		// Clear out of the next one
		$this->table->clear();		
		?>
		</td>
		<td valign="top">
		<?php 
		$this->table->set_heading(lang('recent_sales'));
		if(count($chart_order_history) >= '1')
		{
			$this->table->add_row('<div id="chart_div"></div>');
		}
		else
		{
			$this->table->add_row(lang('no_data_to_plot'));
		}
		echo $this->table->generate();
		$this->table->clear();		
		?>		
		</td>
	</tr>
</table>
<?php 		
if(count($chart_order_history) >= '1'):
?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages:["corechart"]});
	google.setOnLoadCallback(drawChart);
    
	function drawChart() {
		var data = new google.visualization.DataTable();
		data.addColumn('string', 'Year');
		data.addColumn('number', 'Totals');
		data.addColumn('number', 'Subtotals');
		data.addRows(<?php echo count($chart_order_history);?>);
		
		<?php 
		$i = 0;
		foreach($chart_order_history AS $date)
		{ 
			echo "data.setValue($i, 0, '".m62_convert_timestamp(strtotime($date['order_date']), $settings['graph_date_format'])."');";
			echo "data.setValue($i, 1, ".$date['total'].");";
			echo "data.setValue($i, 2, ".$date['subtotal'].");";
			$i++;
		}
		?>
		
		var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		var chart_width = document.getElementById("chart_div").offsetWidth+10;
		var area_width = chart_width+20;
		var formatter = new google.visualization.NumberFormat({prefix: '<?php echo $number_prefix; ?>', negativeColor: 'red', negativeParens: true});
		
		formatter.format(data, 1);
		formatter.format(data, 2);	
		        
		chart.draw(data, {
			width: chart_width, 
			height: 208, 
			legend:'in', 
			hAxis: {slantedText: true},
			backgroundColor: 'none',
			chartArea: {
				width: area_width, 
				height: "160",
				top: 10,
				left:30
			}           
		});
	}

</script>
<?php 
else:

	echo lang('no_matching_orders');

endif; 

?>

<div id="my_accordion">

	<?php echo $hook_view; ?>
	
	<h3 class="accordion"><?=lang('latest_orders')?></h3>
	<div id="todays_orders">
	
	<?php 
	if(count($todays_orders) > 0)
	{
	
		echo form_open($query_base.'delete_order_confirm'); 
		
		$this->table->set_template($cp_pad_table_template);
		$this->table->set_heading(
			lang('order_id').'/'.lang('edit'),
			lang('customer_name'),
			lang('status'),
			lang('order_date'),
			lang('total'),
			''
		);
	
		foreach($todays_orders as $order)
		{
			$toggle = array(
				'name'		=> 'toggle[]',
				'id'		=> 'edit_box_'.$order['entry_id'],
				'value'		=> $order['entry_id'],
				'class'		=>'toggle'
			);
			
			$packingslip_url = m62_get_invoice_url($order['entry_id'], TRUE);
			$invoice_url = m62_get_invoice_url($order['entry_id']);
			$customer_link = 'customer_view&email=';
			$this->table->add_row(
				'<a href="'.$url_base.'order_view'.AMP.'id='.$order['entry_id'].AMP.'lightbox=yes" class="fancybox_overlay"><img style="padding-right:5px; cursor:pointer;" rel="'.$order['entry_id'].'"  class="" src="'.$theme_folder_url.'/cp_themes/default/images/button_close_subadmin.gif" /></a> <a href="'.$url_base.'order_view'.AMP.'id='.$order['entry_id'].'">'.$order['title'].'</a>',
				'<a href="'.$url_base.'customer_view'.AMP.'email='.$order['email'].'">'.$order['first_name'].' '.$order['last_name'].'</a>',
				'<span style="color:#'.m62_status_color($order['status'], $order_channel_statuses).'; cursor:pointer;" class="order_status_td" id="order_status_td_'.$order['entry_id'].'" rel="'.$order['entry_id'].'">'.lang($order['status']).'</span>'.form_dropdown('status', $statuses, $order['status'], 'id="change_status_'.$order['entry_id'].'" rel="'.$order['entry_id'].'" class="aj_order_status_change" style="display:none;"'),
				m62_format_date($order['entry_date'], FALSE, TRUE),
				$number_format_defaults_prefix.$order['order_total'],
				'<a class="nav_button" href="'.$packingslip_url.'" target="_blank" title="'.lang('packing_slip').'"><img src="'.$theme_folder_url.'/third_party/ct_admin/images/packingslip.png" alt="'.lang('packing_slip').'" /></a>'.
				'<a class="nav_button" href="'.$invoice_url.'" target="_blank" title="'.lang('invoice').'"><img src="'.$theme_folder_url.'/third_party/ct_admin/images/invoice.png" alt="'.lang('invoice').'" /></a>'
			);
		}
		
		echo $this->table->generate();
		echo '</form>';
	} else { 
		echo lang('no_matching_orders'); 
	} 
	?>
	</div>
	
	<?php echo $hook_secondary_view; ?>
</div>
<div id="ct_admin_preview_modal">
	<div id="ct_admin_preview_modal_content"></div>
</div>