<h3  class="accordion"><?=lang('configure')?></h3>
<div>
	<?php
	$this->table->set_heading(lang('settings'),' ');
	$this->table->add_row('<label for="success_statuses">'.lang('success_statuses').'</label><div class="subtext">'.lang('success_statuses_instructions').'</div>', form_multiselect('success_statuses[]', $success_statuses, $settings['success_statuses'], 'id="success_statuses"'. $settings_disable));
	$this->table->add_row('<label for="packingslip_template_url">'.lang('packingslip_template_url').'</label><div class="subtext">'.lang('packingslip_template_url_instructions').'</div>', form_dropdown('packingslip_template_url', $template_options, $settings['packingslip_template_url'], 'id="packingslip_template_url"' . $settings_disable));
	$this->table->add_row('<label for="invoice_template_url">'.lang('invoice_template_url').'</label><div class="subtext">'.lang('invoice_template_url_instructions').'</div>', form_dropdown('invoice_template_url', $template_options, $settings['invoice_template_url'], 'id="invoice_template_url"' . $settings_disable));
	$this->table->add_row('<label for="require_valid_coupon">'.lang('require_valid_coupon').'</label><div class="subtext">'.lang('require_valid_coupon_instructions').'</div>', form_checkbox('require_valid_coupon', '1', $settings['require_valid_coupon'], 'id="require_valid_coupon"' . $settings_disable));
	$this->table->add_row('<label for="store_email_address">'.lang('store_email_address').'</label><div class="subtext">'.lang('store_email_address_instructions').'</div>', form_input('store_email_address', $settings['store_email_address'], 'id="store_email_address" autocomplete="no" ' . $settings_disable));
	
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>

<h3  class="accordion"><?=lang('license_number')?></h3>
<div>
	<?php
	$this->table->set_heading(lang('settings'),' ');
	$this->table->add_row('<label for="license_number">'.lang('license_number').'</label>', form_input('license_number', $settings['license_number'], 'id="license_number"'. $settings_disable));
	
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>
<input type="hidden" value="<?php echo $settings['inventory_report_enable']; ?>" name="inventory_report_enable" />
<input type="hidden" value="<?php echo $settings['relative_time']; ?>" name="relative_time" />
<input type="hidden" value="<?php echo $settings['override_cp_order_edit_routing']; ?>" name="override_cp_order_edit_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_product_routing']; ?>" name="override_cp_product_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_dashboard']; ?>" name="override_cp_dashboard" />