<h3  class="accordion"><?=lang('reporting_options')?></h3>
<div>
	<?php
	$this->table->set_heading(lang('settings'),' ');
	$this->table->add_row('<label for="inventory_report_enable">'.lang('inventory_report_enable').'</label><div class="subtext">'.lang('inventory_report_enable_instructions').'</div>', form_checkbox('inventory_report_enable', '1', $settings['inventory_report_enable'], 'id="inventory_report_enable"' . $settings_disable));
	$this->table->add_row('<label for="country_report_data">'.lang('country_report_data').'</label><div class="subtext">'.lang('country_report_data_instructions').'</div>', form_dropdown('country_report_data', $country_report_data_options, $settings['country_report_data'], 'id="country_report_data"' . $settings_disable));
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>
<input type="hidden" value="<?php echo $settings['relative_time']; ?>" name="relative_time" />
<input type="hidden" value="<?php echo $settings['require_valid_coupon']; ?>" name="require_valid_coupon" />
<input type="hidden" value="<?php echo $settings['override_cp_order_edit_routing']; ?>" name="override_cp_order_edit_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_product_routing']; ?>" name="override_cp_product_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_dashboard']; ?>" name="override_cp_dashboard" />