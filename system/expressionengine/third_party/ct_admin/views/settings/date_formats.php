<h3  class="accordion"><?=lang('date_formats')?></h3>
<div>
	<?php
	$this->table->set_heading(lang('settings'),' ');
	$this->table->add_row('<label for="graph_date_format">'.lang('graph_date_format').'</label><div class="subtext">'.lang('graph_date_format_instructions').'</div>', form_input('graph_date_format', $settings['graph_date_format'], 'id="graph_date_format"'. $settings_disable));
	$this->table->add_row('<label for="export_date_format">'.lang('export_date_format').'</label><div class="subtext">'.lang('export_date_format_instructions').'</div>', form_input('export_date_format', $settings['export_date_format'], 'id="export_date_format"'. $settings_disable));
	$this->table->add_row('<label for="ct_date_format">'.lang('ct_date_format').'</label><div class="subtext">'.lang('ct_date_format_instructions').'</div>', form_input('ct_date_format', $settings['ct_date_format'], 'id="ct_date_format"'. $settings_disable));
	$this->table->add_row('<label for="relative_time">'.lang('relative_time').'</label><div class="subtext">'.lang('relative_time_instructions').'</div>', form_checkbox('relative_time', '1', $settings['relative_time'], 'id="relative_time"'. $settings_disable));

	echo $this->table->generate();
	$this->table->clear();
	?>
</div>
<input type="hidden" value="<?php echo $settings['inventory_report_enable']; ?>" name="inventory_report_enable" />
<input type="hidden" value="<?php echo $settings['require_valid_coupon']; ?>" name="require_valid_coupon" />
<input type="hidden" value="<?php echo $settings['override_cp_order_edit_routing']; ?>" name="override_cp_order_edit_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_product_routing']; ?>" name="override_cp_product_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_dashboard']; ?>" name="override_cp_dashboard" />