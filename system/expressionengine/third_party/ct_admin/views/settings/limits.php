<h3 class="accordion"><?=lang('limits')?></h3>
<div>
	<?php 
	$this->table->set_heading(lang('settings'),' ');
	$this->table->add_row('<label for="orders_list_limit">'.lang('orders_list_limit').'</label><div class="subtext">'.lang('orders_list_limit_instructions').'</div>', form_input('orders_list_limit', $settings['orders_list_limit'], 'id="orders_list_limit"' . $settings_disable));
	$this->table->add_row('<label for="cust_list_limit">'.lang('cust_list_limit').'</label><div class="subtext">'.lang('cust_list_limit_instructions').'</div>', form_input('cust_list_limit', $settings['cust_list_limit'], 'id="cust_list_limit"' . $settings_disable));
	$this->table->add_row('<label for="products_list_limit">'.lang('products_list_limit').'</label><div class="subtext">'.lang('products_list_limit_instructions').'</div>', form_input('products_list_limit', $settings['products_list_limit'], 'id="products_list_limit"'. $settings_disable));
	$this->table->add_row('<label for="order_graph_limit">'.lang('order_graph_limit').'</label><div class="subtext">'.lang('order_graph_limit_instructions').'</div>', form_input('order_graph_limit', $settings['order_graph_limit'], 'id="order_graph_limit"'. $settings_disable));
	$this->table->add_row('<label for="latest_orders_limit">'.lang('latest_orders_limit').'</label><div class="subtext">'.lang('latest_orders_limit_instructions').'</div>', form_input('latest_orders_limit', $settings['latest_orders_limit'], 'id="latest_orders_limit"'. $settings_disable));
	
	echo $this->table->generate();
	$this->table->clear();	
	?>
</div>
<input type="hidden" value="<?php echo $settings['inventory_report_enable']; ?>" name="inventory_report_enable" />
<input type="hidden" value="<?php echo $settings['relative_time']; ?>" name="relative_time" />
<input type="hidden" value="<?php echo $settings['override_cp_order_edit_routing']; ?>" name="override_cp_order_edit_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_product_routing']; ?>" name="override_cp_product_routing" />
<input type="hidden" value="<?php echo $settings['override_cp_dashboard']; ?>" name="override_cp_dashboard" />