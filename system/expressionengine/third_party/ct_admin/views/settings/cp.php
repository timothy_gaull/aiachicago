<h3  class="accordion"><?=lang('configure')?></h3>
<div>
	<?php
	$this->table->set_heading(lang('settings'),' ');
	$this->table->add_row('<label for="override_cp_order_edit_routing">'.lang('override_cp_order_edit_routing').'</label><div class="subtext">'.lang('override_cp_order_edit_routing_instructions').'</div>', form_checkbox('override_cp_order_edit_routing', '1', $settings['override_cp_order_edit_routing'], 'id="override_cp_order_edit_routing"' . $settings_disable));
	$this->table->add_row('<label for="override_cp_product_routing">'.lang('override_cp_product_edit_routing').'</label><div class="subtext">'.lang('override_cp_product_edit_routing_instructions').'</div>', form_checkbox('override_cp_product_routing', '1', $settings['override_cp_product_routing'], 'id="override_cp_product_routing"' . $settings_disable));
	$this->table->add_row('<label for="override_cp_dashboard">'.lang('override_cp_dashboard').'</label><div class="subtext">'.lang('override_cp_dashboard_instructions').'</div>', form_checkbox('override_cp_dashboard', '1', $settings['override_cp_dashboard'], 'id="override_cp_dashboard"' . $settings_disable));
	$this->table->add_row('<label for="allowed_access_levels">'.lang('allowed_access_levels').'</label><div class="subtext">'.lang('allowed_access_levels_instructions').'</div>', form_multiselect('allowed_access_levels[]', $member_groups, $settings['allowed_access_levels'], $settings_disable));
	echo $this->table->generate();
	$this->table->clear();
	?>
</div>
<input type="hidden" value="<?php echo $settings['inventory_report_enable']; ?>" name="inventory_report_enable" />
<input type="hidden" value="<?php echo $settings['require_valid_coupon']; ?>" name="require_valid_coupon" />
<input type="hidden" value="<?php echo $settings['relative_time']; ?>" name="relative_time" />