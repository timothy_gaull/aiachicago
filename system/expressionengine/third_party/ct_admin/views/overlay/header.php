<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">

	<?=$this->view->head_title($cp_page_title)?>
	<?=$this->view->head_link('css/jquery-ui-1.8.16.custom.css'); ?>
	<?=$this->view->head_link('css/global.css'); ?>
	<?=$this->view->head_link('css/override.css'); ?>

	<?php if ($this->extensions->active_hook('cp_css_end') === TRUE):?>
	<link rel="stylesheet" href="<?=BASE.AMP.'C=css'.AMP.'M=cp_global_ext';?>" type="text/css" />
	<?php endif;?>
	<!--[if lte IE 7]><?=$this->view->head_link('css/iefix.css')?><![endif]-->

	<?php 
	if (isset($cp_global_js))
	{
		echo $cp_global_js;
	} ?>
	
	<?=$this->view->script_tag('jquery/jquery.js')?>
	<?php //$this->view->script_tag('jquery/plugins/corner.js')?>

	<?php if (isset($advanced_css_mtime)): ?>
	<script charset="utf-8" type="text/javascript" src="<?=BASE.AMP.'C=javascript'.AMP.'M=load'.AMP.'file=css'.AMP.'theme='.$this->cp->cp_theme.AMP.'v='.$advanced_css_mtime?>"></script>
	<?php endif;?>

	<?php
	if (isset($script_head)) 
	{
		echo $script_head;
	}

	foreach ($this->cp->its_all_in_your_head as $item)
	{
		echo $item."\n";
	}
	?>


<style>

html {
    background-color: #FFFFFF;
}
.mainTable
{
	width:100%;
	padding-right:10px;
}

.mainTable th
{
	text-align:left;
	line-height: 13px;
	font-size:12px;
}

h3.accordion
{
	padding-top:10px;
	padding-bottom:5px;
}
</style>
</head>
<body>