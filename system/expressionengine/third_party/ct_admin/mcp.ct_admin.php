<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

 /**
 * mithra62 - CT Admin
 *
 * @package		mithra62:Ct_admin
 * @author		Eric Lamb
 * @copyright	Copyright (c) 2011, mithra62, Eric Lamb.
 * @link		http://mithra62.com/projects/view/ct-admin/
 * @updated		2.0
 * @filesource 	./system/expressionengine/third_party/ct_admin/
 */
 
 /**
 * CT Admin - CP Class
 *
 * Control Panel class
 *
 * @package 	mithra62:Ct_admin
 * @author		Eric Lamb
 * @filesource 	./system/expressionengine/third_party/ct_admin/mcp.ct_admin.php
 */
class Ct_admin_mcp 
{
	/**
	 * The URL to the module
	 * @var string
	 */
	public $url_base = '';
	
	/**
	 * The amount of pagination items per page
	 * @var int
	 */
	public $perpage = 10;
	
	/**
	 * The delimiter for the datatables jquery
	 * @var stirng
	 */
	public $pipe_length = 1;
	
	/**
	 * The name of the module; used for links and whatnots
	 * @var string
	 */
	private $mod_name = 'ct_admin';
	
	/**
	 * The amount of items remaining in inventory to trigger inclusion within a report
	 * @var int
	 */
	public $inventory_report_threshold = 20;
	
	/**
	 * The post statuses to ignore within calculations
	 * @var array
	 */
	public $ignore_statuses = array('closed');	
	
	/**
	 * The breadcrumb override 
	 * @var array
	 */
	protected static $_breadcrumbs = array();
	
	/**
	 * Set everything up
	 */
	public function __construct()
	{
		$this->EE =& get_instance();
		
		//load EE stuff
		$this->EE->load->library('javascript');
		$this->EE->load->library('table');
		$this->EE->load->helper('form');

		//load cartthrob stuff
		$this->EE->load->add_package_path(PATH_THIRD.'cartthrob/');  
		$this->EE->load->library('cartthrob_loader');		
		$this->EE->load->library('reports', 'reports');
		$this->EE->cartthrob_loader->setup($this);		
		$this->EE->load->model('order_model', 'order_model');
		
		//make sure cartthrob is ready and set up
		if($this->EE->cartthrob->store->config('orders_total_field') == '' 
		   || $this->EE->cartthrob->store->config('orders_customer_email') == ''
		   || $this->EE->cartthrob->store->config('orders_billing_first_name') == ''
		   || $this->EE->cartthrob->store->config('orders_billing_last_name') == ''
		   )
		{
			show_error($this->EE->lang->line('cartthrob_not_setup'));
		}

		//load our stuff
		$this->EE->load->model('ct_admin_settings_model', 'ct_admin_settings', TRUE);
		$this->EE->load->library('ct_admin_js');
		$this->EE->load->library('ct_admin_lib');
		$this->EE->load->library('ct_admin_orders');
		$this->EE->load->library('ct_admin_reports'); 
		$this->EE->load->library('ct_admin_customers');	
		$this->EE->load->library('ct_admin_products');
		$this->EE->load->library('ct_admin_carts');
		$this->EE->load->helper('utilities');

		//assign things where they need to be assigned
		$this->settings = $this->EE->ct_admin_settings->get_settings();
		$this->query_base = 'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module='.$this->mod_name.AMP.'method=';
		$this->url_base = BASE.AMP.$this->query_base;
		$this->EE->ct_admin_lib->set_url_base($this->url_base);

		$this->EE->cp->set_right_nav($this->EE->ct_admin_lib->get_right_menu());	
		
		$this->number_format_defaults_prefix = $this->EE->cartthrob->store->config('number_format_defaults_prefix');
		
		$this->errors = $this->EE->ct_admin_lib->error_check();
		$this->order_channel_statuses = $this->EE->ct_admin_orders->get_channel_statuses();
		$this->product_channel_statuses = $this->EE->ct_admin_products->get_channel_statuses();
		
		//now the global filtering SQL query
		$this->order_ignore_sql = FALSE;
		$this->success_statuses = array();
		if(count($this->settings['success_statuses']) >= '1')
		{
			$temp = array();
			foreach($this->order_channel_statuses AS $key => $status)
			{
				if(in_array($status['status_id'], $this->settings['success_statuses']))
				{
					$temp[] = "'".$status['status']."'";
					$this->success_statuses[] = $status['status'];
				}
			}
					
			if(count($temp) >= '1')
			{
				$this->order_ignore_sql = " AND status IN(".implode(',', $temp).")";
			}
		}
		
		//handle the accordions for the CP view
		$ignore_methods = array('orders', 'customers', 'products');
		$method = $this->EE->input->get('method', TRUE);
		if($this->settings['disable_accordions'] === FALSE && !in_array($method, $ignore_methods))
		{
			$this->EE->javascript->output($this->EE->ct_admin_js->get_accordian_css());
		}
		
		if($this->settings['disable_accordions'])
		{
			$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'.$this->EE->config->config['theme_folder_url'].'/third_party/ct_admin/css/accordion.css" />');
		}

		//now the theme goodies
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'.$this->EE->config->config['theme_folder_url'].'/third_party/ct_admin/css/ct_admin.css" />');
		$this->EE->cp->add_to_head('<link type="text/css" rel="stylesheet" href="'.$this->EE->config->config['theme_folder_url'].'/third_party/cartthrob/css/cartthrob.css" />');
		$this->EE->cp->add_to_head('<script type="text/javascript" src="'.$this->EE->config->config['theme_folder_url'].'third_party/ct_admin/js/jquery.simplemodal.js"></script>');	
	
		//and lastly assign the view vars and do some cleanup
		$this->add_breadcrumb($this->url_base.'index', lang('ct_admin_module_name'));
		$this->EE->load->vars(
			array(
				'url_base' => $this->url_base,
				'query_base' => $this->query_base,
				'store_email' => $this->settings['store_email_address'],
				'store_email' => $this->settings['store_email_address'],
				'order_channel_statuses' => $this->order_channel_statuses,
				'number_format_defaults_prefix' => $this->number_format_defaults_prefix,
				'errors' => $this->errors,
				'settings' => $this->settings,
				'theme_folder_url' => $this->EE->config->item('theme_folder_url'),
				'order_channel_statuses' => $this->order_channel_statuses,
				'number_prefix' => $this->EE->cartthrob->store->config('number_format_defaults_prefix'),
				'disable_accordions' => $this->settings['disable_accordions']
			)
		);		
	}
	
	/**
	 * Creates the breadcrumb for CP navigation
	 * @param string $link
	 * @param string $title
	 */
	private function add_breadcrumb($link, $title)
	{
		$this->EE->cp->set_breadcrumb($link, $title);
	}	
	
	public function order_admin()
	{
		$this->EE->functions->redirect(BASE.AMP.'C=addons_modules'.AMP.'M=show_module_cp'.AMP.'module=ct_admin'.AMP.'method=index');
		exit;		
	}
	
	/**
	 * CP Dashboard
	 */
	public function index()
	{
		$this->EE->cp->add_js_script('ui', 'accordion'); 
		$this->EE->jquery->tablesorter('#todays_orders table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[0,1]]}');  
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_modal());
		$this->EE->javascript->output($this->EE->ct_admin_js->ajax_order_status());		
		$this->EE->javascript->compile(); 
		
		$vars = array();
		$max = time()-(86400*$this->settings['latest_orders_limit']);
		$vars['todays_orders'] = $this->EE->ct_admin_orders->get_orders($this->settings['orders_list_limit'], 0, 'entry_date DESC', " AND entry_date > '".$max."' ".$this->order_ignore_sql);
		//$vars['todays_orders'] = $this->EE->ct_admin_orders->get_orders($this->settings['orders_list_limit'], 0, 'entry_date DESC', " AND year = '".date('Y')."' AND month = '07' AND day = '09' ".$this->order_ignore_sql);
		$vars['total_sales'] = $this->EE->ct_admin_orders->get_total_sales($this->order_ignore_sql);
		$vars['average_order'] = $this->EE->ct_admin_orders->get_average_order($this->order_ignore_sql);
		$vars['this_years_sales'] = $this->EE->ct_admin_orders->get_total_sales(" AND year = '".date('Y')."' ".$this->order_ignore_sql);
		$vars['total_successful_orders'] = $this->EE->ct_admin_orders->get_total_orders($this->order_ignore_sql);
		$vars['total_orders'] = $this->EE->ct_admin_orders->get_total_orders();
		$vars['this_years_orders'] = $this->EE->ct_admin_orders->get_total_orders(" AND year = '".date('Y')."'".$this->order_ignore_sql);
		$vars['this_months_orders'] = $this->EE->ct_admin_orders->get_total_orders(" AND year = '".date('Y')."' AND month = '".date('m')."' ".$this->order_ignore_sql);
		$vars['total_customers'] = $this->EE->ct_admin_customers->get_total_customers();
		$vars['chart_order_history'] = $this->EE->ct_admin_reports->get_chart_order_history($this->settings['order_graph_limit'], $this->order_ignore_sql);
		$vars['todays_customers'] = 0;
		$vars['settings'] = $this->settings;
		
		//setup the ajax status change 
		$order_channel_statuses = $this->EE->ct_admin_orders->get_channel_statuses();
		$statuses = array();
		foreacH($order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = $status['status'];
		}
		$vars['statuses'] = $statuses;		
		
		//$vars['order_details_view'] = $this->EE->ct_admin_reports->setup_dashboard_details_view($vars['order_details']);
		$vars['hook_view'] = $this->EE->ct_admin_reports->setup_dashboard_hook_view();
		$vars['hook_secondary_view'] = $this->EE->ct_admin_reports->setup_dashboard_hook_secondary_view();

		$this->EE->view->cp_page_title = $this->EE->lang->line('dashboard');
		return $this->EE->load->view('index', $vars, TRUE);
	}

	/**
	 * Order List Page
	 */
	public function orders()
	{		
		$vars = array();
		$total = $this->EE->ct_admin_orders->get_total_orders();
		$vars['orders'] = $this->EE->ct_admin_orders->get_orders($this->settings['orders_list_limit'], 0);
		if ( ! $rownum = $this->EE->input->get_post('rownum'))
		{		
			$rownum = 0;
		}		
		
		$dt = $this->EE->ct_admin_js->get_orders_datatables('edit_orders_ajax_filter', 6, 1, $this->settings['orders_list_limit'], '"aaSorting": [[ 3, "desc" ]],');
		$this->EE->javascript->output($dt);
		$this->EE->javascript->output($this->EE->ct_admin_js->get_check_toggle());
		
		$this->EE->cp->add_js_script(array('plugin' => array('dataTables'),'ui' => 'datepicker,slider'));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_modal());
		$this->EE->javascript->output($this->EE->ct_admin_js->ajax_order_status());
		$this->EE->javascript->compile();
		$this->EE->load->library('pagination');

		$vars['pagination'] = $this->EE->ct_admin_lib->create_pagination('edit_orders_ajax_filter', $total, $this->settings['orders_list_limit']);
				
		$vars['total_orders'] = $total;
		$statuses = array();
		$i = 0;
		foreach($this->order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = lang($status['status']);
		}
		
		asort($statuses);
		$prefix = array(''=>lang('filter_by_status'));
		$vars['change_statuses'] = $statuses;
		$statuses = array_merge($prefix, $statuses);
		$vars['success_statuses'] = $statuses;
		$vars['status_selected'] = '';
		$vars['date_selected'] = '';
		$vars['order_keywords'] = '';
		
		$first_date = $this->EE->ct_admin_orders->get_first_date();
		if(isset($first_date['day']))
		{
			$vars['default_start_date'] = $first_date['year'].'-'.$first_date['month'].'-'.$first_date['day'];
		}
		else
		{
			$vars['default_start_date'] = date('Y-m-d');
		}		

		$vars['action_options'] = $this->EE->ct_admin_lib->get_action_options();
		$vars['perpage_select_options'] = $this->EE->ct_admin_lib->perpage_select_options();
		$vars['date_select_options'] = $this->EE->ct_admin_lib->date_select_options();		
		$vars['minmax_orders'] = $this->EE->ct_admin_orders->get_minmax_orders();
		$vars['shippable_options'] = $this->EE->ct_admin_lib->get_shippable_options();

		$this->EE->view->cp_page_title = $this->EE->lang->line('orders');
		return $this->EE->load->view('orders', $vars, TRUE); 
	}
	
	/**
	 * AJAX endpoint for Order Datatable
	 */
	public function edit_orders_ajax_filter()
	{
		die($this->EE->ct_admin_orders->json_ordering($this->perpage, $this->url_base));
	}	
	
	/**
	 * Order View Page
	 */
	public function order_view()
	{
		$vars = array();
		$order_id = $this->EE->input->get('id');
		$lightbox = $this->EE->input->get('lightbox');
		$print_invoice = $this->EE->input->get('print_invoice');
		$packing_slip = $this->EE->input->get('packing_slip');
		if(!$order_id)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_found'));
			$this->EE->functions->redirect($this->url_base.'orders');	
			exit;
		}
		
		$vars['lightbox'] = $lightbox;
		$vars['statuses'] = $this->EE->ct_admin_orders->get_channel_statuses();
		$product_channel = $this->EE->cartthrob->store->config('product_channels');
		if($print_invoice)
		{
			$vars['site_name'] = $this->EE->config->config['site_name'];
			$vars['webmaster_email'] = $this->EE->config->config['webmaster_email'];
			$vars['site_url'] = $this->EE->config->config['site_url'];
			$vars['packing_slip'] = $packing_slip;
			
			$order_ids = explode('|', $order_id);
			foreach($order_ids AS $order_id)
			{
				$vars['order_details'][$order_id] = $this->EE->ct_admin_orders->get_order($order_id);
				$vars['order_details'][$order_id]['order_items'] = $this->EE->ct_admin_orders->get_order_items($order_id);
			}
			
			//now check for the custom template
			$custom_path = dirname(__FILE__).'/views/print_invoice_custom.php';
			if(file_exists($custom_path))
			{
				die($this->EE->load->view('print_invoice_custom', $vars, TRUE));
			}
				
			die($this->EE->load->view('print_invoice', $vars, TRUE));
		}		
		
		$vars['order_details'] = $this->EE->ct_admin_orders->get_order($order_id);
		if(!$vars['order_details'] || count($vars['order_details']) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_found'));
			$this->EE->functions->redirect($this->url_base.'orders');	
			exit;			
		}
		
		$vars['order_items'] = $this->EE->ct_admin_orders->get_order_items($order_id);
		$vars['order_id'] = $order_id;
		if(isset($vars['order_details']['order_customer_email']))
		{
			$vars['ee_member_id'] = $this->EE->ct_admin_customers->get_ee_member_id($vars['order_details']['order_customer_email']);
		}	
		
		$this->EE->cp->add_js_script('ui', 'accordion'); 
		//$this->EE->jquery->tablesorter('#order_products table', '{headers: {4: {sorter: false}}, widgets: ["zebra"], sortList: [[0,1]]}');  
		
		$this->EE->javascript->compile(); 

		$vars['order_details_view'] = $this->EE->ct_admin_orders->setup_order_details_view($vars['order_details']);
		$urls = $this->EE->ct_admin_orders->get_order_view_menu($order_id, $vars['order_details']);
		$vars['menu_data'] = $urls;
				
		if($lightbox == 'yes')
		{
			echo  $this->EE->load->view('order_overview', $vars, TRUE);
			exit;
		}		
		
		$vars['order_details_view'] = $this->EE->ct_admin_orders->setup_order_details_view($vars['order_details']);
		$vars['hook_view'] = $this->EE->ct_admin_orders->setup_order_hook_view($vars['order_details']);
		$vars['hook_secondary_view'] = $this->EE->ct_admin_orders->setup_order_hook_secondary_view($vars['order_details']);
		$vars['hook_tertiary_view'] = $this->EE->ct_admin_orders->setup_order_hook_tertiary_view($vars['order_details']);

		$this->add_breadcrumb($this->url_base.'orders', lang('orders'));
		$this->EE->view->cp_page_title = $vars['order_details']['title'];
		return $this->EE->load->view('order_view', $vars, TRUE); 				
	}
	
	/**
	 * Update Order Action
	 */
	public function update_order()
	{
		$order_id = $this->EE->input->post('order_id');
		if(!$order_id)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_updated'));
			$this->EE->functions->redirect($this->url_base.'index');	
			exit;			
		}
		
		$data = $this->EE->ct_admin_orders->get_order($order_id);
		if(!$data)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_updated'));
			$this->EE->functions->redirect($this->url_base.'index');	
			exit;			
		}

		if($this->EE->ct_admin_orders->update_order($order_id, $_POST))
		{
			$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('order_updated'));
			if(isset($_POST['return']))
			{
				$this->EE->functions->redirect($this->url_base.'index');
			}
			else
			{
				$this->EE->functions->redirect($this->url_base.'order_view&id='.$order_id);
			}	
			exit;				
		}
		else
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_updated'));
			$this->EE->functions->redirect($this->url_base.'index');	
			exit;				
		}
	}
	
	/**
	 * Confirmation page for actions on an order
	 */
	public function action_order_confirm()
	{
		$order_ids = $this->EE->input->get_post('toggle', TRUE);
		$submit_action = $this->EE->input->get_post('submit_action', FALSE);
		$change_status = $this->EE->input->get_post('change_status', FALSE);
		
		if(!$order_ids || count($order_ids) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_found'));
			$this->EE->functions->redirect($this->url_base.'orders');	
			exit;			
		}
		
		$order_data = array();
		$ids = array();
		foreach($order_ids AS $id)
		{
			$data = $this->EE->ct_admin_orders->get_order($id);
			if(is_array($data) && count($data) != '0')
			{
				$order_data[] = array('id' => $data['entry_id'], 'title' => $data['title']);
				$ids[] = $data['entry_id'];
			}
		}
		

		$this->add_breadcrumb($this->url_base.'orders', lang('orders'));
		$vars = array();
		$download_delete_question = $cp_page_title = $change_status_question = FALSE;
		switch($submit_action)
		{
			case 'delete':
				$cp_page_title = $this->EE->lang->line('delete_order_confirm');
				$download_delete_question = $this->EE->lang->line('delete_order_confirm');
								
				$vars['form_action'] = $this->query_base.'delete_orders';
				$view = 'delete_confirm';
			break;
			
			case 'change_status':
				
				$question = str_replace('#status#', $change_status, $this->EE->lang->line('change_status_question'));
				$cp_page_title = $question;
				$change_status_question = $question;
				
				$statuses = array();
				$i = 0;
				foreach($this->order_channel_statuses AS $status)
				{
					$statuses[$status['status']] = lang($status['status']);
				}				
				
				$vars['statuses'] = $statuses;
				$vars['status_selected'] = $change_status;
				$vars['form_action'] = $this->query_base.'change_orders_status';			
				$view = 'change_orders_status_confirm';	
							
			break;
			
			case 'print_invoices':
			case 'packing_slip':
			
				$packingslip = ($submit_action == 'packing_slip' ? TRUE : FALSE);
				$vars['view_action_url'] = $this->EE->ct_admin_lib->get_invoice_url(implode('|', $ids), $packingslip);
				
				$cp_page_title = $this->EE->lang->line('action_printing_order_label');
				
				$statuses = array();
				$i = 0;
				foreach($this->order_channel_statuses AS $status)
				{
					$statuses[$status['status']] = lang($status['status']);
				}
				
				$vars['statuses'] = $statuses;
				$vars['status_selected'] = $change_status;
				$vars['form_action'] = $this->query_base.'change_orders_status';
								
				$view = 'print_confirm';
			
			break;			
		}
		
		$vars['damned'] = $ids;
		$vars['data'] = $order_data;
		$vars['submit_action'] = $submit_action;
		$this->EE->load->vars(
			array(
				'download_delete_question' => $download_delete_question,
				'change_status_question' => $change_status_question
			)
		);

		$this->EE->view->cp_page_title = $cp_page_title;
		return $this->EE->load->view($view, $vars, TRUE);
	}
	
	/**
	 * Change Order Status Action
	 */
	public function change_orders_status()
	{
		$order_ids = $this->EE->input->get_post('orders', FALSE);
		$status = $this->EE->input->get_post('status', FALSE);
		if(!$order_ids || count($order_ids) == 0 || !$status)
		{
			if(AJAX_REQUEST)
			{
				$this->EE->output->send_ajax_response(array('failure'), TRUE);
			}
			else
			{			
				$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_found'));
				$this->EE->functions->redirect($this->url_base.'orders');
				exit;
			}
		}		
		
		if($this->EE->ct_admin_orders->upate_status($order_ids, $status))
		{
			if(AJAX_REQUEST)
			{
				$this->EE->output->send_ajax_response(array('success'), FALSE);
			}
			else
			{			
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('orders_status_updated'));
				$this->EE->functions->redirect($this->url_base.'orders');
				exit;
			}
		}
		
		if(AJAX_REQUEST)
		{
			$this->EE->output->send_ajax_response(array('failure'), TRUE);
		}
		else
		{		
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('orders_status_updated_failure'));
			$this->EE->functions->redirect($this->url_base.'index');
			exit;	
		}	
	}
	
	/**
	 * Delete Orders Action
	 */
	public function delete_orders()
	{
		$order_ids = $this->EE->input->get_post('delete', FALSE);
		if(!$order_ids || count($order_ids) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('order_not_found'));
			$this->EE->functions->redirect($this->url_base.'orders');
			exit;
		}
				
		if($this->EE->ct_admin_orders->delete_orders($order_ids))
		{
			$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('orders_deleted'));
			$this->EE->functions->redirect($this->url_base.'orders');	
			exit;			
		}	
		$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('orders_delete_failure'));
		$this->EE->functions->redirect($this->url_base.'index');
		exit;	
	}
	
	/**
	 * Customers List Page
	 */
	public function customers()
	{		
		$vars = array();
		$total = $this->EE->ct_admin_customers->get_total_customers();
		$vars['customer_data'] = $this->EE->ct_admin_customers->get_customers($this->settings['cust_list_limit'], 0, 'last_order DESC'); 
		if ( ! $rownum = $this->EE->input->get_post('rownum'))
		{		
			$rownum = 0;
		}
		
		$this->EE->cp->add_js_script(array('plugin' => 'dataTables','ui' => 'datepicker,slider'));
		$dt = $this->EE->ct_admin_js->get_customers_datatables('edit_customers_ajax_filter', 5, 1, $this->settings['cust_list_limit'], '"aaSorting": [[ 4, "desc" ]],');
		$this->EE->javascript->output($dt);
		$this->EE->javascript->output($this->EE->ct_admin_js->get_check_toggle());
		$this->EE->javascript->compile();

				
		$this->EE->load->library('pagination');
		$vars['pagination'] = $this->EE->ct_admin_lib->create_pagination('edit_customers_ajax_filter', $total, $this->settings['orders_list_limit']);
				
		$vars['total_customers'] = $total;		
		
		$statuses = array();
		$i = 0;
		foreach($this->order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = lang($status['status']);
		}
		
		asort($statuses);
		$prefix = array(''=>lang('filter_by_status'));
		$statuses = array_merge($prefix, $statuses);
		$vars['success_statuses'] = $statuses;
		$vars['status_selected'] = '';
		$vars['date_selected'] = '';
		$vars['order_keywords'] = '';

		$first_date = $this->EE->ct_admin_orders->get_first_date();
		if(isset($first_date['day']))
		{
			$vars['default_start_date'] = $first_date['year'].'-'.$first_date['month'].'-'.$first_date['day'];
		}
		else
		{
			$vars['default_start_date'] = date('Y-m-d');
		}

		$vars['perpage_select_options'] = $this->EE->ct_admin_lib->perpage_select_options();
		$vars['date_select_options'] = $this->EE->ct_admin_lib->date_select_options();
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('customers');
		return $this->EE->load->view('customers', $vars, TRUE);
	}
	
	/**
	 * Customers AJAX Endpoint
	 */
	public function edit_customers_ajax_filter()
	{
		die($this->EE->ct_admin_customers->json_ordering($this->settings['cust_list_limit'], $this->url_base, FALSE));
	}
	
	/**
	 * Customer View Page
	 */
	public function customer_view()
	{
		$email = $this->EE->input->get_post('email', FALSE);
		$this->EE->lang->loadfile('myaccount');
		$this->EE->lang->loadfile('member');
		$this->EE->load->model('member_model');	
		
		$vars = array();
		$vars['customer_data'] = $this->EE->ct_admin_customers->get_customer_data($email);
		$vars['order_data'] = $this->EE->ct_admin_customers->get_orders_by_email($email);
		if($vars['order_data'])
		{
			$vars['total_orders'] = count($vars['order_data']);
		}
		
		$vars['product_data'] = $this->EE->ct_admin_customers->get_products_by_email($email);
		$vars['total_products'] = 0;
		if($vars['product_data'])
		{
			$vars['total_products'] = count($vars['product_data']);
		}		
		
		$vars['total_amount_spent'] = 0;
		foreach($vars['order_data'] AS $order)
		{
			if(in_array($order['status'], $this->success_statuses))
			{
				$vars['total_amount_spent'] = $vars['total_amount_spent']+$order['order_total'];
			}
		}
		
		$purchase_dates = $this->EE->ct_admin_customers->get_purchase_dates($email);
		$vars['last_order'] = $purchase_dates['last_order'];
		$vars['first_order'] = $purchase_dates['first_order'];
				
		$this->EE->cp->add_js_script('ui', 'accordion'); 
		$this->EE->jquery->tablesorter('#customer_orders table', '{headers: {4: {sorter: false}}, widgets: ["zebra"], sortList: [[3,1]]}');  
		//$this->EE->jquery->tablesorter('#customers_products table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[3,1]]}');  
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_modal());
		$this->EE->javascript->output($this->EE->ct_admin_js->ajax_order_status());		
		$this->EE->javascript->compile();
		
		$order_channel_statuses = $this->EE->ct_admin_orders->get_channel_statuses();
		$statuses = array();
		foreacH($order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = $status['status'];
		}
		
		$vars['statuses'] = $statuses;
				
		$urls = $this->EE->ct_admin_customers->get_customer_view_menu($vars['customer_data']);
		$vars['menu_data'] = $urls;	

		$vars['hook_view'] = $this->EE->ct_admin_customers->setup_customer_hook_view($vars['customer_data']);
		$vars['hook_secondary_view'] = $this->EE->ct_admin_customers->setup_customer_hook_secondary_view($vars['customer_data']);
		$vars['hook_tertiary_view'] = $this->EE->ct_admin_customers->setup_customer_hook_tertiary_view($vars['customer_data']);

		$this->add_breadcrumb($this->url_base.'customers', lang('customers'));
		
		$this->EE->load->vars(
			array(
				'customer_email' => $email
			)
		);
		
		$vars['abandoned_carts'] = array();
		if( ! empty($vars['customer_data']['ee_member_id']) )
		{
			$where = array('member_id' => $vars['customer_data']['ee_member_id']);
			$vars['abandoned_carts'] = $this->EE->ct_admin_carts->get_customer_cart_items($where);
		}

		$this->EE->view->cp_page_title = $vars['customer_data']['cust_first_name'].' '.$vars['customer_data']['cust_last_name'];
		return $this->EE->load->view('customer_view', $vars, TRUE);	
	}
	
	/**
	 * Products List Page
	 */
	public function products()
	{	
		$vars = array();
		$total = $this->EE->ct_admin_products->get_total_products();
		
		//we have to check each set product channel since some channels may be empty. Break on 1st though.
		$actual_product_channel = '0';
		foreach($this->EE->ct_admin_products->channel_ids AS $key => $actual_product_channel)
		{
			$vars['products'] = $this->EE->ct_admin_products->get_products($this->settings['products_list_limit'], 0, 'total_sold DESC', "AND ct.channel_id = '".$actual_product_channel."'");
			if(count($vars['products']) >= 1)
			{
				break;
			}
		}		
		
		if ( ! $rownum = $this->EE->input->get_post('rownum'))
		{
			$rownum = 0;
		}
		
		$dt = $this->EE->ct_admin_js->get_products_datatables('edit_products_ajax_filter', 7, 1, $this->settings['products_list_limit'], '"aaSorting": [[ 3, "desc" ]],');
		$this->EE->javascript->output($dt);
		$this->EE->javascript->output($this->EE->ct_admin_js->get_check_toggle());
		$this->EE->cp->add_js_script(array('plugin' => array('dataTables'),'ui' => 'datepicker,slider'));
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_modal());
		//$this->EE->javascript->output($this->EE->ct_admin_js->ajax_order_status());
		$this->EE->javascript->compile();
		
		$this->EE->load->library('pagination');
		$vars['pagination'] = $this->EE->ct_admin_lib->create_pagination('edit_products_ajax_filter', $total, $this->settings['products_list_limit']);
		
		$vars['total_products'] = $total;
		$statuses = array();
		$i = 0;
		
		if(is_array($this->product_channel_statuses))
		{
			foreach($this->product_channel_statuses AS $status)
			{
				$statuses[$status['status']] = lang($status['status']);
			}
		}
		
		asort($statuses);
		$prefix = array(''=>lang('filter_by_status'));
		$vars['change_statuses'] = $statuses;
		$statuses = array_merge($prefix, $statuses);
		$vars['success_statuses'] = $statuses;
		$vars['status_selected'] = '';
		$vars['date_selected'] = '';
		$vars['order_keywords'] = '';
		
		$first_date = $this->EE->ct_admin_products->get_first_date("AND ct.channel_id = '".$actual_product_channel."'");
		if(isset($first_date['day']))
		{
			$vars['default_start_date'] = $first_date['year'].'-'.$first_date['month'].'-'.$first_date['day'];
		}
		else
		{
			$vars['default_start_date'] = date('Y-m-d');
		}
		
		$vars['channel_options'] = $this->EE->ct_admin_products->get_channel_options();
		$vars['action_options'] = $this->EE->ct_admin_lib->get_action_options();
		$vars['perpage_select_options'] = $this->EE->ct_admin_lib->perpage_select_options();
		$vars['date_select_options'] = $this->EE->ct_admin_lib->date_select_options();
		$vars['default_channel_id'] = $actual_product_channel;

		$this->EE->view->cp_page_title = $this->EE->lang->line('products');
		return $this->EE->load->view('products', $vars, TRUE);		
	}
	
	/**
	 * Products List AJAX Endpoint
	 */
	public function edit_products_ajax_filter()
	{
		die($this->EE->ct_admin_products->json_ordering($this->perpage, $this->url_base));
	}

	/**
	 * Product View Page
	 */
	public function product_view()
	{
		$vars = array();
		$product_id = (int)$this->EE->input->get('id', TRUE);
		$lightbox = $this->EE->input->get('lightbox', FALSE);
		if($product_id == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('product_not_found'));
			$this->EE->functions->redirect($this->url_base.'products');
			exit;
		}
		
		$vars['lightbox'] = $lightbox;
		$vars['statuses'] = $this->EE->ct_admin_products->get_channel_statuses();
		$vars['product_details'] = $this->EE->ct_admin_products->get_product($product_id);
		if(!$vars['product_details'] || count($vars['product_details']) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('product_not_found'));
			$this->EE->functions->redirect($this->url_base.'products');
			exit;
		}
		
		$product_channel = $this->EE->cartthrob->store->config('product_channels');
		$vars['product_id'] = $product_id;
		
		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->cp->add_js_script(array('ui' => 'tabs'));
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_dialogs());
		$this->EE->javascript->output($this->EE->ct_admin_js->get_modal());
		$this->EE->jquery->tablesorter('#product_customers table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[2,1]]}');
		$this->EE->jquery->tablesorter('#product_sum_report table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[2,1]]}');
		$this->EE->jquery->tablesorter('#product_abandoned_carts table', '{headers: {6: {sorter: false}}, widgets: ["zebra"], sortList: [[2,1]]}');
		$this->EE->jquery->tablesorter('#monthly_history_report_table table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[0,1]]}');
		$this->EE->javascript->compile();
		
		$urls = $this->EE->ct_admin_products->get_product_view_menu($product_id, $vars['product_details']);
		$vars['menu_data'] = $urls;
		$vars['product_sum_report'] = $this->EE->ct_admin_reports->product_sum_report($this->order_ignore_sql. " AND ecoi.entry_id = '$product_id'"); 
		$vars['successful_sold'] = $vars['successful_quantity'] = '0';
		foreach($vars['product_sum_report'] AS $report)
		{
			$vars['successful_quantity'] = $vars['successful_quantity']+$report['total_quantity'];
			$vars['successful_sold'] = $vars['successful_sold']+$report['total_price'];
		}
		
		if($lightbox == 'yes')
		{
			echo  $this->EE->load->view('product_overview', $vars, TRUE);
			exit;
		}
		
		//janky; need to abstract this WAY better (or, you know, *some*)
		$sub = "SELECT order_id FROM ".$this->EE->db->dbprefix."cartthrob_order_items WHERE entry_id = '$product_id'";
		$where = $this->order_ignore_sql." AND ect.entry_id IN($sub)";
		
		$vars['abandoned_carts'] = $this->EE->ct_admin_carts->get_product_carts(array('product_id' => $product_id));
		$all_totals = $this->EE->ct_admin_reports->get_all_totals($where);
		
		$vars['total_customers'] = $this->EE->ct_admin_customers->get_total_customers($where);
		$vars['customers'] = $this->EE->ct_admin_customers->get_customers($this->settings['cust_list_limit'], 0, 'total_orders DESC', $where);		
		
		$vars['hook_view'] = $this->EE->ct_admin_products->setup_product_hook_view($vars['product_details']);
		$vars['hook_secondary_view'] = $this->EE->ct_admin_products->setup_product_hook_secondary_view($vars['product_details']);
		$vars['hook_tertiary_view'] = $this->EE->ct_admin_products->setup_product_hook_tertiary_view($vars['product_details']);
		
		$vars['product_meta'] = $this->EE->ct_admin_products->setup_meta($vars['product_sum_report']);
		$vars['all_totals'] = $this->EE->ct_admin_reports->clean_month_chart_data(
			$all_totals, $vars['product_meta']['first_order']['entry_date'], $vars['product_meta']['last_order']['entry_date']
		);
		
		$this->add_breadcrumb($this->url_base.'products', lang('products'));
		
		$this->EE->view->cp_page_title = $vars['product_details']['title'];
		return $this->EE->load->view('product_view', $vars, TRUE);		
	}
	
	/**
	 * Product Action Page
	 */
	public function action_product_confirm()
	{
		$product_ids = $this->EE->input->get_post('toggle', TRUE);
		$submit_action = $this->EE->input->get_post('submit_action', FALSE);
		$change_status = $this->EE->input->get_post('change_status', FALSE);
	
		if(!$product_ids || count($product_ids) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('product_not_found'));
			$this->EE->functions->redirect($this->url_base.'products');
			exit;
		}
	
		$product_data = array();
		$ids = array();
		foreach($product_ids AS $id)
		{
			$data = $this->EE->ct_admin_products->get_product($id);
			if(is_array($data) && count($data) != '0')
			{
				$order_data[] = array('id' => $data['entry_id'], 'title' => $data['title']);
				$ids[] = $data['entry_id'];
			}
		}
	
		$cp_page_title = $download_delete_question = $change_status_question = FALSE;
		$vars = array();
		switch($submit_action)
		{
			case 'delete':
				$cp_page_title = $this->EE->lang->line('delete_product_confirm');
				$download_delete_question = $this->EE->lang->line('delete_product_confirm');
	
				$vars['form_action'] = $this->query_base.'delete_products';
				$view = 'delete_product_confirm';
				break;
					
			case 'change_status':
	
				$question = str_replace('#status#', $change_status, $this->EE->lang->line('change_status_question'));
				$cp_page_title = $question;
				$change_status_question = $question;
	
				$statuses = array();
				$i = 0;
				foreach($this->product_channel_statuses AS $status)
				{
					$statuses[$status['status']] = lang($status['status']);
				}
	
				$vars['statuses'] = $statuses;
				$vars['status_selected'] = $change_status;
				$vars['form_action'] = $this->query_base.'change_products_status';
				$view = 'change_product_status_confirm';
				break;
		}
	
		$vars['damned'] = $ids;
		$vars['data'] = $product_data;
		
		$this->EE->load->vars(
			array(
				'cp_page_title' => $cp_page_title,
				'download_delete_question' => $download_delete_question,
				'change_status_question' => $change_status_question
			)
		);		
	
		return $this->EE->load->view($view, $vars, TRUE);
	}
	
	/**
	 * Product Status Change Action
	 */
	public function change_products_status()
	{
		$order_ids = $this->EE->input->get_post('products', FALSE);
		$status = $this->EE->input->get_post('status', FALSE);
		if(!$order_ids || count($order_ids) == 0 || !$status)
		{
			if(AJAX_REQUEST)
			{
				$this->EE->output->send_ajax_response(array('failure'), TRUE);
			}
			else
			{
				$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('product_not_found'));
				$this->EE->functions->redirect($this->url_base.'products');
				exit;
			}
		}
	
		if($this->EE->ct_admin_products->upate_status($product_ids, $status))
		{
			if(AJAX_REQUEST)
			{
				$this->EE->output->send_ajax_response(array('success'), FALSE);
			}
			else
			{
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('products_status_updated'));
				$this->EE->functions->redirect($this->url_base.'products');
				exit;
			}
		}
	
		if(AJAX_REQUEST)
		{
			$this->EE->output->send_ajax_response(array('failure'), TRUE);
		}
		else
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('products_status_updated_failure'));
			$this->EE->functions->redirect($this->url_base.'products');
			exit;
		}
	}
	
	/**
	 * Product Delete Action
	 */
	public function delete_products()
	{
		$product_ids = $this->EE->input->get_post('delete', FALSE);
		if(!$product_ids || count($product_ids) == 0)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('products_not_found'));
			$this->EE->functions->redirect($this->url_base.'products');
			exit;
		}
	
		if($this->EE->ct_admin_products->delete_products($product_ids))
		{
			$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('products_deleted'));
			$this->EE->functions->redirect($this->url_base.'products');
			exit;
		}
		$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('products_delete_failure'));
		$this->EE->functions->redirect($this->url_base.'products');
		exit;
	}	
	
	/**
	 * Summary report Page
	 */
	public function reports()
	{
		$vars = array();
		$vars['current_month_total'] = $this->EE->ct_admin_reports->get_totals(" AND year = '".date('Y')."' AND month = '".date('m')."' ". $this->order_ignore_sql);
		$vars['current_year_total'] = $this->EE->ct_admin_reports->get_totals(" AND year = '".date('Y')."' ". $this->order_ignore_sql);
		$vars['all_totals'] = $this->EE->ct_admin_reports->get_all_totals($this->order_ignore_sql);
		$vars['current_day_total'] = $this->EE->ct_admin_reports->get_totals(" AND year = '".date('Y')."' AND month = '".date('m')."' AND day = '".date('d')."' ".$this->order_ignore_sql);
		$vars['total_sales'] = $this->EE->ct_admin_orders->get_total_sales($this->order_ignore_sql);
		
		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->cp->add_js_script(array('ui' => 'tabs'));
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		//$this->EE->javascript->output($this->EE->ct_admin_js->get_dialogs());
		$this->EE->jquery->tablesorter('#monthly_history_report table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[0,1]]}');
		$this->EE->javascript->compile(); 	

		$this->EE->view->cp_page_title = $this->EE->lang->line('reports');
		$vars['menu_data'] = $this->EE->ct_admin_reports->get_reports_view_menu();
		return $this->EE->load->view('reports/summary', $vars, TRUE);		
	}

	/**
	 * Inventory report Page
	 */
	public function inventory_report()
	{
		$vars['enable_low_stock_report'] = TRUE;
		$vars['low_stock_report'] = $this->EE->ct_admin_reports->low_stock_report();

		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_dialogs());
		$this->EE->jquery->tablesorter('#low_stock_report table', '{headers: {2: {sorter: false}}, widgets: ["zebra"], sortList: [[1,0]]}');
		$this->EE->javascript->compile();
		
		$this->add_breadcrumb($this->url_base.'reports', lang('reports'));
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('low_stock_report');
		$vars['menu_data'] = $this->EE->ct_admin_reports->get_reports_view_menu();
		return $this->EE->load->view('reports/inventory', $vars, TRUE);			
	}
	
	/**
	 * Product Sum report Page
	 */	
	public function product_sum_report()
	{	
		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_dialogs());
		$this->EE->jquery->tablesorter('#product_sum_report table', '{headers: {6: {sorter: false}}, widgets: ["zebra"], sortList: [[3,1]]}');
		$this->EE->javascript->compile();
		
		$this->add_breadcrumb($this->url_base.'reports', lang('reports'));
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('product_sum_report');
		$vars['product_sum_report'] = $this->EE->ct_admin_reports->product_sum_report($this->order_ignore_sql);
		$vars['menu_data'] = $this->EE->ct_admin_reports->get_reports_view_menu();
		return $this->EE->load->view('reports/product_sum', $vars, TRUE);
	}

	/**
	 * Country Order report Page
	 */	
	public function country_report()
	{
		$this->EE->cp->add_js_script('ui', 'accordion');
		$this->EE->cp->add_js_script(array('plugin' => array('overlay', 'overlay.apple')));
		$this->EE->javascript->output($this->EE->ct_admin_js->get_dialogs());
		$this->EE->jquery->tablesorter('#product_sum_report table', '{headers: {6: {sorter: false}}, widgets: ["zebra"], sortList: [[3,1]]}');
		$this->EE->javascript->compile();
				
		if($this->settings['country_report_data'] == 'ip_to_location' && $this->EE->ct_admin_lib->is_installed_module('ip_to_nation'))
		{
			$vars['country_data'] = $this->EE->ct_admin_reports->get_ip_to_country();
		}
		else
		{
			$vars['country_data'] = $this->EE->ct_admin_reports->get_order_countries();
		}

		$this->add_breadcrumb($this->url_base.'reports', lang('reports'));
		
		$this->EE->view->cp_page_title = $this->EE->lang->line('order_country_report');
		$vars['menu_data'] = $this->EE->ct_admin_reports->get_reports_view_menu();
		return $this->EE->load->view('reports/country', $vars, TRUE);	
	}	
	
	/**
	 * History Report Page
	 */
	public function history_report()
	{	
		$month = $this->EE->input->get_post('month', FALSE);
		$year = $this->EE->input->get_post('year', FALSE);
		if(!$year || !$month)
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('nothing_to_report'));
			$this->EE->functions->redirect($this->url_base.'reports');
			exit;
		}		
		
		$customer_where = " AND year='$year' AND month='$month'";
		$where = $this->order_ignore_sql." ".$customer_where;
		
		//setup the product monthly history report stuff
		$vars['product_id'] = $product_id = $this->EE->input->get_post('product_id', FALSE);
		if($product_id)
		{
			$vars['product_data'] = $this->EE->ct_admin_products->get_product($product_id);
			$sub = "SELECT order_id FROM ".$this->EE->db->dbprefix."cartthrob_order_items WHERE entry_id = '$product_id'";
			$where .= " AND ect.entry_id IN($sub)";		
			$customer_where .= " AND ect.entry_id IN($sub)";
		}
		
		$min_date = $this->EE->ct_admin_orders->get_first_date();
		if($min_date['month'] < $month && $year < $min_date['year'])
		{
			/**
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('nothing_to_report'));
			$this->EE->functions->redirect($this->url_base.'reports');
			exit;
			*/
		}
		
		$vars['product_sum_report'] = $this->EE->ct_admin_reports->product_sum_report($where);	
		if(count($vars['product_sum_report']) == '0')
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('nothing_to_report'));
			$this->EE->functions->redirect($this->url_base.'reports');
			exit;
		}		
		
		$title_date = m62_convert_timestamp(mktime(0,0,0, $month, 1, $year), '%M %Y');
		$vars['prev_range'] = 'month='.m62_convert_timestamp(mktime(0,0,0, $month-1, 1, $year), '%m').AMP.'year='.m62_convert_timestamp(mktime(0,0,0, $month-1, 1, $year), '%Y');
		$vars['next_range'] = 'month='.m62_convert_timestamp(mktime(0,0,0, $month+1, 1, $year), '%m').AMP.'year='.m62_convert_timestamp(mktime(0,0,0, $month+1, 1, $year), '%Y');
		$vars['graph_limit'] = days_in_month($month, $year);
		$vars['year'] = $year;
		$vars['month'] = $month;
		
		$time = mktime(0,0,0, $month+1, 1, $year);
		$vars['next_nav_date'] = FALSE;
		if($time < time())
		{
			$vars['next_nav_date'] = m62_convert_timestamp($time, '%M %Y');
		}
		
		$vars['prev_nav_date'] = FALSE;
		$min_date['month'] = (int)$min_date['month'];
		$time = mktime(0,0,0, $month-1, 1, $year);
		$first = mktime(0,0,0, $min_date['month'], 1, $min_date['year']);
		if($first <=  $time)
		{
			$vars['prev_nav_date'] = m62_convert_timestamp($time, '%M %Y');
		}
		
		$vars['current_month'] = FALSE;
		if($month == date('m') && $year == date('Y'))
		{
			$vars['current_month'] = TRUE;
			$vars['graph_limit'] = date('j');
		}
		
		$order_channel_statuses = $this->EE->ct_admin_orders->get_channel_statuses();
		
		$statuses = array();
		foreacH($order_channel_statuses AS $status)
		{
			$statuses[$status['status']] = $status['status'];
		}
		
		$vars['statuses'] = $statuses;
		
		
		$this->EE->cp->add_js_script('ui', 'accordion');
		
		$this->EE->javascript->output($this->EE->ct_admin_js->get_dialogs());
		$this->EE->javascript->output($this->EE->ct_admin_js->get_modal());
		$this->EE->javascript->output($this->EE->ct_admin_js->ajax_order_status());
		
		$this->EE->jquery->tablesorter('#history_customers table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[4,1]]}');
		$this->EE->jquery->tablesorter('#todays_orders table', '{headers: {5: {sorter: false}}, widgets: ["zebra"], sortList: [[3,1]]}');
		$this->EE->jquery->tablesorter('#product_sum_report table', '{headers: {4: {sorter: false}}, widgets: ["zebra"], sortList: [[3,1]]}'); 
		$this->EE->javascript->compile();
				
		$vars['total_sales'] = $this->EE->ct_admin_orders->get_total_sales($where);
		$vars['average_order'] = $this->EE->ct_admin_orders->get_average_order($where);
		$vars['total_orders'] = $this->EE->ct_admin_orders->get_total_orders($where);
		$vars['orders'] = $this->EE->ct_admin_orders->get_orders($this->settings['orders_list_limit'], 0, 'entry_date DESC', $customer_where);
		$this->EE->load->library('pagination');
		$vars['pagination'] = $this->EE->ct_admin_lib->create_pagination('edit_customers_ajax_filter', '20', $this->settings['orders_list_limit']);
		
		$vars['total_customers'] = $this->EE->ct_admin_customers->get_total_customers($customer_where);
		$vars['customers'] = $this->EE->ct_admin_customers->get_customers($this->settings['cust_list_limit'], 0, 'entry_date DESC', $customer_where); 
		$vars['chart_order_history'] = $this->EE->ct_admin_reports->get_chart_order_date($vars['graph_limit'], $where, $month, $year, 1);

		if($first <=  $time) //setup total comparisons for last month numbers
		{
			$prev['month'] = date('m', mktime(0,0,0, $month-1, 1, $year));
			$prev['year'] = date('Y', mktime(0,0,0, $month-1, 1, $year));
			$prev['customer_where'] = " AND year='".$prev['year']."' AND month='".$prev['month']."'";
					
			$prev['where'] = $this->order_ignore_sql." ".$prev['customer_where'];
			if($product_id)
			{
				$sub = "SELECT order_id FROM ".$this->EE->db->dbprefix."cartthrob_order_items WHERE entry_id = '$product_id'";
				$prev['where'] .= " AND ect.entry_id IN($sub)";
				$prev['customer_where'] .= " AND ect.entry_id IN($sub)";
			}			
						
			$vars['prev']['total_sales'] = $this->EE->ct_admin_orders->get_total_sales($prev['where']);
			$vars['prev']['average_order'] = $this->EE->ct_admin_orders->get_average_order($prev['where']);	
			$vars['prev']['total_orders'] = $this->EE->ct_admin_orders->get_total_orders($prev['where']);
			$vars['prev']['total_customers'] = $this->EE->ct_admin_customers->get_total_customers($prev['customer_where']);		
		}
					
		$cp_page_title = $this->EE->lang->line('activity_for').' '.$title_date;	
		$vars['hook_view'] = $this->EE->ct_admin_reports->setup_history_report_hook_view();
		$vars['hook_secondary_view'] = $this->EE->ct_admin_reports->setup_history_report_hook_secondary_view();
		$vars['hook_tertiary_view'] = $this->EE->ct_admin_reports->setup_history_report_hook_tertiary_view();
		
		if($product_id)
		{
			$vars['prev_range'] = 'product_id='.$product_id.AMP.'month='.m62_convert_timestamp(mktime(0,0,0, $month-1, 1, $year), '%m').AMP.'year='.m62_convert_timestamp(mktime(0,0,0, $month-1, 1, $year), '%Y');
			$vars['next_range'] = 'product_id='.$product_id.AMP.'month='.m62_convert_timestamp(mktime(0,0,0, $month+1, 1, $year), '%m').AMP.'year='.m62_convert_timestamp(mktime(0,0,0, $month+1, 1, $year), '%Y');	
			$cp_page_title = $this->EE->lang->line('activity_for').$title_date;
			$this->add_breadcrumb($this->url_base.'products', lang('Products'));
			$this->add_breadcrumb($this->url_base.'product_view&id='.$product_id, $vars['product_data']['title']);
		}
		else
		{
			$this->add_breadcrumb($this->url_base.'reports', lang('reports'));
		}	

		$this->EE->view->cp_page_title = $cp_page_title;
		return $this->EE->load->view('history_report', $vars, TRUE);
	}
	
	/**
	 * Displays the overlay for a given product_id and the shopping carts on the site
	 */
	public function product_abandoned_cart_overlay()
	{
		$product_id = $this->EE->input->get_post('product_id');
		$member_id = $this->EE->input->get_post('member_id');
		$vars['product_data'] = $this->EE->ct_admin_products->get_product($product_id);
		if( ! $vars['product_data'] )
		{
			echo lang('product_not_found');
			exit;
		}
		
		$where = array('product_id' => $product_id, 'cart.member_id' => $member_id);
		$abandoned_cart_meta = $this->EE->ct_admin_carts->get_product_carts($where);
		if( empty($abandoned_cart_meta['0']) )
		{
			echo lang('cart_data_not_found');
			exit;			
		}
		
		$vars['page_title'] = lang('customer_abandoned_cart'). ' ('.$vars['product_data']['title'].')';
		$this->EE->view->cp_page_title = $vars['page_title'];
		$where = array('product_id' => $product_id, 'member_id' => $member_id);
		$vars['abandoned_carts'] = $this->EE->ct_admin_carts->get_customer_cart_items($where);
		$vars['abandoned_cart_meta'] = $abandoned_cart_meta['0'];
		
		echo  $this->EE->load->view('product_abandoned_cart_overlay', $vars, TRUE);
		exit;
	}
	
	/**
	 * Settings Page
	 */
	public function settings()
	{
		if( isset( $_POST['go_settings'] ) )
		{
			if( $this->EE->ct_admin_settings->update_settings($_POST) )
			{	
				$this->EE->logger->log_action($this->EE->lang->line('log_settings_updated'));
				$this->EE->session->set_flashdata('message_success', $this->EE->lang->line('settings_updated'));
				$this->EE->functions->redirect($this->url_base.'settings' . AMP. 'section='.$this->EE->input->get_post('section'));		
				exit;			
			}
			else
			{
				$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('settings_update_fail'));
				$this->EE->functions->redirect($this->url_base.'settings' . AMP. 'section='.$this->EE->input->get_post('section'));	
				exit;					
			}
		}		
		
		$this->EE->cp->add_js_script('ui', 'accordion'); 
		$this->EE->javascript->compile();	

		$vars = array();
		$statuses = array();
		$i = 0;
		foreach($this->order_channel_statuses AS $status)
		{
			$statuses[$status['status_id']] = lang($status['status']);
		}
		
		asort($statuses);
		$vars['success_statuses'] = $statuses;		
		
		$vars['template_options'] = $this->EE->ct_admin_lib->get_template_options();
		$vars['country_report_data_options'] = array('country_field' => lang('country_field'), 'ip_to_location' => lang('ip_to_location_module'));
		$vars['settings_disable'] = FALSE;
		$vars['member_groups'] = $this->EE->ct_admin_settings->get_member_groups();
		
		if(isset($this->EE->config->config['ct_admin']))
		{
			//$vars['settings_disable'] = 'disabled="disabled"';
		}	

		$vars['type'] = $this->EE->input->get_post('section');
		$vars['menu_data'] = $this->EE->ct_admin_lib->get_settings_view_menu();
		$this->EE->view->cp_page_title = $this->EE->lang->line('settings');
		return $this->EE->load->view('settings', $vars, TRUE);
	}
	
	/**
	 * Export Action
	 */
	public function export()
	{
		$type = $this->EE->input->get('type', FALSE);
		$arr = array();
		$i = 0;
		switch($type)
		{
			
			case 'product_abandoned_carts':
				
				$product_id = (int)$this->EE->input->get('product_id');
				$arr = $this->EE->ct_admin_carts->get_product_carts(array('product_id' => $product_id));
				
			break;
			
			case 'product_monthly_history_report':
				
				$product_id = (int)$this->EE->input->get('product_id');
				$sub = "SELECT order_id FROM ".$this->EE->db->dbprefix."cartthrob_order_items WHERE entry_id = '$product_id'";
				$where = $this->order_ignore_sql." AND ect.entry_id IN($sub)";
				
				$all_totals = $this->EE->ct_admin_reports->get_all_totals($where);
				$product_meta = $this->EE->ct_admin_products->setup_meta($all_totals);
				$arr = $this->EE->ct_admin_reports->clean_month_chart_data(
					$all_totals, $product_meta['first_order']['entry_date'], $product_meta['last_order']['entry_date']
				);	
						
			break;
			case 'product_sum_report':
				$year = $this->EE->input->get('year', FALSE);
				$month = $this->EE->input->get('month', FALSE);
				$where = $this->order_ignore_sql;
				if($year && $month)
				{
					$where .= " AND month='".$this->EE->db->escape_str($month)."' AND year='".$this->EE->db->escape_str($year)."'";
				}
				
				$_arr = $this->EE->ct_admin_reports->product_sum_report($where);
				foreach($_arr AS $item)
				{
					$arr[$i] = $item;
					$arr[$i]['total'] = ($item['total_quantity']*$item['price']);
					$i++;
				}
						
			break;
			
			case 'low_stock_report':
				$arr = $this->EE->ct_admin_reports->low_stock_report();	
			break;
														
			case 'monthly_history_report':
				$_arr = $this->EE->reports->get_all_totals();
				foreach($_arr AS $item)
				{
					unset($item['name']);
					unset($item['href']);
					$arr[$i] = $item;
					$i++;
				}						
			break;
																	
			case 'customers':
			case 'product_product_customers':
				$arr = $this->EE->ct_admin_customers->get_export_customers();
			break;
			
			case 'products':
				$arr = $this->EE->ct_admin_products->get_export_products();
			break;
			
			default:
				$_arr = $this->EE->ct_admin_orders->get_export_orders();
				foreach($_arr AS $item)
				{
					unset($item['name']);
					unset($item['href']);
					$arr[$i] = $item;
					$i++;
				}
				$type = 'orders';
			break;	
		}

		if(count($arr) >= 1)
		{
			$this->EE->ct_admin_lib->download_array($arr, TRUE, $type.'.'.$this->settings['export_format']);
		}
		else
		{
			$this->EE->session->set_flashdata('message_failure', $this->EE->lang->line('export_failed'));
			$this->EE->functions->redirect($this->url_base.'index');			
		}
		exit;
	}	
	
	/**
	 * Generic Datatables Action
	 * @param string $ajax_method
	 * @param string $cols
	 * @param string $extra
	 * @return void|unknown
	 */
	public function ajax_filters($ajax_method = '', $cols = '', $extra = FALSE)
	{
		if ($ajax_method == '')
		{
			return;
		}
		
		$last_sort = FALSE;
		switch($ajax_method)
		{
			case 'edit_customers_ajax_filter':
				$last_sort = TRUE;
				$limit = $this->settings['cust_list_limit'];
			break;
			
			case 'edit_orders_ajax_filter':
			default:
				$limit = $this->settings['orders_list_limit'];
			break;
		
		}
		$js = $this->EE->ct_admin_js->get_datatables($ajax_method, $cols, $this->pipe_length, $limit, $extra, $last_sort);
		return $js;
	}
}