<?php if (!defined('BASEPATH')) exit('No direct script access allowed.');
/**
 * PHP SuperGlobals Class
 *
 * @package     ExpressionEngine
 * @category    Plugin
 * @author      Patrick McIntire
 * @copyright   Copyright (c) 2014, Patrick McIntire
 * @link        http://patrickmcintire.com
 * 
 */
$plugin_info = array(
	'pi_name'        => 'SuperGlobals',
	'pi_version'     => '1.0',
	'pi_author'      => 'Patrick McIntire',
	'pi_author_url'  => 'http://patrickmcintire.com/',
	'pi_description' => 'Allows for GET access to PHP Super Globals',
	'pi_usage'       => Super_Globals::usage()
);
/**
 * Super_Globals
 * 
 * Allows for GET access to PHP Super Globals.
 * 
 * @access public
 * @return return_value 
 */
class Super_Globals {
	
	private $_sglobal       = 0;  // number to be formatted
	private $_schain        = 0;  // number of decimal points
	
	public function __construct() {
		
		$this->EE =& get_instance();
		$this->_schain        = (string) $this->EE->TMPL->fetch_param('chain');
		
	}
	
	public function server() {
		
		$return_value = false;
		
		if (!empty($this->_schain)) {
			$return_value = $_SERVER[$this->_schain];
		}
		
		return $return_value;
		
	}
	
	public function post() {
		
		$return_value = false;
		
		if (!empty($this->_schain)) {
			
			$segmented = explode('.', $this->_schain);
		
			if (count($segmented) == 3) {
				if (isset($_POST[$segmented[0]][$segmented[1]][$segmented[2]])) {
					$return_value = $_POST[$segmented[0]][$segmented[1]][$segmented[2]];
				}
			} elseif (count($segmented) == 2) {
				if (isset($_POST[$segmented[0]][$segmented[1]])) {
					$return_value = $_POST[$segmented[0]][$segmented[1]];
				}
			} elseif (count($segmented) == 1) {
				if (isset($_POST[$segmented[0]])) {
					$return_value = $_POST[$segmented[0]];
				}
			}
		}
		
		return $return_value;
		
	}
	
	public static function usage() {
		
		ob_start(); ?>
The Super Globals plugin retrieves a value from PHP Super Globals within the system and returns it.

Basic usage:

{exp:super_globals:server chain='REMOTE_ADDR'}
{exp:super_globals:post chain='custom_data.frist_field'}
		<?php
		$buffer = ob_get_contents();
		ob_end_clean();
		
		return $buffer;
	}
}
/**
 * End of file pi.super_globals.php
 * Location: ./system/expressionengine/third_party/super_globals/pi.super_globals.php
 */