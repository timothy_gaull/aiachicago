<!doctype html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>test</title>
<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
</head>
<body>

<?php

$datetime = date("YmdHis");
$foldername1 = "judging/spa/2016/" . $datetime;
mkdir($foldername1,0755);

?>
<p>Objects <span id="sobjects">started</span></p>
<p>500 Square Feet and Under <span id="s500sq">started</span></p>
<p>501-1,000 Square Feet <span id="s1000sq">started</span></p>
<p>1,001-5,000 Square Feet <span id="s5000sq">started</span></p>
<p>Un-built Buildings <span id="sunbuilt">started</span></p>

<script>
var $sobjects = $('#sobjects');
$sobjects.load('download-images-ajax-objects.php?dt=<?php echo $datetime; ?>');
var $s500sq = $('#s500sq');
$s500sq.load('download-images-ajax-500sq.php?dt=<?php echo $datetime; ?>');
var $s1000sq = $('#s1000sq');
$s1000sq.load('download-images-ajax-501-1000sq.php?dt=<?php echo $datetime; ?>');
var $s5000sq = $('#s5000sq');
$s5000sq.load('download-images-ajax-1001-5000sq.php?dt=<?php echo $datetime; ?>');
var $sunbuilt = $('#sunbuilt');
$sunbuilt.load('download-images-ajax-unbuilt.php?dt=<?php echo $datetime; ?>');
</script>

<?php
// $.ajax({
//   url: "download-images-ajax-objects.php?dt=<?php echo $datetime; ?>",
// });
?>

</body>
</html>
