<?php

function  create_image($project_name){
// Create the image
$im00 = imagecreatetruecolor(1024, 768);

// Create some colors
$white = imagecolorallocate($im00, 255, 255, 255);
imagefilledrectangle($im00, 0, 0, 1023, 767, $white);

// Replace path by your own font path
$font = 'arial.ttf';

// Add the text
imagettftext($im00, 20, 0, 102, 624, $black, $font, "Project Name");
imagettftext($im00, 12, 0, 102, 650, $black, $font, "City / State");

// Using imagepng() results in clearer text compared with imagejpeg()
imagepng($im00,$project_name."_00.png");

imagedestroy($im00);

$im99 = imagecreatetruecolor(1024, 768);

$black = imagecolorallocate($im99, 0, 0, 0);
imagefilledrectangle($im99, 0, 0, 1023, 767, $black);

imagepng($im99,$project_name."_99.png");

imagedestroy($im99);

}

create_image("Project Name");

?>